#!/usr/bin/python
# -*- coding: utf8 -*-

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *
app = QApplication(sys.argv)

from pyssdh.OpenElectrophy.mainwindow import *

app.setFont(QFont("Verdana", 9))
main = MainLayout()
main.show()
sys.exit(app.exec_())

