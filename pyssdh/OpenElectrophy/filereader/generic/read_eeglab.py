# -*- coding: utf-8 -*-


"""
For reading EEGLAB matlab format

Author : 
Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr


"""


from scipy import *

def read_eeglab(filename) :
	print filename
	d = io.loadmat(filename,appendmat=False)
	EEG = d['EEG']
	# all field
	#for k in dir(EEG):
		#print k
		#print EEG.__getattribute__(k)
		
	list_elec = [ ]
	for e,chanloc in enumerate(EEG.chanlocs) :
		elec = {}
		elec['label'] = chanloc.labels
		elec['name'] = chanloc.labels
		elec['fs'] = EEG.srate
		elec['signal'] = EEG.data[e,:]*1e-6
		elec['num_channel'] = e+1
		elec['shift_t0'] = 0.
		elec['unit'] = 'V'
		list_elec.append(elec)
	
	list_epoch = [ ]
	for e,ev in enumerate(EEG.event) :
		epoch = { }
		epoch['type'] = 'EEGLAB trigger'
		epoch['label'] = ev.type
		epoch['num'] = e+1
		epoch['begin'] = float(ev.latency)/EEG.srate
		list_epoch.append(epoch)
	
	return list_elec , list_epoch

def test1() :
	filename = '309_ems_S2_ChS_ICA.set'
	print read_eeglab(filename)
	

if __name__ == '__main__' :
	test1()
