# -*- coding: utf-8 -*-


"""
For reading EEGLAB matlab format

Author : 
Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr


"""

from scipy import *
import re
import datetime

def read_elan(filename):
	
	###########
	## Read header file
	f = open(filename+'.ent')
	#version
	version = f.readline()
	if version[:2] != 'V2' :
		raise('read only V2 .eeg.ent files')
		return
	
	#info
	info1 = f.readline()[:-1]
	info2 = f.readline()[:-1]
	
	#date1
	l = f.readline()
	r1 = re.findall('(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)',l)
	r2 = re.findall('(\d+):(\d+):(\d+)',l)
	r3 = re.findall('(\d+)-(\d+)-(\d+)',l)
	if len(r1) != 0 :
		DD , MM, YY, hh ,mm ,ss = r1[0]
		date1 = datetime.datetime(int(YY) , int(MM) , int(DD) , int(hh) , int(mm) , int(ss) )
	elif len(r2) != 0 :
		hh ,mm ,ss = r2[0]
		date1 = datetime.datetime(2000 , 1 , 1 , int(hh) , int(mm) , int(ss) )
	elif len(r3) != 0:
		DD , MM, YY= r3[0]
		date1 = datetime.datetime(int(YY) , int(MM) , int(DD) , 0,0,0 )
	else :
		date1 = datetime.now()
	print date1
	
	#date2
	l = f.readline()
	r1 = re.findall('(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)',l)
	r2 = re.findall('(\d+):(\d+):(\d+)',l)
	r3 = re.findall('(\d+)-(\d+)-(\d+)',l)
	if len(r1) != 0 :
		DD , MM, YY, hh ,mm ,ss = r1[0]
		date2 = datetime.datetime(int(YY) , int(MM) , int(DD) , int(hh) , int(mm) , int(ss) )
	elif len(r2) != 0 :
		hh ,mm ,ss = r2[0]
		date2 = datetime.datetime(2000 , 1 , 1 , int(hh) , int(mm) , int(ss) )
	elif len(r3) != 0:
		DD , MM, YY= r3[0]
		date2 = datetime.datetime(int(YY) , int(MM) , int(DD) , 0,0,0 )
	else :
		date2 = datetime.now()
	print date2
	
	l = f.readline()
	l = f.readline()
	l = f.readline()
	
	# frequencie sample
	l = f.readline()
	fs = 1./float(l)
	print fs
	
	# nb channel
	l = f.readline()
	nb_channel = int(l)
	print nb_channel
	
	#channel label
	labels = [ ]
	for c in range(nb_channel) :
		labels.append(f.readline()[:-1])
	print labels
	
	# channel type
	types = [ ]
	for c in range(nb_channel) :
		types.append(f.readline()[:-1])
	print types
	
	# channel unit
	units = [ ]
	for c in range(nb_channel) :
		units.append(f.readline()[:-1])
	print units
	
	#range
	min_physic = []
	for c in range(nb_channel) :
		min_physic.append( float(f.readline()) )
	max_physic = []
	for c in range(nb_channel) :
		max_physic.append( float(f.readline()) )
	min_logic = []
	for c in range(nb_channel) :
		min_logic.append( float(f.readline()) )
	max_logic = []
	for c in range(nb_channel) :
		max_logic.append( float(f.readline()) )
	print min_physic,max_physic , min_logic, max_logic
	
	#info filter
	info_filter = []
	for c in range(nb_channel) :
		info_filter.append(f.readline()[:-1])
	print info_filter
	
	f.close()
	
	#raw data
	n = int(round(log(max_logic[0]-min_logic[0])/log(2))/8)
	print n
	data = fromfile(filename,dtype = 'i'+str(n) )
	data = data.byteswap().reshape( (data.size/nb_channel ,nb_channel) ).astype('f4')
	list_sig = [ ]
	for c in range(nb_channel) :
		sig = (data[:,c]-min_logic[c])/(max_logic[c]-min_logic[c])*\
							(max_physic[c]-min_physic[c])+min_physic[c]
		list_sig.append(sig)
	
	# triggers
	f = open(filename+'.pos')
	triggers_pos = []
	triggers_labels = []
	triggers_rejets = []
	for l in f.readlines() :
		r = re.findall(' *(\d+) *(\d+) *(\d+) *',l)
		print r
		if len(r) != 0 :
			triggers_pos.append( float(r[0][0])/fs )
			triggers_labels.append( str(r[0][1]) )
			triggers_rejets.append( str(r[0][2]) )
	f.close()
	print triggers_pos,triggers_labels,triggers_rejets
	
	
	return list_sig , fs, labels , types, units, info1, info2, date1, date2, \
						triggers_pos,triggers_labels,triggers_rejets

def test1() :
	filename = 'POP8_5369S.eeg'
	#filename = 'PTU1_1137S.eeg'
	list_sig , fs, labels , types, units, info1, info2, \
				date1, date2,triggers_pos,triggers_labels,triggers_rejets = read_elan(filename)
	import pylab
	pylab.plot(list_sig[3])
	pylab.show()

if __name__ == '__main__' :
	test1()
