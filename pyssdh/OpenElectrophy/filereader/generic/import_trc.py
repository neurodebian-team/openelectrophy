# -*- coding: utf-8 -*-


from scipy import *
import pylab


from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *

from read_trc import read_trc

name = u'Micromed file TRC'


def open_file( filename , read_marker = True,
						trigger_on_channel = [ ],
						trigger_on_channel_label = [ ] ) :
	
	fs , list_elec , trigger_pos , trigger_label , surname , name , thedate = read_trc(	filename , 
						read_marker = read_marker,
						trigger_on_channel = trigger_on_channel,
						trigger_on_channel_label = trigger_on_channel_label
						)

	return fs , list_elec , trigger_pos , trigger_label , surname , name , thedate

class trc_dialog(ImportTemplateDialogMultiElectrode) :
	def __init__(self, parent=None):
		ImportTemplateDialogMultiElectrode.__init__(self, parent , show_checkbox_serie = False)
		
		
		v1 = QHBoxLayout()
		self.v0.addLayout(v1)
		v1.addWidget(QLabel('Trigger on analog channel : '))
		self.editTriggerChannel = QLineEdit('')
		v1.addWidget(self.editTriggerChannel)
		
		self.checkbox_RisingEgde = QCheckBox('Rising Edge')
		self.checkbox_RisingEgde.setChecked(True)
		v1.addWidget(self.checkbox_RisingEgde)
		
		self.checkbox_FallingEgde = QCheckBox('Falling Edge')
		self.checkbox_FallingEgde.setChecked(False)
		v1.addWidget(self.checkbox_FallingEgde)
		
		v2 = QHBoxLayout()
		self.v0.addLayout(v2)
		self.checkbox_NumericTrigger = QCheckBox('Numeric Trigger')
		self.checkbox_NumericTrigger.setChecked(True)
		v2.addWidget(self.checkbox_NumericTrigger)


	def list_electrode(self, n_total) :
		list_electrode = ImportTemplateDialogMultiElectrode.list_electrode(self,n_total)
		list_electrode = setdiff1d(list_electrode , self.getChannelTrigger() )
		return list_electrode
	
	def getChannelTrigger(self) :
		try :
			l = unicode(self.editTriggerChannel.text()).replace(',',' ').replace(';',' ').replace('	',' ').split(' ')
			return array([ int(e)-1 for e in l ])
		except :
			return [ ]




def import_gui() :
	dialog = trc_dialog()
	ok = dialog.exec_()
	if  ok ==  QDialog.Accepted:
		serie = Serie()
		id_serie = serie.save_to_db()
		for f,filename in enumerate(dialog.list_file()) :
			fs , list_elec , trigger_pos , trigger_label , surname , name , thedate = read_trc(filename ,
												read_marker = dialog.checkbox_NumericTrigger.isChecked() ,
												trigger_on_channel = dialog.getChannelTrigger(),
												trigger_on_channel_label = [ 'Channel %d' % (c+1) for c in dialog.getChannelTrigger() ] ,
												rising_edge = dialog.checkbox_RisingEgde.isChecked() ,
												falling_edge = dialog.checkbox_FallingEgde.isChecked()
												)
				
			trial = Trial()
			trial.info = os.path.split(filename)[1]+' - '+surname
			trial.id_serie = id_serie
			trial.thedatetime = thedate
			id_trial = trial.save_to_db()
		
			list_electrode = dialog.list_electrode(len(list_elec))
			for e in list_electrode :
				el = list_elec[e]
				elec = Electrode(id_trial = id_trial)
				elec.signal = el['signal']
				elec.fs = fs
				elec.num_channel = e+1
				elec.name = el['positive_input']
				elec.label = 'Pos: '+el['positive_input']+' Neg: '+  el['negative_input']
				elec.shift_t0 = 0.
				if  type(el['measurement_unit']) != type('') :
					elec.unit = 'V'
				else :
					elec.unit = el['measurement_unit']
				elec.save_to_db()
			for t in range(len(trigger_pos)) :
				ep = Epoch(id_trial = id_trial,
							type = 'micromed' ,
							label = trigger_label[t] ,
							num=t+1 ,
							begin = trigger_pos[t]/fs,
							zero = trigger_pos[t]/fs)
				ep.save_to_db()
