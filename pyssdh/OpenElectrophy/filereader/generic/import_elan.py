# -*- coding: utf-8 -*-

from scipy import *

from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *
from read_elan import read_elan


name = u'Elan (.eeg)'

class elan_dialog(ImportTemplateDialogMultiElectrode) :
	def __init__(self, parent=None):
		ImportTemplateDialogMultiElectrode.__init__(self, parent , show_checkbox_serie = False)
	
	def list_electrode(self,n) :
		list_electrode = ImportTemplateDialogMultiElectrode.list_electrode(self,n)
		return setdiff1d(list_electrode, [n-2 , n-1])

def import_gui() :
	dialog = elan_dialog()
	ok = dialog.exec_()
	if  ok ==  QDialog.Accepted:
		serie = Serie()
		id_serie = serie.save_to_db()
		for f,filename in enumerate(dialog.list_file()) :
			list_sig , fs, labels , types, units, info1, info2, \
				date1, date2,triggers_pos,triggers_labels,triggers_rejets = read_elan(filename)

			
			trial = Trial()
			trial.info = os.path.split(filename)[1]
			trial.id_serie = id_serie
			trial.thedatetime = date1
			id_trial = trial.save_to_db()
			
			list_electrode = dialog.list_electrode(len(list_sig))
			for e in list_electrode :
				elec = Electrode(id_trial = id_trial)
				elec.signal = list_sig[e]
				elec.fs = fs
				elec.num_channel = e+1
				elec.name = 'channel '+str(e+1)
				elec.label = labels[e]
				elec.shift_t0 = 0.
				elec.unit = units[e]
				elec.save_to_db()
				
			for e in range(len(triggers_pos)) :
				ep = Epoch(id_trial = id_trial,
							type = triggers_rejets[e] ,
							label = triggers_labels[e] ,
							num = e+1 ,
							begin = triggers_pos[e] )
				ep.save_to_db()
			
