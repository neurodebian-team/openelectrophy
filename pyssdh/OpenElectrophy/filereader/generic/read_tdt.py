# -*- coding: utf-8 -*-

import pylab
from scipy import *
import sys

import time

if sys.platform=='win32' :
	from win32com import client

#------------------------------------------------------------------------------
def sig_to_trig(sig ,tresh = 0.5 , bounce_eliminator = None) :
	print 'max',sig.max(),'min',sig.min()
	print 'bounce_eliminator',bounce_eliminator
	#tresh = (sig.max() + sig.min()) /2.
	up, = where( (sig[:-1] < tresh) & (sig[1:] >= tresh))
	dw, = where( (sig[:-1] > tresh) & (sig[1:] <= tresh))
	up.sort()
	dw.sort()
	print 'avant elimibator :',up.size
	if dw.size <1 :
		return array([],dtype='i'),array([],dtype='i')
	
	dw = dw[ dw>up[0] ]
	up = up[ up<dw[-1] ]
	if bounce_eliminator is not None :
		p, = where( (dw-up) <= bounce_eliminator)
		up[p] = -1
		up = up[up!=-1]
		dw[p] = -1
		dw = dw[dw!=-1]

	return up , dw

#------------------------------------------------------------------------------
def read_one_channel(TankName , BlockName , Channel, EventName ,
			t1=None , t2=None,
			chunksize = 60.):
	"""
	
	chunk : size of each chunk of the signal in second
	"""
	
	TT = client.Dispatch('TTank.X')
	
	if not(TT.ConnectServer('Local','Me')):
		raise('cannot find tank server in Local')

	if not(TT.OpenTank(TankName,'R')) :
		raise('cannot open tank %s' % TankName)
	
	if not(TT.SelectBlock(BlockName)):
		raise('cannot select block %s' % BlockName)
	
	# time range
	if t1 is None :
		t = TT.GetValidTimeRangesV()
		t1,t2 = t
		t1, = t1
		t2, = t2
	
	# Size estimation
	TT.SetGlobalB('T1',0.)
	TT.SetGlobalB('T2',1.)
	TT.SetGlobalB('Channel',0)
	tt1 = time.time()
	s = TT.ReadWavesV(EventName)
	s = squeeze(array(s))
	nb_channel = s.shape[1]
	fs = TT.ParseEvInfoV(0,0,9)
	print 'fs',fs
	print (round((t2-t1)*fs), nb_channel)
	sig = zeros( (round((t2-t1)*fs)) , 'f')
	print 'sig.shape', sig.shape
	
	TT.SetGlobalB('Channel',Channel)
	TT.SetGlobalB('WavesMemLimit',2**29)
	
	pos = 0
	for i in range(int((t2-t1)/chunksize)) :
		
		TT.SetGlobalB('T1',t1+i*chunksize)
		TT.SetGlobalB('T2',t1+(i+1)*chunksize)
		print TT.GetGlobalB('T1') , TT.GetGlobalB('T2')
		s = TT.ReadWavesV(EventName)
		#~ print s
		if s is not None:
			s = squeeze(array(s,'f'))
			if s.size>0:
				s = s[:-1]
				sig[pos:pos+s.shape[0] ] = s
				pos += s.shape[0]

	i = int((t2-t1)/chunksize)
	TT.SetGlobalB('T1',t1+i*chunksize)
	TT.SetGlobalB('T2',t2)
	s = TT.ReadWavesV(EventName)
	#~ print s
	if s is not None:
		s = squeeze(array(s,'f'))
		if s.size>0:
			s = s[:-1]
			sig[pos:pos+s.shape[0] ] = s

	TT.CloseTank()
	TT.ReleaseServer()
	
	return sig

#------------------------------------------------------------------------------
def get_tank_list():
	
	TT = client.Dispatch('TTank.X')
	
	if not(TT.ConnectServer('Local','Me')):
		raise('cannot find tank server in Local')
	
	list_TankName = []
	i = 0
	TankName = TT.GetEnumTank(i)
	while TankName != '':
		i += 1
		list_TankName.append(TankName)
		TankName = TT.GetEnumTank(i)
	
	TT.ReleaseServer()
	
	return list_TankName
	

#------------------------------------------------------------------------------
def get_block_list(TankName):
	
	TT = client.Dispatch('TTank.X')
	
	if not(TT.ConnectServer('Local','Me')):
		raise('cannot find tank server in Local')

	if not(TT.OpenTank(TankName,'R')) :
		raise('cannot open tank %s' % TankName)
	
	list_BlockName = []
	i = 0
	BlockName = TT.QueryBlockName(i)
	while BlockName != '':
		i += 1
		list_BlockName.append(BlockName)
		BlockName = TT.QueryBlockName(i)
	
	TT.CloseTank()
	TT.ReleaseServer()
	
	return list_BlockName
	
#------------------------------------------------------------------------------
def getinfo(TankName, BlockName, EventName):
	TT = client.Dispatch('TTank.X')
	
	if not(TT.ConnectServer('Local','Me')):
		raise('cannot find tank server in Local')

	if not(TT.OpenTank(TankName,'R')) :
		raise('cannot open tank %s' % TankName)
	
	if not(TT.SelectBlock(BlockName)):
		raise('cannot select block %s' % BlockName)
	
	TT.SetGlobalB('T1',0.)
	TT.SetGlobalB('T2',1.)
	TT.SetGlobalB('Channel',0)
	
	s = TT.ReadWavesV(EventName)
	s = squeeze(array(s))
	num_channel = s.shape[1]
	fs = TT.ParseEvInfoV(0,0,9)

	TT.CloseTank()
	TT.ReleaseServer()
	return num_channel , fs


#------------------------------------------------------------------------------
def get_fs(TankName, BlockName, EventName):
	num_channel , fs = getinfo(TankName, BlockName, EventName)
	return fs
	
#------------------------------------------------------------------------------
def get_num_channel(TankName, BlockName, EventName) :
	num_channel , fs = getinfo(TankName, BlockName, EventName)
	return num_channel

#------------------------------------------------------------------------------
def test1():
	import time
	TankName = '2008-09-03_13h50_rat=06_sess=AC'
	BlockName = 'adsonde-2'
	Channel = 1
	EventName = 'PBan'
	
	
	TankName = '2008-09-25_14h30_rat=09_sess=C+'
	BlockName = 'adsonde-2'
	Channel = 2
	EventName = 'bara'
	#~ Channel = 1
	#~ EventName = 'PBan'
	
	t1 = time.time()
	sig= read_one_channel(TankName , BlockName , Channel, EventName)
	t2 = time.time()
	print 'cest long',t2-t1
	print sig.shape
	print sig.max(),sig.min()
	pylab.plot(sig[:sig.size/10])
	pylab.show()


#------------------------------------------------------------------------------
def test2():
	print get_tank_list()
	TankName = '2008-09-03_13h50_rat=06_sess=AC'
	print get_block_list(TankName)


#------------------------------------------------------------------------------
if __name__ == '__main__' :
	test1()
	#~ test2()
	





