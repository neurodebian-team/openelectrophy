# -*- coding: utf-8 -*-

from scipy import *
from scipy import io

from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *


name = u'ASCII (.txt)'


#------------------------------------------------------------------------------
class ascii_dialog(ImportTemplateDialog) :
	#------------------------------------------------------------------------------
	def __init__(self, parent=None):
		ImportTemplateDialog.__init__(self, parent , show_checkbox_serie = False)
		
		h1 = QHBoxLayout()
		self.v0.addLayout(h1)
		h1.addWidget(QLabel('Columns : '))
		self.editFirstColumn = QLineEdit('')
		h1.addWidget(self.editFirstColumn)
		h1.addWidget(QLabel('to'))
		self.editLastColumn = QLineEdit('')
		h1.addWidget(self.editLastColumn)
		h1.addStretch(10)
		h1.addWidget(QLabel('Skip until line : '))
		self.editFirstLine = QLineEdit('')
		h1.addWidget(self.editFirstLine)
		
		h2 = QHBoxLayout()
		self.v0.addLayout(h2)
		self.checkbox_FirstColumnTime = QCheckBox('First selected column is time')
		self.checkbox_FirstColumnTime.setChecked(False)
		h2.addWidget(self.checkbox_FirstColumnTime)
		h2.addStretch(10)
		h2.addWidget(QLabel('otherwise frequency is : '))
		self.editFs = QLineEdit('')
		h2.addWidget(self.editFs)
		
		h3 = QHBoxLayout()
		self.v0.addLayout(h3)
		h3.addWidget(QLabel('Separator: '))
		self.editLineSeparator = QLineEdit('\t')
		h3.addWidget(self.editLineSeparator)
		h3.addStretch(10)
		h3.addWidget(QLabel('Labels line : '))
		self.editLabelLine = QLineEdit('')
		h3.addWidget(self.editLabelLine)
		h3.addStretch(10)
		h3.addWidget(QLabel('Unit : '))
		self.editUnit = QLineEdit('V')
		h3.addWidget(self.editUnit)
		
		
		h4 = QHBoxLayout()
		self.v0.addLayout(h4)
		h4.addWidget(QLabel('Trigger on analog channel : '))
		self.editTriggerChannel = QLineEdit('')
		h4.addWidget(self.editTriggerChannel)
		
		self.checkbox_RisingEgde = QCheckBox('Rising Edge')
		self.checkbox_RisingEgde.setChecked(True)
		h4.addWidget(self.checkbox_RisingEgde)
		
		self.checkbox_FallingEgde = QCheckBox('Falling Edge')
		self.checkbox_FallingEgde.setChecked(False)
		h4.addWidget(self.checkbox_FallingEgde)
	
	#------------------------------------------------------------------------------
	def getFirstLine(self) :
		first = self.editFirstLine.text()
		try :
			first = int(first)-1
		except :
			first = None
			first = 0
			
		return first
	
	#------------------------------------------------------------------------------
	def getColumns(self) :
		first = self.editFirstColumn.text()
		last = self.editLastColumn.text()
		try :
			first = int(first)
			last = int(last)
			columns = range( first-1,last  )
			if self.isFirstTimeFistColumn():
				columns = [0] + columns
		except :
			columns = None
		return columns

	#------------------------------------------------------------------------------
	def getSeparator(self) :
		return self.editLineSeparator.text()
	
	#------------------------------------------------------------------------------
	def isFirstTimeFistColumn(self):
		return self.checkbox_FirstColumnTime.isChecked()
	
	#------------------------------------------------------------------------------
	def getFs(self) :
		fs = self.editFs.text()
		try :
			fs = float(fs)
		except :
			fs =None
		return fs
	
	#------------------------------------------------------------------------------
	def getLabelLine(self) :
		labelLine = self.editLabelLine.text()
		try :
			labelLine = int(labelLine)-1
		except :
			labelLine =None
		return labelLine
	
	#------------------------------------------------------------------------------
	def getUnit(self) :
		return self.editUnit.text()
		
		
	#------------------------------------------------------------------------------
	def getChannelTrigger(self) :
		try :
			l = unicode(self.editTriggerChannel.text()).replace(',',' ').replace(';',' ').replace('	',' ').split(' ')
			return array([ int(e)-1 for e in l ])
		except :
			return [ ]





#------------------------------------------------------------------------------
def import_gui() :
	dialog = ascii_dialog()
	ok = dialog.exec_()
	if  ok ==  QDialog.Accepted:
		serie = Serie()
		id_serie = serie.save_to_db()
		for f,filename in enumerate(dialog.list_file()) :
			
			
			print dialog.getColumns()
			print dialog.getFirstLine()
			
			separator = dialog.getSeparator()
			columns = dialog.getColumns()
			
			print separator , columns 
			
									
			
			sig = loadtxt(filename, 
					delimiter = dialog.getSeparator(),
					usecols = dialog.getColumns() ,
					skiprows = dialog.getFirstLine(),
					dtype = 'f4')
			
			#~ sig = sig.astype('f4')
			print sig
			print sig.shape
			
			
			if dialog.isFirstTimeFistColumn() :
				fs = 1./mean(diff(sig[:,0]))
				shift_t0 = sig[0,0]
				sig = sig[:,1:]
			else :
				fs = dialog.getFs()
				shift_t0 = 0.
			print fs , shift_t0
			list_electrode = arange(sig.shape[1])
			print list_electrode
			list_electrode = setdiff1d(list_electrode, dialog.getChannelTrigger())
			print list_electrode
			
			l = dialog.getLabelLine()
			print l
			if l is None :
				labels = ['']*sig.shape[1]
			else :
				f = open(filename)
				line = ''
				for i in xrange(l+1) :
					line = f.readline()
				f.close()
				print line
				labels = line.split(dialog.getSeparator())
				print labels
				if dialog.getColumns() is not None :
					c = dialog.getColumns()
					labels = array(labels)[c]
					if dialog.isFirstTimeFistColumn() :
						labels = labels[1:]
					
			print labels
			trial = Trial()
			trial.info = os.path.split(filename)[1]
			trial.id_serie = id_serie
			trial.thedatetime = datetime.datetime.now()
			id_trial = trial.save_to_db()
			
			for e in list_electrode :
				print 'e', e
				elec = Electrode(id_trial = id_trial)
				elec.signal = sig[:,e]
				elec.fs = fs
				elec.num_channel = e+1
				elec.name = 'channel '+str(e+1)
				elec.label = labels[e]
				elec.shift_t0 = shift_t0
				elec.unit = dialog.getUnit()
				elec.save_to_db()
