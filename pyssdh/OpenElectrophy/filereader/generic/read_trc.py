# -*- coding: utf-8 -*-

"""
For reading TRC file from micromed file.

Author : 
Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

Inspired from the Matlab code for EEGLAB from Rami K. Niazy

Thanks : Etienne Daudrain
"""


import struct
from scipy import *
import datetime
import pylab

class struct_file(file):
	def read_f(self, format):
		return struct.unpack(format , self.read(struct.calcsize(format)))



def read_trc(filename ,
			read_marker = True,
			trigger_on_channel = [ ],
			trigger_on_channel_label = [ ],
			rising_edge = True, falling_edge = False,
			) :
	"""
	filename
	read_marker = True						read event on MKR channel
	trigger_on_channel = [ 0,8,63]			read trigger on eeg channel
	trigger_on_channel_label = [ 'A' , 'B' , 'C'] 	associated label
	rising_edge = True, falling_edge = False		edge for analog triggers
	"""
	

	f = struct_file(filename, 'rb')
	
	#-- Name
	f.seek(64,0)
	surname = f.read(22)
	while surname[-1] == ' ' : 
		if len(surname) == 0 :break
		surname = surname[:-1]
	name = f.read(20)
	while name[-1] == ' ' :
		if len(name) == 0 :break
		name = name[:-1]
	
	#-- Date
	f.seek(128,0)
	day, month, year = f.read_f('bbb')
	thedate = datetime.date(year+1900 , month , day)
	
	#header
	f.seek(175,0)
	header_version, = f.read_f('b')
	if header_version!=4 :
		raise('*.trc file is not Micromed System98 Header type 4')
	
	f.seek(138,0)
	Data_Start_Offset , Num_Chan , Multiplexer , Rate_Min , Bytes = f.read_f('IHHHH')
	f.seek(176+8,0)
	Code_Area , Code_Area_Length, = f.read_f('II')
	f.seek(192+8,0)
	Electrode_Area , Electrode_Area_Length = f.read_f('II')
	f.seek(400+8,0)
	Trigger_Area , Tigger_Area_Length=f.read_f('II')
	
	# reading raw data
	f.seek(Data_Start_Offset,0)
	rawdata = fromstring(f.read() , dtype = 'u'+str(Bytes))
	rawdata = rawdata.reshape(( rawdata.size/Num_Chan , Num_Chan))
	
	# Reading Code Info
	f.seek(Code_Area,0)
	code = fromfile(f, dtype='u2', count=Num_Chan)
	
	units = {-1:1e-9, 0:1e-6, 1:1e-3, 2:1, 100:'percent', 101:'bpm', 102:'Adim'}
	list_elec = [ ]
	for c in range(Num_Chan):
		elec = {}
		elec['chan_record'] = code[c]
		f.seek(Electrode_Area+code[c]*128+2,0)
		
		#-- inputs
		elec['positive_input'] = "%02d-%s" % (c,f.read(6).strip("\x00"))
		elec['negative_input'] = "%02d-%s" % (c,f.read(6).strip("\x00"))
		#-- min and max
		elec['logical_min'] , elec['logical_max'], elec['logical_ground'],elec['physical_min'], elec['physical_max'] = f.read_f('iiiii')
		print elec['logical_min'] , elec['logical_max'], elec['logical_ground'],elec['physical_min'], elec['physical_max']
		#-- unit
		k, = f.read_f('h')
		print k
		if k in units.keys() :
			elec['measurement_unit'] = units[k]
		else :
			elec['measurement_unit'] = 10e-6
		print elec['measurement_unit']
		#-- rate
		f.seek(8,1)
		elec['rate_coef'], = f.read_f('H')

		list_elec.append( elec )
	
	trigger_pos = [ ]
	trigger_label = [ ]
	
	# Read trigger
	if read_marker :
		f.seek(Trigger_Area,0)
		first_trig = 0
		for i in range(0,Tigger_Area_Length/6) :
			pos , label = f.read_f('IH')
			if ( i == 0 )  :
				first_trig = pos
			if ( pos >= first_trig ) and (pos <= rawdata.shape[0]) :
				trigger_pos.append(pos)
				trigger_label.append(label)
				
	# read trigger on eeg channel
	for c,c_trig in enumerate(trigger_on_channel) :
		sig_trig = rawdata[:,c_trig]
		
		tresh = (sig_trig.max() - sig_trig.min())*.7 +sig_trig.min()
		
		
		if rising_edge :
			pos, = where( (sig_trig[:-1]<tresh) & (sig_trig[1:] >= tresh))
			for p in pos :
				trigger_pos.append(p)
				trigger_label.append(trigger_on_channel_label[c])
		if falling_edge :
			pos, = where( (sig_trig[:-1]>tresh) & (sig_trig[1:] <= tresh))
			for p in pos :
				trigger_pos.append(p)
				trigger_label.append(trigger_on_channel_label[c])
			
	# formating data
	for c in range(rawdata.shape[1]) :
		elec = list_elec[c]
		if c in  trigger_on_channel :
			#rawdata[:,c] = nan
			pass
		else :
			factor = float(elec['physical_max'] - elec['physical_min']) / float(elec['logical_max']-elec['logical_min']+1)
			if type(elec['measurement_unit']) != type('') :
				factor *= elec['measurement_unit']
			print factor
			list_elec[c]['signal'] = (rawdata[:,c].astype('f4') - elec['logical_ground'] )* factor

	# freq sampling
	fs = mean([elec['rate_coef'] for elec in list_elec ]) * Rate_Min
	
	return  fs , list_elec , trigger_pos , trigger_label , surname , name , thedate




def test1() :
	#filename = 'EEG_107.TRC'
	#filename = 'EEG_108.TRC'
	#filename = 'EEG_129.TRC'
	#filename = 'EEG_141.TRC'
	filename = 'EEG_115.TRC'
	filename = 'EEG_116.TRC'
	
	filename = 'EEG_308_ems_S1_ChS.TRC'
	#filename = 'EEG_309_ems_S2_ChS.TRC'
	#filename = 'EEG_186_lues_.TRC'
	filename = 'EEG_329_tzl_S2_AnS.TRC'
	filename = 'EEG_315_lur_S1_AnS.TRC'
	#filename = 'EEG_335.TRC'
	
	trigger_on_channel = [  ]
	trigger_on_channel_label = [ ]
	
	fs , list_elec , trigger_pos , trigger_label , surname , name , thedate = read_trc(filename ,
												trigger_on_channel = trigger_on_channel , 
												trigger_on_channel_label = trigger_on_channel_label)
	#print fs , list_elec , trigger_pos , trigger_label , surname , name , thedate
	print surname , name
	print trigger_pos
	c = 61
	elec = list_elec[c]
	print elec
	
	fig = pylab.figure()
	ax = fig.add_subplot(111)
	ax.plot(elec['signal'])
	for t in trigger_pos :
		ax.axvline(t)
	pylab.show()
	



if __name__ == '__main__' :
	test1()
	