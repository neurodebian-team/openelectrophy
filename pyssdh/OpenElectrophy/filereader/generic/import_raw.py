# -*- coding: utf-8 -*-

from scipy import *
import os
from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *
import datetime

name = u'Raw binary file (.raw)'


#------------------------------------------------------------------------------
def open_file(filename , num_total = 16, file_dtype='i2' ,shift=0,range_int = 1, **karg ) :
	if type(file_dtype) == type('') :
		file_dtype = dtype(file_dtype)
	fid = open(filename , 'rb')
	buf = fid.read()
	fid.close()
	sig = fromstring(buf[shift:], dtype = file_dtype )
	if sig.size % num_total != 0 :
		sig = sig[:- sig.size%num_total]
	sig = sig.reshape((sig.size/num_total,num_total)).astype('f4')
	
	if file_dtype.kind == 'i' :
		sig /= 2**(file_dtype.itemsize-1)
		sig *= range_int
	return sig

#------------------------------------------------------------------------------
class ascii_dialog(BaseImportDialog):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None):
		BaseImportDialog.__init__(self, parent ,
									type_choose = QFileDialog.ExistingFiles ,
									show_select_channel = True,
									show_invert_channel = True,
									show_select_trigger = True,
									show_database_option = False)
		list_label =	[ 'dtype (i2, i4, f4, f8, <i2,>i2 )',	'num channel',	'shift at origin (bytes)', 	'range (if dtype int)',	'frequency',	'unit'	]
		default_param =	[ 'i2',									1,				0,							1,						1000.,			'V'		]
		list_param = 	[ 'file_dtype',							'num_total',	'shift',					'range_int',	'fs',			'unit'		]
		self.param_raw = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = None) 
		self.hbox_fileoption.addWidget(self.param_raw)
		

#------------------------------------------------------------------------------
def import_gui():
	
	dialog = ascii_dialog()
	ok = dialog.exec_()
	if  ok ==  QDialog.Accepted:
		serie = Serie()
		id_serie = serie.save_to_db()
		for f,filename in enumerate(dialog.list_file()) :
			karg = dialog.param_raw.get_dict()
			sig = open_file(filename , **karg )
			
			trial = Trial()
			trial.info = os.path.split(filename)[1]
			trial.id_serie = id_serie
			trial.thedatetime = datetime.datetime.now()
			id_trial = trial.save_to_db()
			
			list_electrode = dialog.list_electrode(sig.shape[1])
			for e in list_electrode :
				elec = Electrode(id_trial = id_trial)
				elec.signal = sig[:,e]
				elec.fs = karg['fs']
				elec.num_channel = e+1
				elec.name = 'channel '+str(e+1)
				elec.label = ''
				elec.shift_t0 = 0.
				elec.unit = karg['unit']
				elec.save_to_db()
		
			# Trigger
			list_trigger = dialog.getChannelTrigger()
			rising_edge = dialog.checkbox_RisingEgde.isChecked()
			falling_edge = dialog.checkbox_FallingEgde.isChecked()
			for t in list_trigger :
				trigger_pos = [ ]
				sig_trig = sig[:,t]
				tresh = (sig_trig.max() - sig_trig.min())*.5 +sig_trig.min()
				if rising_edge :
					pos, = where( (sig_trig[:-1]<tresh) & (sig_trig[1:] >= tresh))
					for p in pos :
						trigger_pos.append(p)
				if falling_edge :
					pos, = where( (sig_trig[:-1]>tresh) & (sig_trig[1:] <= tresh))
					for p in pos :
						trigger_pos.append(p)
				
			
				for e in range(len(trigger_pos)) :
					ep = Epoch(id_trial = id_trial,
								type = 'from channel '+str(t+1) ,
								label = '' ,
								num = e+1 ,
								begin = float(trigger_pos[e])/karg['fs'] )
					ep.save_to_db()
			
