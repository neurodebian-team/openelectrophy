# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from read_tdt import *

from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *



import os
import re
import datetime

name = u'TDT via tank server'





class ListTankSelector(QWidget) :
	#------------------------------------------------------------------------------
	def __init__(self, parent = None) :
		QWidget.__init__(self, parent)
		
		mainlayout = QVBoxLayout()
		mainlayout.addWidget(QLabel('List of tank to be inserted'))
		but = QPushButton('Add')
		mainlayout.addWidget(but)
		self.connect(but,SIGNAL('clicked()'),self.add_tank)
		
		w = QWidget()
		self.vboxTank = QVBoxLayout()
		w.setLayout(self.vboxTank)
		scroll = QScrollArea()
		scroll.setWidget(w)
		scroll.setWidgetResizable(True)
		mainlayout.addWidget(scroll)
		
		self.setLayout(mainlayout)
		
		self.list_del = []
		self.list_selector = []
		#self.add_tank()
	
	#------------------------------------------------------------------------------
	def add_tank(self):
		w = QWidget()
		h = QHBoxLayout()
		w.setLayout(h)
		sel = TankSelector()
		h.addWidget(sel)
		self.list_selector.append(sel)
		
		but = QPushButton('del')
		h.addWidget(but)
		self.list_del.append(but)
		self.connect( but, SIGNAL('clicked()'), self.del_tank )
		self.vboxTank.addWidget(w)
		
	#------------------------------------------------------------------------------
	def del_tank(self):
		pos = self.list_del.index(self.sender())
		del self.list_del[pos]
		del self.list_selector[pos]
		
		w = self.sender().parent()
		self.vboxTank.removeWidget(w)
		w.setVisible(False)

		
	#------------------------------------------------------------------------------
	def get_list_tank(self):
		list_tank = []
		for selector in self.list_selector :
			list_tank.append( (selector.getSelectedTank() , selector.getSelectedBlock()) )
			
		return list_tank
			
#------------------------------------------------------------------------------
class TankSelector(QWidget) :
	#------------------------------------------------------------------------------
	def __init__(self, parent = None) :
		QWidget.__init__(self, parent)
		
		v = QHBoxLayout()
		v.addWidget(QLabel('Choose the tank name'))
		self.comboTank = QComboBox()
		v.addWidget(self.comboTank)
		v.addWidget(QLabel('Choose block'))
		self.comboBlock = QComboBox()
		v.addWidget(self.comboBlock)
		
		self.setLayout(v)
		
		self.refreshTankList()
		self.list_TankName = [ ]
		
		self.connect(self.comboTank, SIGNAL('currentIndexChanged( int )'),self.refreshBlockList)
		
	#------------------------------------------------------------------------------
	def refreshTankList(self):
		self.list_TankName = get_tank_list()
		self.comboTank.clear()
		self.comboTank.addItems(self.list_TankName)
	
	#------------------------------------------------------------------------------
	def refreshBlockList(self):
		print self.getSelectedTank()
		self.list_BlockName = get_block_list(self.getSelectedTank())
		self.comboBlock.clear()
		self.comboBlock.addItems(self.list_BlockName)
		
	#------------------------------------------------------------------------------
	def getSelectedTank(self):
		ind = self.comboTank.currentIndex()
		print self.comboTank.currentText()
		return str(self.comboTank.currentText())
	
	#------------------------------------------------------------------------------
	def getSelectedBlock(self):
		ind = self.comboBlock.currentIndex()
		print self.comboBlock.currentText()
		return str(self.comboBlock.currentText())
		





#------------------------------------------------------------------------------
class tdt_dialog(BaseImportDialog):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None):
		BaseImportDialog.__init__(self, parent ,
									show_choose_file = False,
									type_choose = QFileDialog.DirectoryOnly,
									show_select_channel = True,
									show_invert_channel = False,
									show_select_trigger = False,
									show_database_option = False)
		
		v = QVBoxLayout()
		self.hbox_fileoption.addLayout(v)
		
		self.listTankSelector = ListTankSelector()
		v.addWidget(self.listTankSelector)
		
		v.addWidget(QLabel(u'Parameters TDT'))
		default_param =	[ 'Your signal name',		]
		list_param = 		[ 'SigName',	]
		self.param_tdt = ParamWidget(list_param , default_param =default_param , family = None)
		v.addWidget(self.param_tdt)







#------------------------------------------------------------------------------
def import_gui() :
	dialog = tdt_dialog()
	ok = dialog.exec_()
	
	
	if  ok ==  QDialog.Accepted:
		for TankName,BlockName in dialog.listTankSelector.get_list_tank() :
			print TankName,BlockName
		
		id_serie = None
		
		for TankName,BlockName in dialog.listTankSelector.get_list_tank() :
			
			print 'inserting', TankName,BlockName
			
			if id_serie is None :
				if serie.info != TankName :
					# new serie
					serie = Serie()
					serie.info = TankName
					serie.thedatetime = datetime.datetime.now()
					id_serie = serie.save_to_db()
			
			
			d = dialog.param_tdt.get_dict()
			EventNameSig = d['SigName']
			
			num_channel = get_num_channel(TankName, BlockName, EventNameSig)
			fs = get_fs(TankName, BlockName, EventNameSig)
			
			
			trial = Trial()
			trial.info = BlockName
			trial.id_serie = id_serie
			trial.thedatetime = datetime.datetime.now()
			id_trial = trial.save_to_db()
			
			for n in range(num_channel) :
				print 'Extraction channel',n
				sig = read_one_channel(TankName , BlockName , n+1, EventNameSig)
				
				elec = Electrode(id_trial = id_trial,
								signal = sig,
								fs = fs,
								num_channel = n+1,
								label = '',
								name = 'electrode %d'%(n+1),
								shift_t0 = 0,
								unit = u'mV',
								)
				elec.save_to_db()
				
				
				
