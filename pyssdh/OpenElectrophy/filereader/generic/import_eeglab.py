# -*- coding: utf-8 -*-




from scipy import *
import pylab


from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.dialogs.dialog_import_file import *
from read_eeglab import read_eeglab

name = u'EEGLAB (.set)'

def open_file( filename ) :
	return read_eeglab(filename)

class eeglab_dialog(ImportTemplateDialogMultiElectrode) :
	def __init__(self, parent=None):
		ImportTemplateDialogMultiElectrode.__init__(self, parent , show_checkbox_serie = False)
		

def import_gui() :
	dialog = eeglab_dialog()
	ok = dialog.exec_()
	if  ok ==  QDialog.Accepted:
		serie = Serie()
		id_serie = serie.save_to_db()
		for f,filename in enumerate(dialog.list_file()) :
			list_elec , list_epoch = open_file( filename )
			
			trial = Trial()
			trial.info = os.path.split(filename)[1]
			trial.id_serie = id_serie
			trial.thedatetime = datetime.datetime.now()
			id_trial = trial.save_to_db()
		
			list_electrode = dialog.list_electrode(len(list_elec))
			for e in list_electrode :
				el = list_elec[e]
				elec = Electrode(id_trial = id_trial)
				elec.signal = el['signal']
				elec.fs = el['fs']
				elec.num_channel = el['num_channel']
				elec.name = el['name']
				elec.label = el['label']
				elec.shift_t0 = el['shift_t0']
				elec.unit = el['unit']
				elec.save_to_db()
			
			for e in (list_epoch) :
				ep = Epoch(id_trial = id_trial,
							type = e['type'] ,
							label = e['label'] ,
							num=e['num'] ,
							begin = e['begin'])
				ep.save_to_db()