# -*- coding: utf-8 -*-
#
# module for recursif importing analysis
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 09.2006
#



import sys
import os


sys.path.append(os.path.dirname(__file__))
list_filereader = [] #hierarchic
dict_filereader = { } #flat
for filename in os.listdir(os.path.dirname(__file__)) :
	if filename[-3:] == '.py' and filename != '__init__.py' :
		#print filename
		m = __import__(filename[:-3])
		if hasattr(m,'name') and hasattr(m,'import_gui') :
			list_filereader.append(m)
			dict_filereader[m.name] = m
	if os.path.isdir(os.path.join(os.path.dirname(__file__), filename ) ) :
		if os.path.exists( os.path.join(os.path.dirname(__file__), os.path.join(filename,'__init__.py') ) )  :
			sub = __import__(filename)
			list_filereader.append(sub.list_filereader)
			dict_filereader.update(sub.dict_filereader)

name = 'Import ... '

list_filereader = (name , list_filereader )


__all__ = [ 'list_filereader' , 'dict_filereader' ]