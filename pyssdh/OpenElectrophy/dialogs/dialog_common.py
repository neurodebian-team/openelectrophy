# -*- coding: utf-8 -*-

"""
common tools for dialogs

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from pyssdh.core.gui import *



#------------------------------------------------------------------------------
# Virtual Dialog for spike and oscillation computing
#------------------------------------------------------------------------------
class  ComputeElectrodeDialog(QDialog):
	def __init__(self, parent=None ):
		QDialog.__init__(self, parent)
		mainLayout = QVBoxLayout()
		self.h1 = QHBoxLayout()
		self.v2 = QVBoxLayout()
		self.h1.addLayout(self.v2)
		mainLayout.addLayout(self.h1)
		bt_compute = QPushButton('Compute list')
		self.v2.addWidget(bt_compute)
		self.connect(bt_compute,SIGNAL('clicked()') , self.compute_electrode )
		child_hierarchy = [ 	['root',	[	[ 'electrode' ,[] ]	]		]	]
		self.queryResultBox = QueryResultBox(table = 'electrode', field_list = ['name' , 'num_channel' ,'label' ],
												default_query = 'SELECT id_electrode FROM electrode LIMIT 10' )
		self.v2.addWidget(self.queryResultBox)
		self.v3 = QVBoxLayout()
		self.h1.addLayout(self.v3)
		
		self.v_param = QVBoxLayout()
		self.v3.addLayout(self.v_param)
		
		self.setLayout(mainLayout)
		
	def compute_electrode(self) :
		pass

