# -*- coding: utf-8 -*-

"""
dialogs for manual oscillation detection

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from pyssdh.core.gui import *
from pyssdh.OpenElectrophy.my_classes import *
from dialog_common import *

from scipy import *

import pylab

#------------------------------------------------------------------------------
class WindowEditOscillation(QWidget):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None , id_electrode=None , mode = 'all signal' ) :
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		self.id_electrode = id_electrode
		self.elec = Electrode()
		self.elec.load_from_db(self.id_electrode)
		self.elec2 = None
		self.epoch = None
		self.mode = mode
		
		Oscillation().create_table()
		
		self.setWindowTitle (self.tr('Edit oscillation for id_electrode=%s' % id_electrode))
		#design
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		sph = QSplitter(Qt.Horizontal)
		sph.setOpaqueResize(False)
		mainLayout.addWidget(sph , 1)
		
		spv = QSplitter(Qt.Vertical)
		spv.setOpaqueResize(False)

		
		self.tab = QTabWidget()
		spv.addWidget(self.tab)
		
		if self.mode == 'around epoch' :
			fr = QFrame()
			self.tab.addTab(fr,'Epoch list')
			v1 = QVBoxLayout()
			fr.setLayout(v1)
			default_query = "SELECT id_epoch FROM epoch WHERE epoch.id_trial = %d "  % (self.elec.id_trial)
			self.queryBoxEpoch = QueryResultBox(table = 'epoch', field_list = Epoch.list_field, default_query = default_query )
			v1.addWidget(self.queryBoxEpoch)
			self.queryBoxEpoch.treeview_result.setSelectionMode(QAbstractItemView.SingleSelection)
			self.connect(self.queryBoxEpoch.treeview_result , SIGNAL('itemSelectionChanged()') , self.changeEpoch)

			
			list_param = 		[ 't_start',				't_stop'	 	]
			default_param  = 	[ -1.,				2.			]
			list_label = 		['Time start',		'Time stop' ]
			self.param_window = ParamWidget(list_param , default_param , list_label , family = 'window exctraction' )
			v1.addWidget(QLabel('Window Extraction'))
			v1.addWidget(self.param_window)
			
		#tab scalogram
		fr = QFrame()
		self.tab.addTab(fr,'Scalogram')
		v1 = QVBoxLayout()
		fr.setLayout(v1)
		v1.addWidget(QLabel(self.tr('Parameters for backend scalogram : ')))
		list_param = 		['f_start'	,	'f_stop',		'df_sub' ,				't_start',		't_stop',			'dt',				'f0',	'normalisation'	]
		default_param  =	[5.			,	100.,			.5 ,					-inf ,			inf,				1./self.elec.fs,	2.5,	0.				]
		list_label = 			['Freq start' ,	'Freq stop' , 	'Freq precision' , 	'Time start' ,	'Time stop' ,		'Time precision',	'f0',	'Normalisation'	]
		self.param_scalogram = ParamWidget(list_param , default_param , list_label , family = 'morlet scalogram' )
		v1.addWidget(self.param_scalogram)
		bt_recompute_map = QPushButton(self.tr('Recompute map'))
		self.connect(bt_recompute_map , SIGNAL('clicked()') , self.refresh_plot)
		v1.addWidget(bt_recompute_map)
		v1.addWidget(QLabel(self.tr('Parameters for filtering : ')))
		list_param = 		['f_low',			'f_hight'			]
		default_param  =	[ 0.,				inf					]
		list_label = 		['freq low cut',	'Freq higth cut'	]
		self.param_filter = ParamWidget(list_param , default_param , list_label , family = 'filter signal' )
		v1.addWidget(self.param_filter)
		
		
		
		#tab Oscillation detection
		fr = QFrame()
		self.tab.addTab(fr,'Oscillation detection')
		qg1 = QGridLayout()
		v1 = QVBoxLayout()
		qg1.addLayout(v1,0,0)
		fr.setLayout(qg1)
		v1.addWidget(QLabel(self.tr('Parameters for morlet :')))
		list_param = 		['f_start'	,	'f_stop',		'df_sub' ,				't_start',		't_stop',			'dt',				'f0',	'normalisation'	]
		default_param  =	[10.			,	90.,			.5 ,					0. ,			inf,				1./self.elec.fs,	2.5,	0.				]
		list_label = 			['Freq start' ,	'Freq stop' , 	'Freq precision' , 	'Time start' ,	'Time stop' ,		'Time precision',	'f0',	'Normalisation'	]
		self.param_detection = ParamWidget(list_param , default_param , list_label , family = 'morlet scalogram' )
		v1.addWidget(self.param_detection)
		
		
		v1 = QVBoxLayout()
		qg1.addLayout(v1,0,1)
		v1.addWidget(QLabel(self.tr('Parameters for treshold : ')))
		
		list_param = 		['abs_tresh',		'std_tresh',						't1' ,			't2' ,			'f1' ,			'f2'				]
		default_param =		[nan,			7.,								-inf,			0.,				10.,				90.				]
		list_label =			['Abs treshold',	'Std tresh (if abs tresh is null)',	'Window ref.t1' ,	'Window ref.t2' ,	'Window ref.f1' ,	'Window ref.f2'	]
		self.param_tresh = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'treshold oscillation' )
		v1.addWidget(self.param_tresh)
		
		
		h2 = QHBoxLayout()
		v1.addLayout(h2)
		bt_refresh_winref = QPushButton('Re reference tresh')
		h2.addWidget(bt_refresh_winref)
		self.connect(bt_refresh_winref , SIGNAL('clicked()') , self.refresh_winref )
		self.labelWinref = QLabel()
		h2.addWidget(self.labelWinref)


		v1.addWidget(QLabel(self.tr('Parameters clean : ')))
		list_param = 		[ 'min_size_cycle' , 'eliminate_simultaneous' , 	'regroup_overlap',	 'eliminate_partial_overlap'   ]
		default_param  =	[3.,				  True,						True,				True						]
		list_label = 			['Min nb of cycle' , 'Eliminate simultaneous', 	'Regroup overlap' ,	'Eliminate partial overlap' 	]
		
		self.param_clean_line = ParamWidget(list_param , default_param , list_label , family = 'clean line')
		v1.addWidget(self.param_clean_line)
		
		bt_recompute_selection = QPushButton(self.tr('Recompute selected oscillation'))
		qg1.addWidget(bt_recompute_selection,1,0)
		self.connect(bt_recompute_selection , SIGNAL('clicked()') , self.recompute_selection)
		bt_clean_selection = QPushButton(self.tr('Clean selected oscillation'))
		qg1.addWidget(bt_clean_selection,2,0)
		self.connect(bt_clean_selection , SIGNAL('clicked()') , self.clean_selection)
		bt_show_max = QPushButton(self.tr('Show new max '))
		qg1.addWidget(bt_show_max,1,1)
		self.connect(bt_show_max , SIGNAL('clicked()') , self.show_max)
		bt_add_recompute = QPushButton(self.tr('Recompute and add to list'))
		qg1.addWidget(bt_add_recompute,2,1)
		self.connect(bt_add_recompute , SIGNAL('clicked()') , self.recompute_and_add)
		bt_reset_recompute = QPushButton(self.tr('Reset all,  recompute and clean'))
		qg1.addWidget(bt_reset_recompute,3,1)
		self.connect(bt_reset_recompute , SIGNAL('clicked()') , self.reset_recompute)
		
		
		sph.addWidget(spv)
		
		#figure and canvas
		spv = QSplitter(Qt.Vertical)
		spv.setOpaqueResize(False)
		sph.addWidget(spv)
		
		self.treeview_oscillation = QTreeWidget()
		self.treeview_oscillation.setColumnCount(3)
		self.treeview_oscillation.setHeaderLabels(['oscillation' , 'time_max', 'freq_max','val_max' ])
		self.treeview_oscillation.setSelectionMode(QAbstractItemView.ExtendedSelection)
		spv.addWidget(self.treeview_oscillation)

		v2 = QVBoxLayout()
		self.canvas = MyMplCanvas(nb_ax = 0 , )
		self.fig = self.canvas.fig
		
		self.ax1 = self.canvas.fig.add_subplot(2,1,2 )
		self.ax2 = self.canvas.fig.add_subplot(2,1,1 , sharex = self.ax1)
		self.ax_colorbar = self.fig.add_axes([0.05 , 0.97 , .9 , 0.02 ] )
		v2.addWidget(self.canvas)
		self.figToolBar = MyNavigationToolbar(self.canvas , self.canvas , direction = 'h')
		v2.addWidget(self.figToolBar)
		fr = QFrame()
		fr.setLayout(v2)
		spv.addWidget(fr)
		
		# Save and Load
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		self.buttonSave = QPushButton('Save to DB (no delete)')
		self.buttonSave.setIcon(dict_icon['filesave'])
		h.addWidget(self.buttonSave)
		self.connect(self.buttonSave , SIGNAL('clicked()') , self.save_to_db_no_delete)
		
		self.buttonSave = QPushButton('Save to DB (delete old in interest zone)')
		self.buttonSave.setIcon(dict_icon['filesave'])
		h.addWidget(self.buttonSave)
		self.connect(self.buttonSave , SIGNAL('clicked()') , self.save_to_db_and_delete)
		
		self.buttonLoad = QPushButton('Reload from DB')
		self.buttonLoad.setIcon(dict_icon['reload'])
		h.addWidget(self.buttonLoad)
		self.connect(self.buttonLoad , SIGNAL('clicked()') , self.reload_from_db_all)
		
		self.buttonLoad = QPushButton('Reload from DB (only interest zone)')
		self.buttonLoad.setIcon(dict_icon['reload'])
		h.addWidget(self.buttonLoad)
		self.connect(self.buttonLoad , SIGNAL('clicked()') , self.reload_from_db_only_zone)
		
		
		
		h.addStretch(10)
		self.buttonQuit = QPushButton('Quit')
		self.buttonQuit.setIcon(dict_icon['fileclose'])
		h.addWidget(self.buttonQuit)
		frame.setLayout(h)
		mainLayout.addWidget(frame)
		
		
		
		
		self.connect(self.buttonQuit , SIGNAL('clicked()') , self , SLOT('close()') )
		
		
		
		#self.refresh_spinBox_abs_tresh()
		
		#shorcuts
		DeleteShortcut = QShortcut(QKeySequence(Qt.Key_Delete),self)
		self.connect(DeleteShortcut, SIGNAL("activated()"), self.deleteSelection)
		
		#context menu
		#SIGNAL('customContextMenuRequested ( const QPoint & pos )')
		self.treeview_oscillation.setContextMenuPolicy(Qt.CustomContextMenu)
		self.connect(self.treeview_oscillation,SIGNAL('customContextMenuRequested( const QPoint &)'),self.contextMenuTreeviewOscillation)

		self.connect(self.treeview_oscillation,SIGNAL('itemDoubleClicked(QTreeWidgetItem *, int)'),self.zoomOscillationOnDoubleClick)

		self.connect(self.treeview_oscillation,SIGNAL('itemSelectionChanged()'),self.selectionOscillationChanged)
		
		self.line_max = []
		#self.list_oscillation = [ ]
		self.reload_from_db_all()
		

	#------------------------------------------------------------------------------
	def save_to_db(self , delete_old = False ) :
		
		t_start = self.param_detection.get_dict()['t_start']
		t_stop = self.param_detection.get_dict()['t_stop']
		f_start = self.param_detection.get_dict()['f_start']
		f_stop = self.param_detection.get_dict()['f_stop']
		if self.mode == 'all signal' :
			# delete old oscillations in database
			if delete_old :
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						AND oscillation.freq_max > %s
						AND oscillation.freq_max < %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql(query , (self.id_electrode, f_start, f_stop, t_start, t_stop )  )
				for id_oscillation in id_oscillations :
					os = Oscillation()
					os.id_oscillation = id_oscillation
					os.id_principal = id_oscillation
					os.delete_from_db_and_child(dict_hierarchic_class )
			
			#create new ones
			for o,os in enumerate(self.list_oscillation) :
				os.id_principal = None
				os.id_oscillation = None
				os.id_trial = self.elec.id_trial
				os.id_electrode = self.elec.id_electrode
				id_oscillation = os.save_to_db()
		
		if self.mode == 'around epoch' :
			if self.epoch is None : return
			if delete_old :
				t_start = self.param_window.get_dict()['t_start']
				t_stop = self.param_window.get_dict()['t_stop']
				
				# delete old oscillations in database around epoch
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						AND oscillation.freq_max > %s
						AND oscillation.freq_max < %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql( query , (self.id_electrode, f_start, f_stop, self.epoch.begin+t_start , self.epoch.begin+t_stop ) )
				for id_oscillation in id_oscillations :
					os = Oscillation()
					os.id_oscillation = id_oscillation
					os.id_principal = id_oscillation
					os.delete_from_db_and_child(dict_hierarchic_class )
			
			#create new ones
			for o,os in enumerate(self.list_oscillation) :
				os.id_principal = None
				os.id_oscillation = None
				os.id_trial = self.elec.id_trial
				os.id_electrode = self.elec.id_electrode
				os.shift_t0 = self.elec.shift_t0
				os.time_start += self.epoch.begin
				os.time_stop += self.epoch.begin
				os.time_max += self.epoch.begin
				os.line_t = os.line_t + int((self.epoch.begin+self.elec2.shift_t0-self.elec.shift_t0)*self.elec.fs)
				id_oscillation = os.save_to_db()

	#------------------------------------------------------------------------------
	def save_to_db_no_delete(self) :
		self.save_to_db( delete_old = False)

	#------------------------------------------------------------------------------
	def save_to_db_and_delete(self):
		self.save_to_db( delete_old = True)
	
	#------------------------------------------------------------------------------
	def reload_from_db(self , only_by_zone = True):
		t_start = self.param_detection.get_dict()['t_start']
		t_stop = self.param_detection.get_dict()['t_stop']
		f_start = self.param_detection.get_dict()['f_start']
		f_stop = self.param_detection.get_dict()['f_stop']
		if self.mode == 'all signal' :
			self.list_oscillation = []
			if only_by_zone :
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						AND oscillation.freq_max > %s
						AND oscillation.freq_max < %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql(query , (self.id_electrode, f_start, f_stop, t_start, t_stop )  )
			else :
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						"""
				id_oscillations, = sql(query , (self.id_electrode)  )
			for id_oscillation in id_oscillations :
				os = Oscillation()
				os.load_from_db(id_oscillation)
				self.list_oscillation.append(os)
		
		if self.mode == 'around epoch' :
			if self.epoch is None : return
			self.list_oscillation = [ ]
			t_start = self.param_window.get_dict()['t_start']
			t_stop = self.param_window.get_dict()['t_stop']
			if only_by_zone :
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						AND oscillation.freq_max > %s
						AND oscillation.freq_max < %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql( query , (self.id_electrode, f_start,  f_stop, self.epoch.begin+t_start , self.epoch.begin+t_stop ) )
			else :
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE oscillation.id_electrode = %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql( query , (self.id_electrode, self.epoch.begin+t_start , self.epoch.begin+t_stop ) )
			
			for id_oscillation in id_oscillations :
				os = Oscillation()
				os.load_from_db(id_oscillation)
				os.shift_t0 = self.elec2.shift_t0
				os.time_start -= self.epoch.begin
				os.time_stop -= self.epoch.begin
				os.time_max -= self.epoch.begin
				os.line_t = os.line_t - int((self.epoch.begin+self.elec2.shift_t0-self.elec.shift_t0)*self.elec.fs)

				self.list_oscillation.append(os)
			
		self.refresh_plot()
	#------------------------------------------------------------------------------
	def reload_from_db_all(self) :
		self.reload_from_db( only_by_zone = False)
		
		
	#------------------------------------------------------------------------------
	def reload_from_db_only_zone(self):
		self.reload_from_db( only_by_zone = True)
		
	
	#------------------------------------------------------------------------------
	def deleteSelection(self) :
		mb = QMessageBox.warning(self,self.tr('Delete'),self.tr("You want to delete theses oscillation ?"), 
				QMessageBox.Ok ,QMessageBox.Cancel  | QMessageBox.Default  | QMessageBox.Escape,
				QMessageBox.NoButton)
		if mb == QMessageBox.Cancel : return
		toremove = [ ]
		for item in  self.treeview_oscillation.selectedItems() :
			pos = self.treeview_oscillation.indexFromItem(item).row()
			toremove.append(self.list_oscillation[pos])
		for r in toremove :
			self.list_oscillation.remove(r)
		self.refresh_plot()
	
	#------------------------------------------------------------------------------
	def contextMenuTreeviewOscillation(self , point) :
		for item in  self.treeview_oscillation.selectedItems() :
			pos = self.treeview_oscillation.indexFromItem(item).row()
			menu = QMenu()
			act = menu.addAction(self.tr('Delete'))
			self.connect(act,SIGNAL('triggered()') ,self.deleteSelection)
			act = menu.addAction(self.tr('Recompute'))
			self.connect(act,SIGNAL('triggered()') ,self.recompute_selection)
			act = menu.addAction(self.tr('Clean'))
			self.connect(act,SIGNAL('triggered()') ,self.clean_selection)
			menu.exec_(self.cursor().pos())
	
	#------------------------------------------------------------------------------
	def zoomOscillationOnDoubleClick(self,item):
		 # push the current view to define home if stack is empty
		if self.figToolBar._views.empty(): self.figToolBar.push_current()
		pos = self.treeview_oscillation.indexFromItem(item).row()
		osc=self.list_oscillation[pos]
		fmin=osc.line_f.min()*osc.df
		fmax=osc.line_f.max()*osc.df
		delta_f=max(2.5,(fmax-fmin)/3.)
		delta_t=(osc.time_stop-osc.time_start)/10.
		self.ax1.set_ylim(max(0.,fmin-delta_f),fmax+delta_f)
		self.ax1.set_xlim(osc.time_start-delta_t,osc.time_stop+delta_t,emit=True)
		# Redraw the figure with new limits
		self.figToolBar.draw()
		# push the current view to add new view in the toolbar history
		self.figToolBar.push_current()
		
	#------------------------------------------------------------------------------
	def selectionOscillationChanged(self) :
		for l in self.list_line1 :
			l.set_linewidth = 2
			l.set_color = 'm'
		for l in self.list_line2 :
			l.set_linewidth = 2
			l.set_color = 'm'
		for item in  self.treeview_oscillation.selectedItems() :
			pos = self.treeview_oscillation.indexFromItem(item).row()
			self.list_line1[pos].set_linewidth = 5
			self.list_line2[pos].set_linewidth = 5
			self.list_line1[pos].set_color = 'w'
			self.list_line2[pos].set_color = 'w'
			#self.list_line1[pos].draw()
			#self.list_line2[pos].draw()
			#self.list_line1[pos].update()
			#self.list_line2[pos].update()
			
		#self.canvas1.draw_idle()
		#self.canvas2.draw_idle()
		#self.canvas.draw()
		self.canvas.draw_idle()
		#self.canvas2.draw()
		#redraw_in_frame
		#redraw_in_frame
	
	#------------------------------------------------------------------------------
	def getSelectedEpoch(self) :
		tree = self.queryBoxEpoch.treeview_result
		sel = tree.selectedItems()
		if len(sel) ==0 : return None
		id_epoch = int(sel[0].text(0))
		epoch = Epoch()
		epoch.load_from_db(id_epoch)
		return epoch
	
	#------------------------------------------------------------------------------
	def reload_epoch_and_sub_elec(self) :
			epoch = self.getSelectedEpoch()
			if epoch is None :
				self.epoch = None
				self.elec2 = None
			else :
				self.epoch = epoch
				t_start = self.param_window.get_dict()['t_start']
				t_stop = self.param_window.get_dict()['t_stop']
				self.elec2 = Electrode()
				self.elec2.id_electrode = self.elec.id_electrode
				self.elec2.fs = self.elec.fs
				
				self.elec2.label = self.elec.label
				t = self.elec.t()
				self.elec2.signal = self.elec.signal[ (t >= epoch.begin+t_start) & (t <= epoch.begin+t_stop)]
				self.elec2.shift_t0 = (t-epoch.begin)[(t-epoch.begin)>=t_start][0]

	
	#------------------------------------------------------------------------------
	def changeEpoch(self) :
		#self.list_oscillation = []
		self.reload_epoch_and_sub_elec()
		self.reload_from_db_only_zone()
		self.refresh_plot()
	
	#------------------------------------------------------------------------------
	def refresh_plot(self, refresh_xlim = True) :
		self.ax1.clear()
		self.ax2.clear()
		self.ax_colorbar.clear()
		
		if self.mode == 'all signal' :
			self.elec2 = self.elec
		if self.mode == 'around epoch' :
			self.reload_epoch_and_sub_elec()
			if self.epoch is None : return
		
		# plot signal
		d = self.param_filter.get_dict()
		f_low = d['f_low']/(self.elec2.fs/2)
		f_hight = d['f_hight']/(self.elec2.fs/2)
		sig = fft_filter(self.elec2.signal,f_low =f_low,f_hight=f_hight )
		self.ax2.plot(self.elec2.t(),sig , 'b' , label=None)
		self.ax2.nb_plot_electrode_sig = 1
		
		#plot scalogram
		self.tf = TimeFreq(self.elec2 , **self.param_scalogram.get_dict() )
		self.tf.tf_map(recompute = True )
		self.tf.plot(ax = self.ax1 , colorbar = False)
		im = self.ax1.get_images()[0]
		self.fig.colorbar(im,self.ax_colorbar ,orientation='horizontal')
		
		# rectangle for study zone
		d = self.param_detection.get_dict()
		x1,x2,y1,y2 = d['t_start'], d['t_stop'], d['f_start'], d['f_stop']
		x1,x2,y1,y2 = max(x1 , self.elec2.t()[0]), min(x2, self.elec2.t()[-1] ) ,  y1, y2
		rect = pylab.Rectangle((x1,y1) ,x2-x1 , y2-y1 , edgecolor = 'm' , fill = False , linewidth = 3)
		self.ax1.add_patch(rect)
		# rectangle for reference zone
		d = self.param_tresh.get_dict()
		x1,x2,y1,y2 = d['t1'], d['t2'], d['f1'], d['f2']
		x1,x2,y1,y2 = max(x1 , self.elec2.t()[0]), min(x2, self.elec2.t()[-1] ) ,  y1, y2
		rect = pylab.Rectangle((x1,y1) ,x2-x1 , y2-y1 , edgecolor = 'w' , fill = False, linewidth = 3)
		self.ax1.add_patch(rect)

		self.treeview_oscillation.clear()
		self.list_line1= []
		self.list_line2= []
		for o,os in enumerate(self.list_oscillation) :
			
			self.list_line1 += os.plot_oscillation(self.ax1)
			self.list_line2 += os.plot_oscillation(self.ax2)
			
			item = QTreeWidgetItem(["oscillation %s" % o  ,str(os.time_max) , str(os.freq_max) , str(os.val_max) ] )
			#item.setBackground(0, QBrush( QColor(self.cmap(c))  ) )
			#pix = QPixmap(10,10 )
			#pix.fill(QColor( self.cmap(c)[0]*255 , self.cmap(c)[1]*255 , self.cmap(c)[2]*255  ))
			#icon = QIcon(pix)
			#item.setIcon(0,icon)
			self.treeview_oscillation.addTopLevelItem(item)
		
		self.canvas.draw_idle()
		
	
	#------------------------------------------------------------------------------
	# Tab scalogram
	#------------------------------------------------------------------------------
		
		
	#------------------------------------------------------------------------------
	# Tab Oscillation detection
	#------------------------------------------------------------------------------
	
	#------------------------------------------------------------------------------
	def refresh_winref(self) :
		d = self.param_tresh.get_dict()
		d2 = self.param_detection.get_dict()
		if isnan(d['abs_tresh']) :
			if self.elec2 is None :
				self.labelWinref.setText('')
				return None
			tfreq = TimeFreq(self.elec2 , t_start = d['t1'], t_stop = d['t2'] , f_start = d['f1'] , f_stop = d['f2'] ,
									f0 = d2['f0'] ,normalisation = d2['normalisation'],  df_sub = d2['df_sub'])
			mean_winref  = mean(abs(tfreq.tf_map()))
			std_winref  = std(abs(tfreq.tf_map()))
			
			abs_tresh =  mean_winref + d['std_tresh'] * std_winref
			print 'mean_winref' , mean_winref , 'std_winref' , std_winref , 'abs_tresh' , abs_tresh
			self.labelWinref.setText('Equivalent '+str(abs_tresh))
			return abs_tresh
		else : 
			self.labelWinref.setText('')
			return d['abs_tresh']
	
	#------------------------------------------------------------------------------
	def show_max(self) :
		for l in self.line_max:
			if l in self.ax1.lines : self.ax1.lines.remove(l)
		
		#tfreq = TimeFreq(self.elec2 ,  **self.param_detection.get_dict())
		
		d1 = self.param_scalogram.get_dict()
		d2 = self.param_detection.get_dict()
		d2['fs_sub'] = max(d1['f_stop']*2,d2['f_stop']*2)
		tfreq = TimeFreq(self.elec2 ,  **d2)
		maxt,maxf = tfreq.local_max_2D_sub(
			abs_tresh = self.refresh_winref(),
			min_dt = None,min_df = None)
		self.line_max = self.ax1.plot(maxt,maxf,'wo')
		self.canvas.draw_idle()

	#------------------------------------------------------------------------------
	def clean_selection(self) :
		list_oscillation = []
		for item in  self.treeview_oscillation.selectedItems() :
			pos = self.treeview_oscillation.indexFromItem(item).row()
			osci = self.list_oscillation[pos]
			list_oscillation.append(osci)
		for osci in list_oscillation :
			pos = self.list_oscillation.index(osci)
			del self.list_oscillation[pos]
		
		self.list_oscillation += self.tf.clean_line(list_oscillation, **self.param_clean_line.get_dict() )
		self.refresh_plot()
		
		

	#------------------------------------------------------------------------------
	def recompute_selection(self) :
		for item in  self.treeview_oscillation.selectedItems() :
			pos = self.treeview_oscillation.indexFromItem(item).row()
			
			#tfreq = TimeFreq(self.elec2 ,  **self.param_detection.get_dict())
			
			d1 = self.param_scalogram.get_dict()
			d2 = self.param_detection.get_dict()
			d2['fs_sub'] = max(d1['f_stop']*2,d2['f_stop']*2)
			tfreq = TimeFreq(self.elec2 ,  **d2)
			
			osci = self.list_oscillation[pos]
			self.list_oscillation[pos] = tfreq.find_oscillation_from_max([osci.time_max] , [osci.freq_max] , self.refresh_winref())[0]
			
		self.refresh_plot()

	
	#------------------------------------------------------------------------------
	def reset_recompute(self) :
		#abs_tresh = self.refresh_winref()
		#tfreq = TimeFreq(self.elec2 ,  **self.param_detection.get_dict())
		#list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh )
		#self.list_oscillation = tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		#self.list_oscillation = tfreq.clean_line(self.list_oscillation,**self.param_clean_line.get_dict())
		#self.refresh_plot()
		
		abs_tresh = self.refresh_winref()
		d1 = self.param_scalogram.get_dict()
		d2 = self.param_detection.get_dict()
		d2['fs_sub'] = max(d1['f_stop']*2,d2['f_stop']*2)
		tfreq = TimeFreq(self.elec2 ,  **d2)
		list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh  , min_dt = None,min_df = None)
		self.list_oscillation = tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		self.list_oscillation = tfreq.clean_line(self.list_oscillation,**self.param_clean_line.get_dict())
		self.refresh_plot()
		
		
	
	#------------------------------------------------------------------------------
	def recompute_and_add(self) :
		#abs_tresh = self.refresh_winref()
		#tfreq = TimeFreq(self.elec2 ,  **self.param_detection.get_dict())
		#list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh )
		#self.list_oscillation += tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		#self.refresh_plot()
		
		abs_tresh = self.refresh_winref()
		d1 = self.param_scalogram.get_dict()
		d2 = self.param_detection.get_dict()
		d2['fs_sub'] = max(d1['f_stop']*2,d2['f_stop']*2)
		tfreq = TimeFreq(self.elec2 ,  **d2)
		list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh  , min_dt = None,min_df = None)
		self.list_oscillation += tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		self.refresh_plot()
		
		
	#------------------------------------------------------------------------------
	def recompute_and_add_and_clean(self) :
		#abs_tresh = self.refresh_winref()
		#tfreq = TimeFreq(self.elec2 ,  **self.param_detection.get_dict())
		#list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh )
		#self.list_oscillation += tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		#self.refresh_plot()
		
		abs_tresh = self.refresh_winref()
		d1 = self.param_scalogram.get_dict()
		d2 = self.param_detection.get_dict()
		d2['fs_sub'] = max(d1['f_stop']*2,d2['f_stop']*2)
		tfreq = TimeFreq(self.elec2 ,  **d2)
		list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh )
		self.list_oscillation += tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
		self.refresh_plot()
		

#------------------------------------------------------------------------------
def OpenWindowEditOscillation(parent , id_electrode) :
	w = WindowEditOscillation(parent=parent ,id_electrode = id_electrode)
	w.show()

#------------------------------------------------------------------------------
def OpenWindowEditOscillationByEpoch(parent , id_electrode) :
	w = WindowEditOscillation(parent=parent ,id_electrode = id_electrode , mode = 'around epoch')
	w.show()


#------------------------------------------------------------------------------
class  ComputeOscillationElectrodeDialog(ComputeElectrodeDialog) :
	#------------------------------------------------------------------------------
	def __init__(self, parent=None ):
		ComputeElectrodeDialog.__init__(self, parent)
		
		list_param = 		['f_start'	,	'f_stop',		'df_sub' ,			't_start',		't_stop',			'dt',				'f0',	'normalisation'	]
		default_param  =	[5.			,	100.,		.5 ,					-inf ,			inf,				0.000001,	2.5,	0.				]
		list_label = 			['Freq start' ,	'Freq stop' , 	'Freq precision' , 	'Time start' ,	'Time stop' ,		'Time precision',	'f0',	'Normalisation'	]
		self.param_morlet = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'morlet scalogram' )
		self.v_param.addWidget(self.param_morlet)
		
		list_param = 		['abs_tresh',		'std_tresh',						't1' ,			't2' ,			'f1' ,			'f2'				]
		default_param =		[nan,			14.,								-inf,			0.,				35.,				90.				]
		list_label =			['Abs treshold',	'Std tresh (if abs tresh is null)',	'Window ref.t1' ,	'Window ref.t2' ,	'Window ref.f1' ,	'Window ref.f2'	]
		self.param_tresh = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'treshold oscillation' )
		self.v_param.addWidget(self.param_tresh)
		
		list_param = 		[ 'min_size_cycle' , 'eliminate_simultaneous' , 	'regroup_overlap',	 'eliminate_partial_overlap'   ]
		default_param  =	[3.,				  True,						True,				True						]
		list_label = 			['Min nb of cycle' , 'Eliminate simultaneous', 	'Regroup overlap' ,	'Eliminate partial overlap' 	]
		self.param_clean_line = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'clean line' )
		self.v_param.addWidget(self.param_clean_line)
		
		list_param = 		[ 'delete_old' 	  				]
		default_param  =	[ False							]
		list_label = 		['Delete old oscillation (in zone)'	]
		self.param_database = ParamWidget(list_param , default_param =default_param , list_label=list_label  )
		self.v2.addWidget(self.param_database)
		
		
	#------------------------------------------------------------------------------
	def compute_electrode(self) :
		query = self.queryResultBox.get_query()
		id_electrodes, = sql(query)
		for id_electrode in id_electrodes :
			elec = Electrode()
			elec.load_from_db(id_electrode)
			fs = elec.fs
			
			abs_tresh = self.param_tresh.get_dict()['abs_tresh']
			if isnan(abs_tresh) :
				d = self.param_tresh.get_dict()
				t1 = d['t1']
				t2 = d['t2']
				f1 = d['f1']
				f2 = d['f2']
				#d['t_start'] , d['t_stop'] , d['f_start'] , d['f_stop'] = t1 , t2 , f1 , f2
				tfreq = TimeFreq(elec , t_start = t1, t_stop = t2 , f_start = f1 , f_stop = f2)
				#ind_t, =  where((tfreq.t()>=t1) & (tfreq.t()<=t2))
				#ind_f, = where((tfreq.f()>=f1) & (tfreq.f()<=f2))
				#self.mean_winref  = mean(abs(tfreq.tf_map())[ind_t[0]:ind_t[-1] ,ind_f[0]:ind_f[-1] ])
				#self.std_winref  = std(abs(tfreq.tf_map())[ind_t[0]:ind_t[-1] ,ind_f[0]:ind_f[-1] ])
				mean_winref  = mean(abs(tfreq.tf_map()))
				std_winref  = std(abs(tfreq.tf_map()))
				std_tresh = self.param_tresh.get_dict()['std_tresh']
				abs_tresh = mean_winref + std_tresh * std_winref
				
			#print 'abs_tresh',abs_tresh
			
			tfreq = TimeFreq(elec ,  **self.param_morlet.get_dict())
			list_max_t , list_max_f = tfreq.local_max_2D_sub( abs_tresh =  abs_tresh )
			list_oscillation = tfreq.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)
			list_oscillation = tfreq.clean_line(list_oscillation,**self.param_clean_line.get_dict())
			
			
			Oscillation().create_table()
			
			
			#delete old oscillation
			if self.param_database.get_dict()['delete_old'] :
				
				d = self.param_morlet.get_dict()
				query = """
						SELECT id_oscillation
						FROM oscillation
						WHERE id_electrode = %s'
						AND oscillation.freq_max > %s
						AND oscillation.freq_max < %s
						AND oscillation.time_max > %s
						AND oscillation.time_max < %s
						"""
				id_oscillations, = sql(query , (elec.id_electrode, d['f_start'], d['f_stop'], d['t_start'], d['_stop']) )
				for id_oscillation in id_oscillations :
					os = Oscillation()
					os.id_oscillation = id_oscillation
					os.id_principal = id_oscillation
					os.delete_from_db_and_child(dict_hierarchic_class )
		
			#create new ones
			for o,os in enumerate(list_oscillation) :
				os.id_principal = None
				os.id_oscillation = None
				os.id_trial = elec.id_trial
				os.id_electrode = elec.id_electrode
				id_oscillation = os.save_to_db()



if __name__ == "__main__":
	pass

