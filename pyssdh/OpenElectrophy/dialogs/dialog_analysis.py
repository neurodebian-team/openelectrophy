# -*- coding: utf-8 -*-

"""
dialogs for lauching analysis

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



import sys , os

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from pyssdh.core.gui import *

from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.analysis import list_analysis , dict_analysis

import datetime
import pylab

import threading

#------------------------------------------------------------------------------
class Analysis(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg) :
		database_storage.__init__(self , **karg)
		
	#------------------------------------------------------------------------------
	table_name = 'analysis'
	list_field = [		'name',
					'type_analysis',
					'state',
					'info',
					'datetime_creation',
					'datetime_start',
					'datetime_finished',
					'param',
					'list_query' ,
					'result'
					
				]
	field_type = [	'TEXT',
					'TEXT',
					'TEXT',
					'TEXT',
					'DATETIME',
					'DATETIME',
					'DATETIME',
					'PYOBJECT',
					'PYOBJECT',
					'PYOBJECT'
				]
	list_table_child = []
	#------------------------------------------------------------------------------
	def new(self,**karg):
		for field , value in karg.iteritems() :
			self.__dict__[field] = value
	
	#------------------------------------------------------------------------------
	def compute(self , store_dir = None) :
		if self.id_analysis is None :return
		if self.state != 'not computed' :return
		self.state = 'in computation'
		self.datetime_start = datetime.datetime.now()
		self.save_to_db()
		
		module_analysis = dict_analysis[self.type_analysis]
		result = module_analysis.compute(self.list_query , **self.param )
		
		self.result = result
		self.datetime_finished = datetime.datetime.now()
		self.state = 'computed'
		self.save_to_db()
		
		if store_dir is not None :
			if os.path.isdir(store_dir) :
				list_fig = [  pylab.figure() for n in range(module_analysis.n_fig) ]
				module_analysis.plot( list_fig = list_fig , **self.result )
				for f,fig in enumerate(list_fig) :
					name = datetime.datetime.now().strftime("%Y-%m-%d %Hh%Mm%S")+' '+\
								self.type_analysis+' '+self.name+' '+str(f+1)
					fig.savefig( os.path.join(store_dir ,name) )
	
	#------------------------------------------------------------------------------
	def plot(self , list_fig = None) :
		if self.state != 'computed' : return
		module_analysis = dict_analysis[self.type_analysis]
		
		module_analysis.plot( list_fig = list_fig , **self.result )

#------------------------------------------------------------------------------
class ThreadComputeAnalysis(threading.Thread) :
	#------------------------------------------------------------------------------
	def __init__(self , list_id_analysis = [] , store_dir = None ) :
		threading.Thread.__init__(self)
		self.list_id_analysis = list_id_analysis
		self.store_dir = store_dir
		
	#------------------------------------------------------------------------------
	def run(self) :
		for id_analysis in self.list_id_analysis :
			ana = Analysis(id_analysis = id_analysis)
			ana.compute( store_dir = self.store_dir )
			
#------------------------------------------------------------------------------
class WidgetAnalysis(QFrame) :
	#------------------------------------------------------------------------------
	def __init__(self, analysis_name , parent = None ):
		QFrame.__init__(self, parent )
		
		self.module_analysis = dict_analysis[analysis_name]
		
		self.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		
		h = QHBoxLayout()
		mainLayout.addLayout(h)
		
		v1 = QVBoxLayout()
		h.addLayout(v1)
		v1.addWidget(QLabel(self.module_analysis.analysis_name))
		
		gl = QGridLayout()
		v1.addLayout(gl)
		gl.addWidget(QLabel('Name'),0,0)
		self.editName = QLineEdit('analysis name')
		gl.addWidget(self.editName,0,1)
		gl.addWidget(QLabel('Info'),1,0)
		self.editInfo = QTextEdit('info')
		gl.addWidget(self.editInfo,1,1)
		
		v2 = QVBoxLayout()
		h.addLayout(v2)
		#list query
		self.listQueryResultBox = [ ]
		for t,table in enumerate(self.module_analysis.list_table_query) :
			v2.addWidget(QLabel(self.module_analysis.list_table_title[t]))
			
			self.listQueryResultBox +=  [ QueryResultBox(table = table, field_list = [ 'id_'+table  ] ) ]
			v2.addWidget(self.listQueryResultBox[-1])
		
		#parameters
		v3 = QVBoxLayout()
		h.addLayout(v3)
		self.paramWidget = ParamWidget(self.module_analysis.list_param , 
											default_param =self.module_analysis.default_param , 
											list_label= self.module_analysis.list_label , 
											family = self.module_analysis.analysis_name )
		v3.addWidget(self.paramWidget)
		
		
		
		

#------------------------------------------------------------------------------
class WindowOneAnalysis(QWidget) :
	#------------------------------------------------------------------------------
	def __init__(self, analysis_name , parent = None ):
		QWidget.__init__(self, parent )
		self.setWindowFlags(Qt.Window)
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		self.widgetAnalysis = WidgetAnalysis( analysis_name )
		mainLayout.addWidget(self.widgetAnalysis)
		
		self.setWindowTitle (analysis_name)
		
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		self.buttonAdd = QPushButton('Add list')
		self.buttonAdd.setIcon(dict_icon['add'])
		h.addWidget(self.buttonAdd)
		self.buttonComputeNow = QPushButton('Compute now')
		self.buttonComputeNow.setIcon(dict_icon['exec'])
		h.addWidget(self.buttonComputeNow)
		h.addStretch(10)
		self.buttonQuit = QPushButton('Quit')
		self.buttonQuit.setIcon(dict_icon['fileclose'])
		h.addWidget(self.buttonQuit)
		frame.setLayout(h)
		mainLayout.addWidget(frame)
		
		self.connect(self.buttonAdd , SIGNAL('clicked()') , self.addToList)
		self.connect(self.buttonComputeNow , SIGNAL('clicked()') , self.computeNow)
		self.connect(self.buttonQuit , SIGNAL('clicked()') , self.close)
		
	#------------------------------------------------------------------------------
	def addToList(self) :
		ana = Analysis()
		
		ana.name = unicode(self.widgetAnalysis.editName.text ())
		ana.type_analysis = self.widgetAnalysis.module_analysis.analysis_name
		ana.state = 'not computed'
		ana.info =  unicode(self.widgetAnalysis.editInfo.toPlainText ())
		ana.datetime_creation = datetime.datetime.now()
		ana.param = self.widgetAnalysis.paramWidget.get_dict()
		ana.list_query = [ queryBox.get_query()  for queryBox in self.widgetAnalysis.listQueryResultBox ]
		id_analysis = ana.save_to_db()
		
		return id_analysis
		
	#------------------------------------------------------------------------------
	def computeNow(self) :
		id_analysis = self.addToList()
		ana = Analysis( id_analysis = id_analysis )
		ana.compute()

#------------------------------------------------------------------------------
class WindowListAnalysis(QWidget) :
	#------------------------------------------------------------------------------
	list_field = [ 'id_analysis' , 'name','state','info', 'datetime_creation','datetime_start','datetime_finished' ]
	#------------------------------------------------------------------------------
	def __init__(self , parent =None ) :
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		Analysis().create_table()
		
		self.setWindowTitle (self.tr('List of analysis'))
		
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		
		spv = QSplitter(Qt.Vertical)
		spv.setOpaqueResize(False)
		mainLayout.addWidget(spv , 1)
		
		frame = QFrame()
		spv.addWidget(frame)
		v = QVBoxLayout()
		frame.setLayout(v)
		h = QHBoxLayout()
		v.addLayout(h)
		
		self.comboView = QComboBox()
		h.addWidget( self.comboView )
		list_label = ['Show All' ,'Show "in computation"' , 'Show "computed"' , 'Show "not computed"' ]
		list_cond = [ ' 1  ' , ' analysis.state = "in computation"  ',' analysis.state = "computed" ', ' analysis.state = "not computed" ']
		self.dict_view = dict(zip(list_label , list_cond))
		self.comboView.addItems(list_label)
		self.connect(self.comboView,SIGNAL('currentIndexChanged( int  )') , self.refresh_tree )
		bt_refresh = QPushButton('')
		bt_refresh.setIcon(dict_icon['reload'])
		bt_refresh.setMaximumSize(20,20)
		h.addWidget(bt_refresh)
		self.connect(bt_refresh,SIGNAL("clicked()"), self.refresh_tree)
		
		self.treeWidget = QTreeWidget()
		self.treeWidget.setColumnCount(len(self.list_field))
		self.treeWidget.setHeaderLabels(self.list_field)
		v.addWidget(self.treeWidget)
		self.treeWidget.setSelectionMode(QAbstractItemView.ExtendedSelection)
		self.treeWidget.setContextMenuPolicy(Qt.CustomContextMenu)
		self.connect(self.treeWidget,	SIGNAL('customContextMenuRequested( const QPoint &)'),
								self.contextMenuTreeview)
		
		frame = QFrame()
		spv.addWidget(frame)
		gl = QGridLayout()
		frame.setLayout(gl)
		
		self.checkStoreInRep = QCheckBox('Store figure in directory : ')
		self.checkStoreInRep.setChecked(False)
		gl.addWidget(self.checkStoreInRep,0,0)
		self.chooseDir = ChooseDirWidget(family = 'path for figure')
		gl.addWidget(self.chooseDir,0,1)
		
		self.checkMultiThread = QCheckBox('Compute analysis in diferent thread')
		gl.addWidget(self.checkMultiThread,1,0)
		
		bt_compute = QPushButton('Compute selected')
		bt_compute.setIcon(dict_icon['exec'])
		gl.addWidget(bt_compute , 2,0)
		self.connect(bt_compute,SIGNAL("clicked()"), self.compute_selected)
		
		bt_delete = QPushButton('Delete selected')
		bt_delete.setIcon(dict_icon['emptytrash'])
		gl.addWidget(bt_delete , 2,1)
		self.connect(bt_delete,SIGNAL("clicked()"), self.delete_selected)
		
		bt_plot = QPushButton('Plot selected')
		bt_plot.setIcon(dict_icon['plot'])
		gl.addWidget(bt_plot , 2,2)
		self.connect(bt_plot,SIGNAL("clicked()"), self.plot_selected)
		
		
		
		self.refresh_tree()
		
		self.list_thread = [ ]
	
	#------------------------------------------------------------------------------
	def cancel(self) :
		pass
		
	#------------------------------------------------------------------------------
	def	contextMenuTreeview(self) :
		list_action = [self.tr('delete') , self.tr('compute') , self.tr('plot result') ]
		list_call = [self.delete_selected , self.compute_selected , self.plot_selected ]
		menu = QMenu()
		for a,action in enumerate(list_action) :
			act = menu.addAction(action)
			self.connect(act, SIGNAL('triggered()') , list_call[a])
		act = menu.exec_(self.cursor().pos())
		
	#------------------------------------------------------------------------------
	def refresh_tree(self) :
		
		k = str(self.comboView.currentText())
		cond = self.dict_view[k]
		
		self.treeWidget.clear()
		list_type, = sql( 'SELECT type_analysis FROM analysis WHERE %s GROUP BY type_analysis' % cond )
		items_analysis = [ ]
		for t , type_analysis in enumerate(list_type) :
			item = QTreeWidgetItem(None, [type_analysis]+['']*(len(self.list_field)-1))
			query = """
					SELECT id_analysis
					FROM analysis
					WHERE type_analysis = %s
					"""
			id_analysiss, = sql(query,type_analysis)
			child = [ ]
			for id_analysis in id_analysiss :
				ana = Analysis(id_analysis = id_analysis)
				child.append(QTreeWidgetItem(None, [ str(ana[field]) for field in self.list_field ]))
			item.addChildren(child)
			items_analysis.append(item)
		
		self.treeWidget.insertTopLevelItems(0, items_analysis)
	
	#------------------------------------------------------------------------------
	def compute_selected(self) :
		if self.checkStoreInRep.isChecked() :
			store_dir = self.chooseDir.get_dir()
		else :
			store_dir = None
		list_id_analysis = [ ] 
		for item in  self.treeWidget.selectedItems() :
			try :
				list_id_analysis.append(int(item.text(0)))
			except :
				continue
		print list_id_analysis
		if self.checkMultiThread.isChecked() :
			for id_analysis in list_id_analysis :
				thre = ThreadComputeAnalysis(list_id_analysis = [id_analysis] , store_dir = store_dir)
				self.list_thread.append(thre)
				thre.start()
		else :
			thre = ThreadComputeAnalysis(list_id_analysis = list_id_analysis, store_dir = store_dir)
			self.list_thread.append(thre)
			thre.start()
			
	
	#------------------------------------------------------------------------------
	def delete_selected(self) :
		for item in  self.treeWidget.selectedItems() :
			try :
				id_analysis = int(item.text(0))
			except :
				continue
			ana = Analysis(id_analysis = id_analysis)
			ana.delete_from_db_and_child(dict_hierarchic_class)
			#self.treeWidget.model()
			del item

	#------------------------------------------------------------------------------
	def plot_selected(self) :
		for item in  self.treeWidget.selectedItems() :
			try : id_analysis = int(item.text(0))
			except : continue
			ana = Analysis(id_analysis = id_analysis)
			if ana.state != 'computed' : continue
			module_analysis = dict_analysis[ana.type_analysis]
			list_fig = [ ]
			list_canvas = [ ]
			for f in range(module_analysis.n_fig) :
				canvas = MyMplCanvas()
				#del canvas.ax
				canvas.fig.delaxes(canvas.ax)
				list_canvas +=  [ canvas ]
				list_fig += [ canvas.fig ]
			ana.plot( list_fig = list_fig)
			for canvas in list_canvas :
				w = QWidget(self)
				w.setWindowFlags(Qt.Window)
				w.setWindowTitle (ana.type_analysis+' - '+ana.name)
				h = QVBoxLayout()
				h.addWidget(canvas)
				figToolBar = MyNavigationToolbar(canvas , canvas , direction = 'h')
				h.addWidget(figToolBar)
				w.setLayout(h)
				w.show()


