# -*- coding: utf-8 -*-

"""
window for context menu

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from pyssdh.core.gui import *
from pyssdh.OpenElectrophy.core_classes import *

from pyssdh.OpenElectrophy.dialogs.dialog_edit_spike import *

from scipy import * 
import pylab

from respiration import *


#------------------------------------------------------------------------------
# For serie
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CreateNewCell(parent , id_serie):
	cell = Cell()
	cell.id_serie = id_serie
	cell.save_to_db()


#------------------------------------------------------------------------------
class MenuEditSpikeSerie(QMenu) :
	#------------------------------------------------------------------------------
	def __init__(self,parent , id_serie) :
		QMenu.__init__(self,parent)
		self.parent = parent
		self.move(self.cursor().pos())
		self.id_serie = id_serie
		self.list_act = [ ]
		
		query = """	SELECT num_channel FROM electrode, trial
					WHERE
					trial.id_trial = electrode.id_trial
					AND trial.id_serie = %s
				"""
		num_channels, = sql(query , (id_serie) )
		num_channels = unique(num_channels)
		num_channels.sort()
		self.num_channels = num_channels
		for num_channel in num_channels :
			act = self.addAction('num_channel %d' % num_channel )
			self.connect(act , SIGNAL('triggered()') , self.editspikeserie)
			self.list_act +=[act]
			
	
	#------------------------------------------------------------------------------
	def editspikeserie(self) :
		if self.sender() in self.list_act :
			ind = self.list_act.index(self.sender())
			num_channel = self.num_channels[ind]
			w = WindowEditSpike(parent=self.parent , id_serie = self.id_serie , num_channel = num_channel )
			w.show()
			

#------------------------------------------------------------------------------
def OpenWindowEditSpikeSerie(parent, id_serie ) :
	menu = MenuEditSpikeSerie(parent , id_serie)
	menu.show()


#------------------------------------------------------------------------------
# For trial
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
class MenuMoveToSerie(QMenu) :
	#------------------------------------------------------------------------------
	def __init__(self,parent , id_trial) :
		QMenu.__init__(self,parent)
		self.move(self.cursor().pos())
		self.id_trial = id_trial
		self.list_act = [ ]
		self.list_id_serie = [ ]
		
		for id_serie,info in sql_pack('SELECT id_serie , info FROM serie') :
			act = self.addAction(str(id_serie)+' '+info)
			self.connect(act , SIGNAL('triggered()') , self.movetoserie)
			self.list_act +=[act]
			self.list_id_serie += [id_serie]
	#------------------------------------------------------------------------------
	def movetoserie(self) :
		if self.sender() in self.list_act :
			ind = self.list_act.index(self.sender())
			id_serie = self.list_id_serie[ind]
			trial = Trial()
			trial.load_from_db(self.id_trial)
			trial.id_serie = id_serie
			trial.save_to_db()
		
#------------------------------------------------------------------------------
def ShowMenuMoveToSerie(parent , id_trial) :
	menu = MenuMoveToSerie(parent , id_trial)
	menu.show()
	



#------------------------------------------------------------------------------
# For cell
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
class MenuMoveToCell(QMenu) :
	#------------------------------------------------------------------------------
	def __init__(self,parent , id_spiketrain) :
		QMenu.__init__(self,parent)
		self.move(self.cursor().pos())
		self.id_spiketrain = id_spiketrain
		self.list_act = [ ]
		self.list_id_cell = [ ]
		query = """SELECT cell.id_cell , cell.info
				FROM cell, spiketrain, trial
				WHERE 
				cell.id_serie = trial.id_serie
				AND spiketrain.id_trial = trial.id_trial
				AND spiketrain.id_spiketrain = %s
				"""
		for id_cell,name in sql_pack(query , (id_spiketrain)) :
			if name is None : name =''
			act = self.addAction(str(id_cell)+' '+name)
			self.connect(act , SIGNAL('triggered()') , self.movetocell)
			self.list_act +=[act]
			self.list_id_cell += [id_cell]
	
	#------------------------------------------------------------------------------
	def movetocell(self) :
		if self.sender() in self.list_act :
			ind = self.list_act.index(self.sender())
			id_cell = self.list_id_cell[ind]
			spiketrain = SpikeTrain()
			spiketrain.load_from_db(self.id_spiketrain)
			spiketrain.id_cell = id_cell
			spiketrain.save_to_db()
		
#------------------------------------------------------------------------------
def ShowMenuMoveToCell(parent , id_spiketrain) :
	menu = MenuMoveToCell(parent , id_spiketrain)
	menu.show()
	
