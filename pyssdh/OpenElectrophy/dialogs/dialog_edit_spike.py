# -*- coding: utf-8 -*-

"""
dialogs for manual spike sorting and detection

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from pyssdh.core.gui import *
from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.core_classes.spike import *
from pyssdh.OpenElectrophy.computing  import spikesorting
from dialog_common import *

from scipy import *
import pylab




#------------------------------------------------------------------------------
class WidgetMultiMethodsParam(QFrame) :
	def __init__(self,  parent = None ,
						list_method = [ ],
						method_name = '' ):
		QFrame.__init__(self, parent)
		
		self.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		self.v1 = QVBoxLayout()
		v1 = self.v1
		self.setLayout(v1)
		
		self.list_method = list_method
		self.method_name = method_name
		
		v1.addWidget(QLabel(self.method_name))
		self.comboBox_method = QComboBox()
		v1.addWidget(self.comboBox_method)
		self.comboBox_method.addItems([ method.name for  method in list_method ])
		
		self.connect(self.comboBox_method,SIGNAL('currentIndexChanged( int  )') , self.comboBoxChangeMethod )
		
		self.paramWidget = None
		
		
		self.comboBoxChangeMethod()
	
	#------------------------------------------------------------------------------
	def comboBoxChangeMethod(self) :
		pos = self.comboBox_method.currentIndex()
		if self.paramWidget is not None :
			self.paramWidget.setVisible(False)
			self.v1.removeWidget(self.paramWidget)
			del self.paramWidget
		method = self.list_method[pos]
		self.paramWidget = ParamWidget(method.list_param ,
										default_param = method.default_param ,
										list_label = method.list_label ,
										family = self.method_name+method.name )
		self.v1.addWidget(self.paramWidget,1)
	
	#------------------------------------------------------------------------------
	def get_method(self) :
		pos = self.comboBox_method.currentIndex()
		method = self.list_method[pos]()
		return method
	
	#------------------------------------------------------------------------------
	def get_dict(self) :
		return dict( zip(self.get_method().list_param , self.paramWidget.get_param() ) )
		
		

#------------------------------------------------------------------------------
class TabSpikeSorting(QTabWidget) :
	#------------------------------------------------------------------------------
	def __init__(self , parent=None ,
					id_electrode=None,
					id_serie = None , num_channel = None ):
		QTabWidget.__init__(self,parent )
		self.setTabPosition(QTabWidget.West)
		
		self.setAttribute(Qt.WA_DeleteOnClose)
		
		self.id_electrode = id_electrode
		self.id_serie = id_serie
		self.num_channel = num_channel

		
		##
		#tab filtering
		fr = QFrame()
		self.addTab(fr,'Filtering')
		self.h1_filtering = QHBoxLayout()
		fr.setLayout(self.h1_filtering)
		
		self.v1_filtering = QVBoxLayout()
		self.h1_filtering.addLayout(self.v1_filtering)
		self.multiMethod_filtering = WidgetMultiMethodsParam(list_method = spikesorting.filtering.list_method , method_name = 'Filetring methods')
		self.v1_filtering.addWidget(self.multiMethod_filtering,1)
		
		##
		#tab detection
		fr = QFrame()
		self.addTab(fr,'Detection')
		self.h1_detection = QHBoxLayout()
		fr.setLayout(self.h1_detection)
		
		self.v1_detection = QVBoxLayout()
		self.h1_detection.addLayout(self.v1_detection)
		list_param =	[ 'waveform_size',			'oversampling'	]
		default_param =	[ 0.004,					1				]
		list_label = 	['Waveform size',			'Oversampling factor for waveform'	]
		self.param_detection_global = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'win size spike sorting' )
		self.v1_detection.addWidget(self.param_detection_global)
		
		
		self.multiMethod_detection = WidgetMultiMethodsParam(list_method = spikesorting.detection.list_method , method_name = 'Detection methods')
		self.v1_detection.addWidget(self.multiMethod_detection , 1)
		
		##
		#tab projection
		fr = QFrame()
		self.addTab(fr,'Projection')
		self.h1_projection = QHBoxLayout()
		fr.setLayout(self.h1_projection)
		
		self.v1_projection = QVBoxLayout()
		self.h1_projection.addLayout(self.v1_projection)
		self.multiMethod_projection = WidgetMultiMethodsParam(list_method = spikesorting.projection.list_method , method_name = 'Projection methods')
		self.v1_projection.addWidget(self.multiMethod_projection,1)
		
		##
		#tab clustering
		fr = QFrame()
		self.addTab(fr,'Clustering')
		self.h1_clustering = QHBoxLayout()
		fr.setLayout(self.h1_clustering)

		self.v1_clustering = QVBoxLayout()
		self.h1_clustering.addLayout(self.v1_clustering)
		self.multiMethod_clustering = WidgetMultiMethodsParam(list_method = spikesorting.clustering.list_method , method_name = 'Clustering methods')
		self.v1_clustering.addWidget(self.multiMethod_clustering,1)
		
		##
		#tab database
		fr = QFrame()
		self.addTab(fr,'Database option')
		self.v1_database = QVBoxLayout()
		self.h2_database = QHBoxLayout()
		self.v1_database.addLayout(self.h2_database)
		fr.setLayout(self.v1_database)
		list_param =	[ 'waveform_size',			'oversampling',								'save_filtered_waveform'	]
		default_param =	[ 0.004,					1,											True						]
		list_label = 	['Waveform size',			'Oversampling factor (in database)',		'Save filterered waveform'	]
		self.param_database = ParamWidget(list_param , default_param =default_param , list_label=list_label , family = 'parameters for save spike in database' )
		self.h2_database.addWidget(self.param_database)
		
		
	#------------------------------------------------------------------------------		
	def load_signal(self) :
		if self.id_electrode is not None :
			# mode one electrode
			self.elec = Electrode()
			self.elec.load_from_db(self.id_electrode)
			self.sig = self.elec.signal
			self.fs = self.elec.fs
			self.list_elec = None
		else :
			# mode all electrode on same serie
			query = """
					SELECT id_electrode
					FROM electrode , trial
					WHERE
					electrode.id_trial = trial.id_trial
					AND trial.id_serie = %s
					AND num_channel = %s
					ORDER BY trial.thedatetime
					"""
			self.list_elec = [ ]
			id_electrodes, = sql(query , (self.id_serie , self.num_channel))
			self.sig = array([])
			for id_electrode in id_electrodes :
				elec = Electrode()
				elec.load_from_db(id_electrode)
				self.list_elec.append(elec)
				self.sig = concatenate((self.sig , elec.signal))
				self.fs = elec.fs
		
		self.t = arange(self.sig.size)/self.fs
		self.pos_spike = [ ]
		self.sig_f = [ ]
		self.waveform = [ ]
		self.waveform_projected = [ ]
		self.cluster = [ ]
		self.waveform_size = None
		self.oversampling = None


	#------------------------------------------------------------------------------
	def computeFiltering(self) :
		fil = self.multiMethod_filtering.get_method()
		karg = self.multiMethod_filtering.get_dict()
		self.sig_f = fil.compute(self.sig , self.fs , **karg)
		
	#------------------------------------------------------------------------------
	def computeDetection(self) :
		self.waveform_size = self.param_detection_global.get_dict()['waveform_size']
		self.oversampling = self.param_detection_global.get_dict()['oversampling']
		
		detec = self.multiMethod_detection.get_method()
		karg = self.multiMethod_detection.get_dict()
		
		self.pos_spike = detec.compute(self.sig_f , self.fs , **karg)
		self.waveform = waveform_extraction(self.pos_spike,self.sig_f, self.fs , self.waveform_size,self.oversampling, full_window = True )
		
	
	#------------------------------------------------------------------------------
	def computeProjection(self) :
		project = self.multiMethod_projection.get_method()
		karg = self.multiMethod_projection.get_dict()
		
		self.waveform_projected = project.compute(self.waveform , self.fs*self.oversampling , **karg)
	
	#------------------------------------------------------------------------------
	def computeClustering(self) :
		clus = self.multiMethod_clustering.get_method()
		karg = self.multiMethod_clustering.get_dict()
		
		self.cluster = clus.compute(self.waveform_projected , **karg)
		
	
	#------------------------------------------------------------------------------
	def recomputeAllSteps(self) :
		self.computeFiltering()
		self.computeDetection()
		self.computeProjection()
		self.computeClustering()
		
	#------------------------------------------------------------------------------
	def save_to_db(self) :
		n_cluster = unique(self.cluster).size
		waveform_size = self.param_database.get_one_param('waveform_size')
		oversampling = self.param_database.get_one_param('oversampling')
		
		if self.id_electrode is not None :
			# mode one electrode
			
			# delete old spiketrain and spike in database
			id_spiketrains, = sql('SELECT id_spiketrain FROM spiketrain WHERE id_electrode = %s'  , self.id_electrode)
			for id_spiketrain in id_spiketrains :
				sptr = SpikeTrain()
				sptr.id_spiketrain = id_spiketrain
				sptr.id_principal = id_spiketrain
				sptr.delete_from_db_and_child(dict_hierarchic_class )
			
			#create new ones
			for n,cl in enumerate(unique(self.cluster)) :
				
				sptr = SpikeTrain()
				sptr.id_trial = self.elec.id_trial
				sptr.id_electrode = self.elec.id_electrode
				sptr.id_cell = None
				sptr.fs = self.elec.fs
				sptr.shift_t0 =  self.elec.shift_t0
				sptr.oversampling = oversampling
				sptr.f_low = None
				sptr.f_hight = None
				sptr.label = u''
				sptr.coment = u''
				id_spiketrain = sptr.save_to_db()
				
				pos = self.pos_spike[self.cluster== cl]
				isi = r_[diff(pos)/float(self.fs) , Inf]
				if self.param_database.get_one_param('save_filtered_waveform') :
					fil = self.multiMethod_filtering.get_method()
					karg = self.multiMethod_filtering.get_dict()
					sig_f = fil.compute(self.sig , self.fs , **karg)
				else : 
					sig_f = self.sig
				waveform = waveform_extraction(pos,sig_f, self.fs , waveform_size,oversampling)
				for s in range(len(pos)) :
					sp = Spike()
					sp.id_spiketrain = id_spiketrain
					sp.id_electrode = self.elec.id_electrode
					sp.pos = pos[s]
					sp.val_max = sig_f[pos[s]]
					sp.waveform = squeeze(waveform[s,:])
					sp.isi = isi[s]
					sp.save_to_db()
		
		else:
			# mode all electrode on same serie
			
			# delete old spiketrain and spike in database
			query = """
					SELECT spiketrain.id_spiketrain
					FROM spiketrain , electrode , trial 
					WHERE
					trial.id_trial = electrode.id_trial
					AND electrode.id_electrode = spiketrain.id_electrode
					AND trial.id_serie = %s
					AND electrode.num_channel = %s
					"""
			id_spiketrains, = sql(query  , (self.id_serie , self.num_channel ))
			for id_spiketrain in id_spiketrains :
				sptr = SpikeTrain()
				sptr.id_spiketrain = id_spiketrain
				sptr.id_principal = id_spiketrain
				sptr.delete_from_db_and_child(dict_hierarchic_class )
			
			#create new cells, spiketrain et spike
			for n,cl in enumerate(unique(self.cluster)) :
				cell = Cell()
				cell.id_serie = self.id_serie
				cell.info = u''
				cell.name = u'Cell %s NumChannel %s' %( n+1 , self.num_channel)
				id_cell = cell.save_to_db()
				
				start = 0
				for e,elec in enumerate(self.list_elec):
					sptr = SpikeTrain()
					sptr.id_trial = elec.id_trial
					sptr.id_electrode = elec.id_electrode
					sptr.id_cell = id_cell
					sptr.fs = elec.fs
					sptr.shift_t0 =  elec.shift_t0
					sptr.oversampling = oversampling
					sptr.f_low = None
					sptr.f_hight = None
					sptr.label = u''
					sptr.coment = u''
					id_spiketrain = sptr.save_to_db()
					
					pos = self.pos_spike[self.cluster== cl]
					pos = pos[ (pos>= start) & (pos<start + elec.signal.size)]
					pos = pos - start
					isi = r_[diff(pos)/float(elec.fs) , Inf]
					if self.param_database.get_one_param('save_filtered_waveform') :
						fil = self.multiMethod_filtering.get_method()
						karg = self.multiMethod_filtering.get_dict()
						sig_f = fil.compute(elec.signal , elec.fs , **karg)
					else : 
						sig_f = elec.signal
					waveform = waveform_extraction(pos,sig_f, self.fs , waveform_size,oversampling)
					for s in range(len(pos)) :
						sp = Spike()
						sp.id_spiketrain = id_spiketrain
						sp.id_electrode = elec.id_electrode
						sp.pos = pos[s]
						sp.val_max = sig_f[pos[s]]
						sp.waveform = squeeze(waveform[s,:])
						sp.isi = isi[s]
						sp.save_to_db()

					start += elec.signal.size
					
	#------------------------------------------------------------------------------
	def reload_from_db(self) :
		if self.id_electrode is not None :
			# mode one electrode
			id_spiketrains, = sql('SELECT id_spiketrain FROM spiketrain WHERE id_electrode = %s'  , self.id_electrode)
			self.pos_spike = array([ ],dtype='i')
			self.cluster = array([ ],dtype='i')
			for i,id_spiketrain in enumerate(id_spiketrains) :
				sptr = SpikeTrain()
				sptr.load_from_db(id_spiketrain)
				pos = sptr.pos_spike()
				self.pos_spike = concatenate((self.pos_spike , pos))
				self.cluster = concatenate((self.cluster , i*ones((len(pos)) , dtype = 'i') ))
		else:
			# mode all electrode on same serie
			query = """
					SELECT spiketrain.id_spiketrain , cell.id_cell , electrode.id_electrode
					FROM spiketrain , electrode , trial , cell
					WHERE
					trial.id_trial = electrode.id_trial
					AND electrode.id_electrode = spiketrain.id_electrode
					AND cell.id_cell = spiketrain.id_cell
					AND trial.id_serie = %s
					AND electrode.num_channel = %s
					ORDER BY cell.id_cell
					"""
			id_spiketrains,id_cells,id_electrodes = sql(query  , (self.id_serie , self.num_channel ))
			self.pos_spike = array([ ],dtype='i')
			self.cluster = array([ ],dtype='i')
			n_cluster = unique(id_cells).size
			
			for i,id_spiketrain in enumerate(id_spiketrains) :
				id_cell,id_electrode = id_cells[i],id_electrodes[i]
				start = 0
				for e,elec in enumerate(self.list_elec):
					if elec.id_electrode == id_electrode : break
					start += elec.signal.size

				sptr = SpikeTrain()
				sptr.load_from_db(id_spiketrain)
				pos = sptr.pos_spike()+start
				self.pos_spike = concatenate((self.pos_spike , pos))
				
				cluster = where(id_cell == unique(id_cells))[0]
				self.cluster = concatenate((self.cluster , cluster*ones((len(pos)) , dtype = 'i') ))
			


#------------------------------------------------------------------------------
class WindowEditSpike(QWidget):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None ,
					id_electrode=None,
					id_serie = None , num_channel = None ):
		
		"""
		2 modes :	work on 1 electrode
					work on all same electrode from a serie ( same cells )
		"""
		
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		SpikeTrain().create_table()
		Spike().create_table()
		
		self.id_electrode = id_electrode
		self.id_serie = id_serie
		self.num_channel = num_channel
		
		
		
		if id_electrode is not None :
			# mode one electrode
			self.setWindowTitle (self.tr('Edit spike for id_electrode=%s' % id_electrode))
		else :
			self.setWindowTitle (self.tr('Edit spike for id_serie=%s num_channel=%s ' % (self.id_serie, self.num_channel ) ))
		
		
		#design
		
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		
		sph = QSplitter(Qt.Horizontal)
		sph.setOpaqueResize(False)
		mainLayout.addWidget(sph , 1)
		
		spv = QSplitter(Qt.Vertical)
		spv.setOpaqueResize(False)
		sph.addWidget(spv)
		
		self.tab = TabSpikeSorting(parent=self, id_electrode = self.id_electrode , 
							id_serie = self.id_serie , num_channel = self.num_channel)
		
		spv.addWidget(self.tab)
		
		##
		#tab filtering
		self.buttonComputeFiltering = QPushButton('Compute filtering')
		self.buttonComputeFiltering.setIcon(dict_icon['redo'])
		self.connect(self.buttonComputeFiltering , SIGNAL('clicked()') , self.computeFiltering)
		self.tab.v1_filtering.addWidget(self.buttonComputeFiltering)
		
		v2 = QVBoxLayout()
		self.tab.h1_filtering.addLayout(v2 , 1)
		self.canvas_filtering = MyMplCanvas( width=2, height=2,nb_ax = 2 , ax_shape = (2,1), sharex= None)
		v2.addWidget(self.canvas_filtering)
		self.ax_signal = self.canvas_filtering.list_ax[0]
		self.canvas_filtering.figure.delaxes(self.canvas_filtering.list_ax[1])
		self.ax_filtered = self.canvas_filtering.figure.add_subplot(2,1,2, sharex = self.ax_signal )
		
		#~ self.ax_filtered = self.canvas_filtering.list_ax[1]
		self.figToolBar = MyNavigationToolbar(self.canvas_filtering , self.canvas_filtering , direction = 'h')
		v2.addWidget(self.figToolBar)
		
		##
		#tab detection
		self.buttonComputeDetection = QPushButton('Compute detection')
		self.buttonComputeDetection.setIcon(dict_icon['redo'])
		self.connect(self.buttonComputeDetection , SIGNAL('clicked()') , self.computeDetection)
		self.tab.v1_detection.addWidget(self.buttonComputeDetection)
		
		v2 = QVBoxLayout()
		self.tab.h1_detection.addLayout(v2 , 1)
		self.canvas_detection = MyMplCanvas( width=2, height=2,nb_ax = 2 , ax_shape = (2,1), sharex= None)
		v2.addWidget(self.canvas_detection) 
		self.ax_detection = self.canvas_detection.list_ax[0]
		self.ax_waveform = self.canvas_detection.list_ax[1]
		self.figToolBar2 = MyNavigationToolbar(self.canvas_detection , self.canvas_detection , direction = 'h')
		v2.addWidget(self.figToolBar2)
		
		##
		#tab projection
		self.buttonComputeProjection = QPushButton('Compute projection')
		self.buttonComputeProjection.setIcon(dict_icon['redo'])
		self.connect(self.buttonComputeProjection , SIGNAL('clicked()') , self.computeProjection)
		self.tab.v1_projection.addWidget(self.buttonComputeProjection)
		
		v2 = QVBoxLayout()
		self.tab.h1_projection.addLayout(v2,1)
		self.canvas_projection = MyMplCanvas( width=2, height=2,nb_ax = 2 , ax_shape = (1,2), sharex= None)
		v2.addWidget(self.canvas_projection)
		self.ax_not_projected = self.canvas_projection.list_ax[0]
		self.ax_projected = self.canvas_projection.list_ax[1]
		self.figToolBar3 = MyNavigationToolbar(self.canvas_projection , self.canvas_projection , direction = 'h')
		v2.addWidget(self.figToolBar3)
		
		##
		#tab clustering
		self.buttonComputeClustering = QPushButton('Compute clustering')
		self.buttonComputeClustering.setIcon(dict_icon['redo'])
		self.connect(self.buttonComputeClustering , SIGNAL('clicked()') , self.computeClustering)
		self.tab.v1_clustering.addWidget(self.buttonComputeClustering)
		
		v2 = QVBoxLayout()
		self.tab.h1_clustering.addLayout(v2,2)
		self.canvas_clustering = MyMplCanvas( width=2, height=2,nb_ax = 3 , ax_shape = (1,3), sharex= None)
		v2.addWidget(self.canvas_clustering)
		self.ax_waveform_sorted = self.canvas_clustering.list_ax[0]
		self.ax_projection_sorted = self.canvas_clustering.list_ax[1]
		self.ax_2_components = self.canvas_clustering.list_ax[2]
		self.figToolBar4 = MyNavigationToolbar(self.canvas_clustering , self.canvas_clustering , direction = 'h')
		v2.addWidget(self.figToolBar4)

		##
		#tab database
		self.canvas_database = MyMplCanvas( width=2, height=2,nb_ax = 1 , ax_shape = (1,1), sharex= None)
		self.ax_waveform_database = self.canvas_database.list_ax[0]
		self.tab.h2_database.addWidget(self.canvas_database)
		self.canvas_database2 = MyMplCanvas( width=2, height=2,nb_ax = 1 , ax_shape = (1,1), sharex= None)
		self.ax_signal_database = self.canvas_database2.list_ax[0]
		self.tab.v1_database.addWidget(self.canvas_database2)
		self.figToolBar5 = MyNavigationToolbar(self.canvas_database2 , self.canvas_database2 , direction = 'h')
		self.tab.v1_database.addWidget(self.figToolBar5)
		
		
		#Treview
		self.treeview_spiketrain = QTreeWidget()
		self.treeview_spiketrain.setColumnCount(3)
		self.tab.v1_database.addWidget(self.treeview_spiketrain)
		#shorcuts
		DeleteShortcut = QShortcut(QKeySequence(Qt.Key_Delete),self)
		self.connect(DeleteShortcut, SIGNAL("activated()"), self.deleteSelection)
		#context menu
		self.treeview_spiketrain.setContextMenuPolicy(Qt.CustomContextMenu)
		self.connect(self.treeview_spiketrain,SIGNAL('customContextMenuRequested( const QPoint &)'),self.contextMenuTreeviewSpikeTrain)
		
		
		
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		self.buttonRecomputeAll = QPushButton('Recompute all steps')
		self.buttonRecomputeAll.setIcon(dict_icon['redo'])
		h.addWidget(self.buttonRecomputeAll)
		h.addStretch(10)
		self.buttonSave = QPushButton('Save to database')
		self.buttonSave.setIcon(dict_icon['filesave'])
		h.addWidget(self.buttonSave)
		self.buttonLoad = QPushButton('Reload from database')
		self.buttonLoad.setIcon(dict_icon['reload'])
		h.addWidget(self.buttonLoad)
		h.addStretch(10)
		self.buttonQuit = QPushButton('Quit')
		self.buttonQuit.setIcon(dict_icon['fileclose'])
		h.addWidget(self.buttonQuit)
		frame.setLayout(h)
		mainLayout.addWidget(frame)

		self.connect(self.buttonRecomputeAll , SIGNAL('clicked()') , self.recomputeAllSteps)
		self.connect(self.buttonSave , SIGNAL('clicked()') , self.save_to_db)
		self.connect(self.buttonLoad , SIGNAL('clicked()') , self.reload_from_db)
		self.connect(self.buttonQuit , SIGNAL('clicked()') , self , SLOT('close()') )
		
		self.setAttribute(Qt.WA_DeleteOnClose)
		#self.connect(self , SLOT('close()'),self.destroy )
		
		self.tab.load_signal()
		
	#------------------------------------------------------------------------------
	def computeFiltering(self) :
		self.tab.computeFiltering()
		self.ax_signal.clear()
		self.ax_signal.plot(self.tab.t , self.tab.sig)
		
		self.ax_filtered.clear()
		self.ax_filtered.plot(self.tab.t , self.tab.sig_f)
		
		self.canvas_filtering.draw_idle()
		
	#------------------------------------------------------------------------------
	def computeDetection(self) :
		self.tab.computeDetection()
		
		self.ax_detection.clear()
		self.ax_waveform.clear()
		
		#self.waveform_size = self.param_detection_global.get_dict()['waveform_size']
		#self.oversampling = self.param_detection_global.get_dict()['oversampling']
		
		#detec = self.multiMethod_detection.get_method()
		#karg = self.multiMethod_detection.get_dict()
		
		#self.pos_spike = detec.compute(self.sig_f , self.fs , **karg)
		#self.waveform = waveform_extraction(self.pos_spike,self.sig_f, self.fs , self.waveform_size,self.oversampling, full_window = True )
		
		self.ax_detection.plot(self.tab.t , self.tab.sig_f , color = 'b')
		self.ax_detection.plot(self.tab.t[self.tab.pos_spike], self.tab.sig_f[self.tab.pos_spike],
							'o', markerfacecolor = 'r' , markeredgecolor = 'k' )
		
		tw = arange(self.tab.waveform.shape[1])/(self.tab.oversampling*self.tab.fs)
		self.ax_waveform.plot(tw,self.tab.waveform.transpose() , color = 'b')

		
		self.canvas_detection.draw_idle()
		
	
	#------------------------------------------------------------------------------
	def computeProjection(self) :
		self.tab.computeProjection()
		self.ax_not_projected.clear()
		self.ax_projected.clear()
		
		tw = arange(self.tab.waveform.shape[1])/(self.tab.oversampling*self.tab.fs)
		self.ax_not_projected.plot(tw,self.tab.waveform.transpose() , color = 'b')
		
		#project = self.multiMethod_projection.get_method()
		#karg = self.multiMethod_projection.get_dict()
		#self.waveform_projected = project.compute(self.waveform , self.fs*self.oversampling , **karg)
		
		self.ax_projected.plot(self.tab.waveform_projected.transpose() , color = 'b' , marker = '.')
		
		self.canvas_projection.draw_idle()
	
	#------------------------------------------------------------------------------
	def computeClustering(self) :
		self.tab.computeClustering()
		self.ax_waveform_sorted.clear()
		self.ax_projection_sorted.clear()
		self.ax_2_components.clear()
		
		#clus = self.multiMethod_clustering.get_method()
		#karg = self.multiMethod_clustering.get_dict()
		#self.cluster = clus.compute(self.waveform_projected , **karg)
		
		tw = arange(self.tab.waveform.shape[1])/(self.tab.oversampling*self.tab.fs)
		cmap = pylab.get_cmap('jet',unique(self.tab.cluster).size)
		for c,cl in enumerate(unique(self.tab.cluster)) :
			ind, = where(cl == self.tab.cluster)
			self.ax_waveform_sorted.plot( tw,self.tab.waveform[ind,:].transpose() , color = cmap(c) )
			self.ax_projection_sorted.plot(self.tab.waveform_projected[ind,:].transpose() ,
								color = cmap(c) , marker = '.')
			if self.tab.waveform_projected.shape[1] >1 :
				self.ax_2_components.plot(self.tab.waveform_projected[ind,0] , self.tab.waveform_projected[ind,1] ,
									ls =' ', color = cmap(c) , marker = 'o')
		
		self.canvas_clustering.draw_idle()
		self.refresh_treeview()

	
	#------------------------------------------------------------------------------
	def recomputeAllSteps(self) :
		self.computeFiltering()
		self.computeDetection()
		self.computeProjection()
		self.computeClustering()
	
	
	#------------------------------------------------------------------------------
	def save_to_db(self) :
		self.tab.save_to_db()
	
	#------------------------------------------------------------------------------
	def reload_from_db(self) :
		self.tab.reload_from_db()
		self.refresh_treeview()


	#------------------------------------------------------------------------------
	def refresh_treeview(self) :
		self.ax_waveform_database.clear()
		self.ax_signal_database.clear()
		n_cluster = unique(self.tab.cluster).size
		cmap = pylab.get_cmap('jet',n_cluster)
		
		waveform_size = self.tab.param_database.get_one_param('waveform_size')
		oversampling = self.tab.param_database.get_one_param('oversampling')
		save_filtered_waveform = self.tab.param_database.get_one_param('save_filtered_waveform')
		
		
		if save_filtered_waveform :
			fil = self.tab.multiMethod_filtering.get_method()
			karg = self.tab.multiMethod_filtering.get_dict()
			sig_f = fil.compute(self.tab.sig , self.tab.fs , **karg)
		else : 
			sig_f = self.tab.sig
		waveform = waveform_extraction(self.tab.pos_spike,sig_f, self.tab.fs , waveform_size,oversampling)

		tw = arange(waveform.shape[1])/(oversampling*self.tab.fs)
		
		if self.id_electrode is not None :
			# mode one electrode
			t = self.tab.t
			self.ax_signal_database.plot(t,sig_f)
			
			self.treeview_spiketrain.clear()
			for n,cl in enumerate(unique(self.tab.cluster)) :
				ind, = where(cl == self.tab.cluster)
				self.ax_waveform_database.plot( tw,waveform[ind,:].transpose() , color = cmap(n) )
				
				item = QTreeWidgetItem(["spiketrain %s" % (n+1)  ,str(ind.size) , '' ] )
				pix = QPixmap(10,10 )
				pix.fill(QColor( cmap(n)[0]*255 , cmap(n)[1]*255 , cmap(n)[2]*255  ))
				icon = QIcon(pix)
				item.setIcon(0,icon)
				self.treeview_spiketrain.addTopLevelItem(item)
				
				self.ax_signal_database.plot(t[self.tab.pos_spike[ind]], sig_f[self.tab.pos_spike[ind]],
							'o', markerfacecolor = cmap(n) , markeredgecolor = 'k' )
		else:
			# mode all electrode on same serie
			t = self.tab.t
			self.ax_signal_database.plot(t,sig_f)
			
			self.treeview_spiketrain.clear()
			for n,cl in enumerate(unique(self.tab.cluster)) :
				ind, = where(cl == self.tab.cluster)
				self.ax_waveform_database.plot( tw,waveform[ind,:].transpose() , color = cmap(n) )
				start = 0
				for e,elec in enumerate(self.tab.list_elec):
					pos = self.tab.pos_spike[ind]
					pos = pos[ (pos>=start) & (pos<start+elec.signal.size) ]
					item = QTreeWidgetItem(["cell %s" % (n+1)  , 'id_electrode '+str(elec.id_electrode) , str(pos.size)  ] )
					pix = QPixmap(10,10 )
					pix.fill(QColor( cmap(n)[0]*255 , cmap(n)[1]*255 , cmap(n)[2]*255  ))
					icon = QIcon(pix)
					item.setIcon(0,icon)
					self.treeview_spiketrain.addTopLevelItem(item)
					
					self.ax_signal_database.plot(t[self.tab.pos_spike[ind]], sig_f[self.tab.pos_spike[ind]],
							'o', markerfacecolor = cmap(n) , markeredgecolor = 'k' )
					
					start += elec.signal.size
		self.canvas_database.draw_idle()
		self.canvas_database2.draw_idle()
		
	#------------------------------------------------------------------------------
	def deleteSelection(self) :
		if self.id_electrode is not None :
			# mode one electrode
			text = u"You want to delete this spiketrain and its spikes ?"
		else:
			# mode all electrode on same serie
			text = u"You want to delete this cell and its spikes ?"
			
		mb = QMessageBox.warning(self,self.tr('Delete'),text, 
				QMessageBox.Ok ,QMessageBox.Cancel  | QMessageBox.Default  | QMessageBox.Escape,
				QMessageBox.NoButton)
		if mb == QMessageBox.Cancel : return
		for item in  self.treeview_spiketrain.selectedItems() :
			pos = self.treeview_spiketrain.indexFromItem(item).row()
			if self.id_electrode is not None :
				# mode one electrode
				pos = pos
			else:
				# mode all electrode on same serie
				pos = int(pos / len(self.tab.list_elec))

			c = unique(self.tab.cluster)[pos]
			self.tab.pos_spike = self.tab.pos_spike[self.tab.cluster != c]
			self.tab.cluster = self.tab.cluster[self.tab.cluster != c]
		self.refresh_treeview()
	
	#------------------------------------------------------------------------------
	def groupSpikeTrainWithSpikeTrain(self ):
		c1 = self.sender().spiketrain1
		c2 = self.sender().spiketrain2
		self.tab.cluster[self.tab.cluster == c1] = c2
		self.tab.cluster[self.tab.cluster > c1] -= 1
		self.refresh_treeview()
	
	#------------------------------------------------------------------------------
	def contextMenuTreeviewSpikeTrain(self , point) :
		for item in  self.treeview_spiketrain.selectedItems() :
			pos = self.treeview_spiketrain.indexFromItem(item).row()
			menu = QMenu()
			act = menu.addAction(self.tr('Delete'))
			self.connect(act,SIGNAL('triggered()') ,self.deleteSelection)
			l = unique(self.tab.cluster).tolist()
			if self.id_electrode is not None :
				# mode one electrode
				pos = pos
			else:
				# mode all electrode on same serie
				pos = int(pos / len(self.tab.list_elec))
			l.remove(unique(self.tab.cluster)[pos])
			list_action = []
			for i in l :
				act = menu.addAction(self.tr('Group with  ')+str(i+1))
				act.spiketrain1 = pos
				act.spiketrain2 = i
				list_action.append(act)
				self.connect(act,SIGNAL('triggered()') ,self.groupSpikeTrainWithSpikeTrain)
			menu.exec_(self.cursor().pos())




#------------------------------------------------------------------------------
def OpenWindowEditSpike(parent, id_electrode) :
	w = WindowEditSpike(parent=parent , id_electrode = id_electrode)
	w.show()
	



#------------------------------------------------------------------------------
class ComputeSpikeElectrodeDialog(QWidget):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None ,
					mode = 'electrode' ):
		
		"""
		2 modes :	work on 1 electrode
					work on all same electrode from a serie ( same cells )
		"""
		
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		SpikeTrain().create_table()
		Spike().create_table()
		
		self.mode = mode
		
		#design
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		self.h1 = QHBoxLayout()
		self.v2 = QVBoxLayout()
		self.h1.addLayout(self.v2)
		mainLayout.addLayout(self.h1)
		bt_compute = QPushButton('Compute list')
		self.v2.addWidget(bt_compute)
		self.connect(bt_compute,SIGNAL('clicked()') , self.compute )
		
		if self.mode == 'electrode' :
			# mode one electrode
			child_hierarchy = [ 	['root',	[	[ 'electrode' ,[] ]	]		]	]
			self.queryResultBox = QueryResultBox(table = 'electrode',
											field_list = ['name' , 'num_channel' ,'label' ],
											default_query = 'SELECT id_electrode FROM electrode LIMIT 10' )
			self.v2.addWidget(self.queryResultBox)
			
			self.setWindowTitle (self.tr('Edit oscillation for id_electrodes' ))
		else :
			child_hierarchy = [ 	['root',	[	[ 'serie' ,[] ]	]		]	]
			self.queryResultBox = QueryResultBox(table = 'serie', field_list = ['info'  ] )
			self.v2.addWidget(self.queryResultBox)
			
			self.setWindowTitle (self.tr('Edit oscillation for id_series' ))
		
		self.v3 = QVBoxLayout()
		self.h1.addLayout(self.v3)
		self.tab = TabSpikeSorting()
		self.v_param = QVBoxLayout()
		self.v3.addLayout(self.v_param)
		self.v_param.addWidget(self.tab)
		
	
	#------------------------------------------------------------------------------
	def compute(self):
		if self.mode == 'electrode' :
			query = self.queryResultBox.get_query()
			id_electrodes, = sql(query)
			for id_electrode in id_electrodes :
				self.tab.id_electrode = id_electrode
				self.tab.load_signal()
				self.tab.recomputeAllSteps()
				self.tab.save_to_db()
		else :
			query = self.queryResultBox.get_query()
			id_series, = sql(query)
			for id_serie in id_series :
				query = """
						SELECT DISTINCT(electrode.num_channel)
						FROM electrode , trial
						WHERE
						trial.id_trial = electrode.id_trial
						AND trial.id_serie = %s
						"""
				num_channels, = sql(query , (id_serie))
				print num_channels
				for num_channel in num_channels :
					print id_serie , num_channel
					self.tab.id_serie = id_serie
					self.tab.num_channel = num_channel
					self.tab.load_signal()
					self.tab.recomputeAllSteps()
					self.tab.save_to_db()






