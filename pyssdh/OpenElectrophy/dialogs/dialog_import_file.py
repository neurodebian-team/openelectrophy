# -*- coding: utf-8 -*-


"""
some Qt based dialogs for importing file

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from pyssdh.core.gui import *
from pyssdh.OpenElectrophy.my_classes import *



#------------------------------------------------------------------------------
# New import base template dialog
class BaseImportDialog(QDialog) :
	"""
	class to for crating an import dialog
	"""
	def __init__(self, parent=None ,
					show_choose_file = True,
					type_choose = QFileDialog.ExistingFiles ,
					show_select_channel = True,
					show_invert_channel = True,
					show_select_trigger = True,
					show_database_option = True,
					**karg):
		QDialog.__init__(self, parent)
		self.mainLayout = QVBoxLayout()
		
		# Chosse file
		self.hbox_file = QHBoxLayout()
		self.mainLayout.addLayout(self.hbox_file)
		if show_choose_file :
			self.hbox_file.addWidget(QLabel(self.tr('File :')))
			self.chooseFileDir = ChooseFileDirWidget(type_choose = type_choose )
			self.hbox_file.addWidget(self.chooseFileDir)
		
		
		
		# File
		fr = QFrame()
		self.hbox_fileoption = QHBoxLayout()
		fr.setLayout(self.hbox_fileoption)
		self.mainLayout.addWidget(fr)
		
		# Channel selection
		if show_select_channel :
			fr = QFrame()
			self.hbox_selectchannel = QHBoxLayout()
			fr.setLayout(self.hbox_selectchannel)
			self.mainLayout.addWidget(fr)
			
			h = QHBoxLayout()
			self.radioAll = QRadioButton(self.tr("All electrode except :"))
			self.radioAll.setChecked(True)
			self.editAllExcept = QLineEdit()
			self.radioOnly = QRadioButton(self.tr("Only :"))
			self.editOnly = QLineEdit()
			h.addWidget(self.radioAll)
			h.addWidget(self.editAllExcept)
			h.addWidget(self.radioOnly)
			h.addWidget(self.editOnly)
			#groupBox = QGroupBox(self.tr("Selected electrode channel"))
			groupBox = QGroupBox('')
			groupBox.setLayout(h)
			self.hbox_selectchannel.addWidget(groupBox)
			
		# Channel inversion
		if show_invert_channel :
			fr = QFrame()
			#fr.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
			fr.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
			self.hbox_invertchannel = QHBoxLayout()
			fr.setLayout(self.hbox_invertchannel)
			self.mainLayout.addWidget(fr)
			
			self.hbox_invertchannel.addWidget(QLabel(self.tr('Invert channel :')))
			self.editInvertChannel = QLineEdit()
			self.hbox_invertchannel.addWidget(self.editInvertChannel)
			
		# Trigger selection
		if show_select_trigger :
			fr = QFrame()
			fr.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
			self.hbox_selecttrigger = QHBoxLayout()
			fr.setLayout(self.hbox_selecttrigger)
			self.mainLayout.addWidget(fr)
			
			self.hbox_selecttrigger.addWidget(QLabel('Trigger on analog channel : '))
			self.editTriggerChannel = QLineEdit('')
			self.hbox_selecttrigger.addWidget(self.editTriggerChannel)
			self.checkbox_RisingEgde = QCheckBox('Rising Edge')
			self.checkbox_RisingEgde.setChecked(True)
			self.hbox_selecttrigger.addWidget(self.checkbox_RisingEgde)
			self.checkbox_FallingEgde = QCheckBox('Falling Edge')
			self.checkbox_FallingEgde.setChecked(False)
			self.hbox_selecttrigger.addWidget(self.checkbox_FallingEgde)
			
		# Database option
		if show_database_option :
			fr = QFrame()
			fr.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
			self.hbox_databaseoption = QHBoxLayout()
			fr.setLayout(self.hbox_databaseoption)
			self.mainLayout.addWidget(fr)
			
			self.checkbox_serie = QCheckBox('Group all files in the same serie:')
			self.checkbox_serie.setChecked(True)
			self.hbox_databaseoption.addWidget(self.checkbox_serie)
		
		# OK and cancel
		h = QHBoxLayout()
		self.buttonOK = QPushButton('OK')
		h.addWidget(self.buttonOK)
		self.connect(self.buttonOK , SIGNAL('clicked()') , self , SLOT('accept()'))
		self.buttonCancel = QPushButton('Cancel')
		self.connect(self.buttonCancel , SIGNAL('clicked()') , self , SLOT('reject()'))
		h.addWidget(self.buttonCancel)
		self.mainLayout.addLayout(h)
		
		#self.connect(self , SIGNAL('accepted()') , self.ok)
		#self.connect(self , SIGNAL('rejected()') , self.cancel)

		self.setLayout(self.mainLayout)
		self.setModal(True)

	#def ok(self) :
		#pass
	#def cancel(self) :
		#pass
	

	#------------------------------------------------------------------------------
	def list_file(self) :
		return self.chooseFileDir.list_file()
	
	#------------------------------------------------------------------------------
	def list_electrode(self , n_total) :
		if self.radioAll.isChecked() :
			list_electrode = arange(n_total)
			l = unicode(self.editAllExcept.text()).replace(',',' ')\
							.replace(';',' ').replace('	',' ').split(',')
			while '' in l : l.remove('')
			list_electrode = setdiff1d(list_electrode ,[ int(e)-1 for e in l ] ) 
		else :
			l = unicode(self.editOnly.text()).replace(',',' ').split(' ')
			list_electrode = array([ int(e) for e in l ])
		list_electrode = setdiff1d( list_electrode , self.getChannelTrigger() )
		return list_electrode

	#------------------------------------------------------------------------------
	def list_inversion(self) : 
		l = unicode(self.editInvertChannel.text()).replace(',',' ').split(',')
		while '' in l : l.remove('')
		invertchannel = array([ int(e)-1 for e in l ])
		return invertchannel
	
	#------------------------------------------------------------------------------
	def getChannelTrigger(self) :
		try :
			l = unicode(self.editTriggerChannel.text()).replace(',',' ')\
								.replace(';',' ').replace('	',' ').split(' ')
			return array([ int(e)-1 for e in l ])
		except :
			return array([ ])
	
	#------------------------------------------------------------------------------
	def groupSameSerie(self) :
		return self.checkbox_serie.isChecked()



#------------------------------------------------------------------------------
# Template dialogs for import file :
# mono or multi electrode with or without option
#------------------------------------------------------------------------------
class ImportTemplateDialog(QDialog):
	"""
	class to for crating an import dialog
	"""
	def __init__(self, parent=None , show_checkbox_serie = True, type_choose = QFileDialog.ExistingFiles , **karg):
		QDialog.__init__(self, parent)
		self.mainLayout = QVBoxLayout()
		
		self.v0 = QVBoxLayout()
		self.mainLayout.addLayout(self.v0)
		self.v1 = QVBoxLayout()
		self.mainLayout.addLayout(self.v1)
		
		h2= QHBoxLayout()
		self.v0.addLayout(h2)
		h2.addWidget(QLabel(self.tr('File :')))
		self.chooseFileDir = ChooseFileDirWidget(type_choose = type_choose )
		h2.addWidget(self.chooseFileDir)
		
		#self.editFileName = QLineEdit()
		#h2.addWidget(self.editFileName)
		#buttonOpen = QPushButton()
		#buttonOpen.setMaximumSize(28,28)
#		buttonOpen.setIcon(QIcon(dict_icon['fileopen.png']))
		#h2.addWidget(buttonOpen)
		#self.connect(buttonOpen , SIGNAL('clicked()'),self.chooseFile)
		
		if show_checkbox_serie :
			h2= QHBoxLayout()
			self.v0.addLayout(h2)
			h2.addWidget(QLabel(self.tr('Group all files in the same serie:')))
			self.checkbox_serie = QCheckBox()
			self.checkbox_serie.setChecked(True)
			h2.addWidget(self.checkbox_serie)
		
		h = QHBoxLayout()
		self.buttonOK = QPushButton('OK')
		h.addWidget(self.buttonOK)
		self.connect(self.buttonOK , SIGNAL('clicked()') , self , SLOT('accept()'))
		self.buttonCancel = QPushButton('Cancel')
		self.connect(self.buttonCancel , SIGNAL('clicked()') , self , SLOT('reject()'))
		h.addWidget(self.buttonCancel)
		self.mainLayout.addLayout(h)
		
		self.connect(self , SIGNAL('accepted()') , self.ok)
		self.connect(self , SIGNAL('rejected()') , self.cancel)
		
		self.setLayout(self.mainLayout)
		self.setModal(True)
	
	def list_file(self) :
		return self.chooseFileDir.list_file()
	
	def groupSameSerie(self) :
		return self.checkbox_serie.isChecked()

	def ok(self) :
		pass
	def cancel(self) :
		pass
	
class ImportOption :
	"""
	class to derivate for adding option channel
	"""
	def __init__(self,**karg) :
		h4 = QHBoxLayout()
		self.v1.addLayout(h4)
		h4.addWidget(QLabel(self.tr('Add special channel')))
		self.comboSpecialChannel = QComboBox()
		self.comboSpecialChannel.setEditable(False)
		for optional_name in dict_optional_data_class.keys() :
			self.comboSpecialChannel.addItem(optional_name)
		h4.addWidget(self.comboSpecialChannel)
		self.buttonNewOption = QPushButton('+')
		self.buttonNewOption.setMaximumSize(28,28)
		h4.addWidget(self.buttonNewOption)
		self.connect(self.buttonNewOption , SIGNAL('clicked()') , self.addNewOptionalChannel)
		self.list_special_channel = [ ]
	
	def get_list_special_channel(self) : 
		for special_channel in self.list_special_channel :
			try :
				num = int(special_channel['editNumSpecialChannel'].text())-1
			except :
				num = None
			special_channel['num'] = num
			special_channel['class'] = dict_optional_data_class[special_channel['channelName']]
		return self.list_special_channel
	
	def addNewOptionalChannel(self) :
		channelName = str(self.comboSpecialChannel.currentText())
		h = QHBoxLayout()
		self.v1.addLayout(h)
		h.addWidget(QLabel(channelName))
		h.addWidget(QLabel(self.tr('num :')))
		editNumSpecialChannel = QLineEdit()
		h.addWidget(editNumSpecialChannel)
		self.list_special_channel.append( { 'editNumSpecialChannel' : editNumSpecialChannel  , 'channelName' : channelName } )


class ImportMulti :
	"""
	class to derivate for managing multi electrode import
	"""
	def __init__(self,**karg) :
		h3 = QHBoxLayout()
		self.radioAll = QRadioButton(self.tr("All electrode except :"))
		self.radioAll.setChecked(True)
		self.editAllExcept = QLineEdit()
		self.radioOnly = QRadioButton(self.tr("Only :"))
		self.editOnly = QLineEdit()
		h3.addWidget(self.radioAll)
		h3.addWidget(self.editAllExcept)
		h3.addWidget(self.radioOnly)
		h3.addWidget(self.editOnly)
		groupBox = QGroupBox(self.tr("Selected electrode channel"))
		groupBox.setLayout(h3)
		self.v0.addWidget(groupBox)
	list_special_channel = [ ]
	
	def list_electrode(self , n_total) :
		if self.radioAll.isChecked() :
			list_electrode = arange(n_total)
			l = unicode(self.editAllExcept.text()).replace(',',' ').split(',')
			while '' in l : l.remove('')
			list_electrode = setdiff1d(list_electrode ,[ int(e)-1 for e in l ] ) 
		else :
			l = unicode(self.editOnly.text()).replace(',',' ').split(' ')
			list_electrode = array([ int(e) for e in l ])
		for special_channel in self.list_special_channel :
			list_electrode = setdiff1d(list_electrode , [int(special_channel['editNumSpecialChannel'].text())-1 ] )
		return list_electrode

#------------------------------------------------------------------------------
class ImportTemplateDialogMonoElectrode(ImportTemplateDialog):
	def __init__(self, parent=None,**karg) :
		ImportTemplateDialog.__init__(self,parent,**karg)
		
#------------------------------------------------------------------------------
class ImportTemplateDialogMonoElectrodeAndOption(ImportTemplateDialog,ImportOption):
	def __init__(self, parent=None,**karg) :
		ImportTemplateDialog.__init__(self,parent,**karg)
		ImportOption.__init__(self,**karg)
#------------------------------------------------------------------------------
class ImportTemplateDialogMultiElectrode(ImportTemplateDialog,ImportMulti):
	def __init__(self, parent=None,**karg) :
		ImportTemplateDialog.__init__(self,parent,**karg)
		ImportMulti.__init__(self,**karg)
#------------------------------------------------------------------------------
class ImportTemplateDialogMultiElectrodeAndOption(ImportTemplateDialog,ImportMulti,ImportOption):
	def __init__(self, parent=None,**karg) :
		ImportTemplateDialog.__init__(self,parent,**karg)
		ImportMulti.__init__(self,**karg)
		ImportOption.__init__(self,**karg)	
		
		
		