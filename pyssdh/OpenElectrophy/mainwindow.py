# -*- coding: utf-8 -*-


"""
this is the main window module

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)



"""

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

applicationname = 'OpenElectrophy'
import pyssdh.core.dialogs
pyssdh.core.dialogs.applicationname = applicationname
pyssdh.core.dialogs.usersetting = pyssdh.core.dialogs.usersettings.UserSettings(name = applicationname)

from pyssdh.core.gui import *

#from pyssdh.core.dialogs import *
#from pyssdh.core.sql_util import *
#from pyssdh.core.explorer import *
#from pyssdh.core.figurezone import *
#from pyssdh.core.my_mpl_widget import *
#from pyssdh.core.icon import dict_icon



from my_classes import *
from pyssdh.OpenElectrophy.analysis import *






class MainLayout(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		
		self.setWindowTitle (self.tr('Open Electrophy'))
		self.menuBar = QMenuBar()
		self.menuFile = self.menuBar.addMenu(self.tr("&File"))
		act = self.menuFile.addAction(self.tr('Connect to server'))
		self.connect(act,SIGNAL('triggered()') , self.dialog_reconnection_server)
		
		
		self.dict_menu_filereader = { }
		
		def add_menu_recursive(menu , list_menu) :
			name , list_men = list_menu
			sub_menu = menu.addMenu(name)
			for men in list_men :
				if  type(men) == type(()) :
					sub_menu = add_menu_recursive(sub_menu , men )
				else :
					act = sub_menu.addAction(men.name)
					self.dict_menu_filereader[men.name] = act
					self.connect(act , SIGNAL('triggered()') , men.import_gui)
			return menu
		from pyssdh.OpenElectrophy.filereader import list_filereader
		add_menu_recursive(self.menuFile , list_filereader )

		"""
		self.menuImport = self.menuFile.addMenu(self.tr('Create new recording from file '))
		self.listActionImport = []
		from open_electrophy import import_file
		for m in import_file.list_module :
			if hasattr(m,'name') and hasattr(m,'import_gui') :
				act = self.menuImport.addAction(m.name)
				self.connect(act,SIGNAL('triggered()') , m.import_gui)
		"""
		
		
		self.menuDetection = self.menuBar.addMenu(self.tr("&Detection"))
		act = self.menuDetection.addAction(self.tr('Extract spike for electrodes'))
		self.connect(act,SIGNAL('triggered()') , self.dialog_ComputeSpikeElectrodeDialog )
		act = self.menuDetection.addAction(self.tr('Extract spike for series'))
		self.connect(act,SIGNAL('triggered()') , self.dialog_ComputeSpikeSerieDialog )
		act = self.menuDetection.addAction(self.tr('Extract oscillation'))
		self.connect(act,SIGNAL('triggered()') , self.dialog_ComputeOscillationElectrodeDialog )
		
		self.menuAnalysis = self.menuBar.addMenu(self.tr("&Analysis"))
		act = self.menuAnalysis.addAction(self.tr('List Analysis'))
		self.connect(act,SIGNAL('triggered()') , self.window_ListAnalysis )
		
		self.dict_menu_analysis = { }
		def add_menu_recursive(menu , list_analysis) :
			name , list_ana = list_analysis
			sub_menu = menu.addMenu(name)
			for ana in list_ana :
				if  type(ana) == type(()) :
					sub_menu = add_menu_recursive(sub_menu , ana)
				else :
					act = sub_menu.addAction(ana.analysis_name)
					self.dict_menu_analysis[ana.analysis_name] = act
					self.connect(act , SIGNAL('triggered()') , self.window_OneAnalysis)
			return menu
		add_menu_recursive(self.menuAnalysis , list_analysis)

		
		v1 = QVBoxLayout()
		v1.setMenuBar(self.menuBar)
		
		sp2 = QSplitter(Qt.Horizontal)
		sp2.setOpaqueResize(False)
		v1.addWidget(sp2)
		
		#Main explorer
		self.dialog_connection_server()
		child_hierarchy0 = construct_hierarchy(dict_hierarchic_class , dict_default_browsable)
		child_hierarchy1 = [ 	['root',	[	[ 'electrode' ,[] ]	]		]	]
		child_hierarchy2 = [ 	['root',	[	[ 'spiketrain' ,[] ]	]		]	]
		#child_hierarchy3 = [ 	['root',	[	[ 'cell' ,[['spiketrain',[]]] ]	]		]	]
		child_hierarchy3 = [ 	['root',	[	[ 'serie',	[[ 'cell' ,[['spiketrain',[]]] ]	]		]	] ]]
		list_child_hierarchy = [ child_hierarchy0 , child_hierarchy1 ,child_hierarchy2 ,child_hierarchy3]
		list_title = ['Hierarchic explorer' ,'electrode' , 'spiketrain' , 'cell']
		dict_default_shown_field['root'] = ['id' ,'' ,'']
		list_dict_field = [dict_default_shown_field]*4
		dict_query0 = dict([ ( k ,'' ) for k in  dict_default_shown_field.keys() ])
		#dict_query1 = dict([ ( k ,'' ) for k in  dict_default_shown_field.keys() ])
		dict_query1 = { 'electrode' : 'SELECT id_electrode FROM electrode LIMIT 10' }
		dict_query2 = dict([ ( k ,'' ) for k in  dict_default_shown_field.keys() ])
		#dict_query3 = { 'cell' :'' , 'spiketrain' : '' }
		dict_query3 = { 'serie' : '' ,  'cell' :'' , 'spiketrain' : '' }
		list_dict_query = [dict_query0 , dict_query1 , dict_query2 ,  dict_query3]
		list_showQueryEdit = [ False , True , True , True ]
		
		# verify is new field in each class :
		for k,v in dict_hierarchic_class.iteritems():
			if v().is_table():
				v().test_all_field()
		
		self.mainExplorer = MainExplorer(list_title = list_title,
						list_child_hierarchy =list_child_hierarchy,
						list_showQueryEdit = list_showQueryEdit,
						list_dict_field = list_dict_field ,
						list_dict_query = list_dict_query ,
						dict_hierarchic_class = dict_hierarchic_class,
						dict_context_menu = dict_context_menu
						)
		sp2.addWidget(self.mainExplorer)
		#self.mainExplorer.refreshView()
		self.mainExplorer.changeBaseName(self.database_name)
		
		v4 = QVBoxLayout()
		fv4 = QFrame()
		fv4.setLayout(v4)
		sp2.addWidget(fv4)
		v4.addWidget(QLabel('Figures'))
		self.f = FigureZone(explorer = self.mainExplorer , dict_hierarchic_class = dict_hierarchic_class )
		v4.addWidget(self.f)
		
		self.setLayout(v1)
		
	def import_file(self) :
		pass
	
	def dialog_connection_server(self) :
		self.host , self.login , self.password = ConnectionSql()
		init_connection(host=self.host, login=self.login, password=self.password )
		dchoose = ChooseBaseDialog()
		if dchoose.exec_() :
			self.database_name = dchoose.database_name
			init_connection(host=self.host, login=self.login, password=self.password , nom_base = self.database_name )
			#self.mainExplorer.refreshView()
			#self.mainExplorer.changeBaseName(self.database_name)
	
	def dialog_reconnection_server(self) :
		self.dialog_connection_server()
		self.mainExplorer.refreshView()
		self.mainExplorer.changeBaseName(self.database_name)
		
	def dialog_ComputeSpikeElectrodeDialog(self) :
		ComputeSpikeElectrodeDialog(parent = self , mode = 'electrode').show()
		
	def dialog_ComputeSpikeSerieDialog(self) :
		ComputeSpikeElectrodeDialog(parent = self , mode = 'serie').show()	
		
	def dialog_ComputeOscillationElectrodeDialog(self) :
		ComputeOscillationElectrodeDialog().exec_()
		
	def window_ListAnalysis(self) :
		w =WindowListAnalysis(self)
		w.show()

	def window_OneAnalysis(self) :
		pos = self.dict_menu_analysis.values().index(self.sender())
		analysis_name = self.dict_menu_analysis.keys()[pos]
		w =  WindowOneAnalysis(analysis_name , self)
		w.show()

def start_app() :
	app.setFont(QFont("Verdana", 11))
	ml = MainLayout()
	ml.show()
	sys.exit(app.exec_())
	
#------------------------------------------------------------------------------
if __name__ == "__main__":
	start_app()
