# -*- coding: utf-8 -*-


from serie import *
from trial import *
from electrode import *
from spike import *
from timefreq import *
from epoch import *







dict_hierarchic_class = {
						'serie' : Serie,
						'trial' : Trial ,
						'electrode' : Electrode ,
						'cell' : Cell,
						'spiketrain' : SpikeTrain,
						'spike' : Spike,
						'epoch' : Epoch ,
						'oscillation' : Oscillation
						}