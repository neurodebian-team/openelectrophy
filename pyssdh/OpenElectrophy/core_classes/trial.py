# -*- coding: utf-8 -*-


"""
class trial : child of serie and father of electrode

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



from pyssdh.core.sql_util import *


import datetime

class Trial(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self, **karg ) :
		database_storage.__init__(self,**karg)
		
		
	#------------------------------------------------------------------------------
	table_name = 'trial'
	
	list_field = [		'id_serie',
					'thedatetime',
					'info'
				]
				
	field_type = [	'INDEX',
					'DATETIME',
					'TEXT'
				]
	list_table_child = [ 'electrode' , 'epoch']
	#------------------------------------------------------------------------------
		
		
