# -*- coding: utf-8 -*-


"""
classes for managing spike and spike train

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from pyssdh.core.sql_util import *

from scipy import *
from scipy.stats import histogram2

import os,stat


from electrode import *
from pyssdh.OpenElectrophy.computing import *



import pylab

#------------------------------------------------------------------------------
class Cell(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self,**karg) :
		database_storage.__init__(self,**karg)
		
	table_name = 'cell'
	list_field = [	'id_serie',
					'name',
					'info'
				]
	field_type = [	'INDEX',
					'TEXT',
					'TEXT',
				]
	list_table_child = [ 'spiketrain' ]
	#------------------------------------------------------------------------------
	def list_spiketrain(self) :
		query = """SELECT id_spiketrain FROM spiketrain WHERE id_cell = %s """
		id_spiketrains , = sql(query,(self.id_cell))
		list_spiketrain = []
		for id_spiketrain in id_spiketrains :
			sptr = SpikeTrain()
			sptr.load_from_db(id_spiketrain)
			list_spiketrain.append(sptr)
		return list_spiketrain

	#------------------------------------------------------------------------------
	list_type_plot = [  'waveform' , 'ISI hist' , 'auto correlation' ]
	list_option_default = [
							[ ],
							[0.,	1.,	0.001],
							[-1.,	1.,	0.01],
						]
	list_option_param = [
							[ ],
							[ 't1' , 't2' , 't_step'],
							[ 't1' , 't2' , 't_step'],
						]
	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'raster' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [  self.plot_waveform , self.plot_isi_hist , self.plot_crosscorr ]
		list_func_plot[pos](ax=ax , **param )
	
	#------------------------------------------------------------------------------
	def plot_waveform(self,ax = None, 
					color = None,
					titl = None, label = None ) : 
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			
		if 'nb_plot_waveform_spike' not in dir(ax) :
			ax.nb_plot_waveform_spike = 0
			ax.list_plot_waveform_spike = []
		ax.nb_plot_waveform_spike +=1
		
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_waveform_spike)
			list_color = []
			for l,lines in enumerate(ax.list_plot_waveform_spike) :
				for line in lines : 
					line.set_color(cmap(l))
			color = cmap(ax.nb_plot_waveform_spike)
			
		lines = []
		for spiketrain in self.list_spiketrain() :
			for s,sp in enumerate(spiketrain.list_spike()) :
				#pylab.plot(sp.waveform , '.' , color = color , markerfacecolor = color )
				t = arange(0,sp.waveform.size)/(spiketrain.oversampling*spiketrain.fs)
				t = t - t[-1]/2.
				lines += ax.plot(t,sp.waveform ,  color = color , markerfacecolor = color )
		ax.list_plot_waveform_spike.append(lines)
		
		return ax
			
	#------------------------------------------------------------------------------
	def plot_isi_hist(self,ax = None, 
					color = None,
					titl = None, label = None,
					t1 = 0. , t2 = 1. , t_step = .001  ) : 
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			
		if 'nb_plot_isi_hist' not in dir(ax) :
			ax.nb_plot_isi_hist = 0
			ax.list_plot_isi_hist = []
		ax.nb_plot_isi_hist +=1
		
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_isi_hist)
			list_color = []
			for l,lines in enumerate(ax.list_plot_isi_hist) :
				for line in lines : 
					line.set_color(cmap(l))
			color = cmap(ax.nb_plot_isi_hist)
			
		lines = []
		query = """
				SELECT spike.isi
				FROM cell , spiketrain , spike
				WHERE
				cell.id_cell = spiketrain.id_cell
				AND spiketrain.id_spiketrain = spike.id_spiketrain
				AND cell.id_cell = %s
				"""
		isi, = sql(query , (self.id_cell))
		fen = arange(t1,t2,t_step)
		y,x = histogram(isi , fen)
		lines += ax.plot(x,y ,  color = color , markerfacecolor = color )
		ax.list_plot_isi_hist.append(lines)
		
		return ax
	
	#------------------------------------------------------------------------------
	def plot_crosscorr(self,ax = None, 
					color = None,
					titl = None, label = None,
					t1 = -1. , t2 = 1. , t_step = .001  ) :
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			
		if 'nb_plot_isi_hist' not in dir(ax) :
			ax.nb_plot_crosscorr = 0
			ax.list_plot_crosscorr = []
		ax.nb_plot_crosscorr +=1
		
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_crosscorr)
			list_color = []
			for l,lines in enumerate(ax.list_plot_crosscorr) :
				for line in lines : 
					line.set_color(cmap(l))
			color = cmap(ax.nb_plot_crosscorr)
		
		lines = [ ]
		fen = arange(t1,t2,t_step)-t_step/2.
		all= zeros(fen.shape , dtype = 'f')
		for spiketrain in self.list_spiketrain() :
			t = spiketrain.time_spike()
			print t.size
			t1 , t2 = combinate_all_pair(t , t , once = False)
			print t1.size ,
			y,x = histogram(t1-t2 , fen)
			print y
			all += y
		lines += ax.plot(x[1:-1],all[1:-1] ,  color = color , markerfacecolor = color )
		ax.list_plot_crosscorr.append(lines)




#------------------------------------------------------------------------------
class Spike(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self,**karg) :
		database_storage.__init__(self,**karg)
		
	table_name = 'spike'
	list_field = [	'id_spiketrain',
					'id_electrode',
					'pos',
					'isi',
					'val_max',
					'waveform'
				]
	field_type = [	'INDEX',
					'INDEX',
					'INT',
					'FLOAT',
					'FLOAT',
					'NUMPY'
				]
	list_table_child = []

#------------------------------------------------------------------------------
class SpikeTrain(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self ,**karg) :
		database_storage.__init__(self,**karg)
		
	table_name = 'spiketrain'
	list_field = [	'id_trial',
					'id_electrode',
					'id_cell',
					'fs' ,
					'shift_t0',
					'oversampling',
					'label',
					'coment',
					'f_low',
					'f_hight'
				]
	field_type = [	'INDEX',
					'INDEX',
					'INDEX',
					'FLOAT',
					'FLOAT',
					'INT',
					'TEXT',
					'TEXT',
					'FLOAT',
					'FLOAT'
				]
	list_table_child = [ 'spike' ]
	#------------------------------------------------------------------------------
	def list_spike(self) :
		query = """SELECT id_spike FROM spike WHERE id_spiketrain = %s """
		id_spikes , = sql(query,(self.id_spiketrain))
		list_spike = []
		for id_spike in id_spikes :
			sp = Spike()
			sp.load_from_db(id_spike)
			list_spike.append(sp)
		return list_spike
	
	#------------------------------------------------------------------------------
	def pos_spike(self) :
		query = """SELECT pos FROM spike WHERE id_spiketrain = %s  ORDER BY pos"""
		pos_spike , = sql(query,(self.id_spiketrain), Array = True , dtype ='i')
		return pos_spike
	
	#------------------------------------------------------------------------------
	def time_spike(self) :
		return self.pos_spike().astype('f8')/self.fs + self.shift_t0
	
	#------------------------------------------------------------------------------
	def isi(self) :
		return diff( self.time_spike() )
	
	#------------------------------------------------------------------------------
	def binary(self , bin_size = .001 , t_start = 0. , t_stop = 1. ) :
		win = arange( t_start , t_stop , bin_size )
		b = stats.histogram2(self.time_spike() ,win)
		b[-1] = 0
		b[ b >=1 ] = 1
		return b
		
	#------------------------------------------------------------------------------
	def val_max_spike(self) :
		query = """SELECT val_max FROM spike WHERE id_spiketrain = %s """
		val_max_spike , = sql(query,(self.id_spiketrain), Array = True , dtype ='i')
		return val_max_spike
	
	
	#------------------------------------------------------------------------------
	def nb_spike(self) :
		query = """SELECT count(*) FROM spike WHERE id_spiketrain = %s """
		nb, = sql1(query,(self.id_spiketrain))
		return nb
	
	#------------------------------------------------------------------------------
	list_type_plot = [ 'raster' , 'waveform',  'on signal' ]
	list_option_default = [
							[ ],
							[],
							[ 300., 3000.]
						]
	list_option_param = [
							[ ],
							[ ],
							[ 'f_low' , 'f_hight']
						]
	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'raster' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [ self.plot_raster , self.plot_waveform , self.plot_on_signal ]
		list_func_plot[pos](ax=ax , **param )
		
	#------------------------------------------------------------------------------
	def plot_raster(self, ax = None , color = None , y = None, label='' ) :
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			
		if 'nb_plot_raster_spike' not in dir(ax) :
			ax.nb_plot_raster_spike = 0
			ax.list_plot_raster_spike = []
		ax.nb_plot_raster_spike +=1
		
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_raster_spike)
			list_color = []
			for l,lines in enumerate(ax.list_plot_raster_spike) :
				for line in lines : 
					line.set_color(cmap(l))
			color = cmap(ax.nb_plot_raster_spike)
		
		if y is None :
			y = float(ax.nb_plot_raster_spike)
		
		lines = ax.plot(self.time_spike() , ones(self.time_spike().shape, dtype='f')*y ,'.',color = color , label = '' )
		print len(lines)
		ax.list_plot_raster_spike.append(lines)
		
		ax.set_ylim([-1,ax.nb_plot_raster_spike+1])
		

	
	#------------------------------------------------------------------------------
	def plot_waveform(self,ax = None, 
					color = None,
					titl = None, label = None ) : 
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			
		if 'nb_plot_waveform_spike' not in dir(ax) :
			ax.nb_plot_waveform_spike = 0
			ax.list_plot_waveform_spike = []
		ax.nb_plot_waveform_spike +=1
		
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_waveform_spike)
			list_color = []
			for l,lines in enumerate(ax.list_plot_waveform_spike) :
				for line in lines : 
					line.set_color(cmap(l))
			color = cmap(ax.nb_plot_waveform_spike)
			
		lines = []
		for s,sp in enumerate(self.list_spike()) :
			#pylab.plot(sp.waveform , '.' , color = color , markerfacecolor = color )
			t = arange(0,sp.waveform.size)/(self.oversampling*self.fs)
			t = t - t[-1]/2.
			lines += ax.plot(t,sp.waveform ,  color = color , markerfacecolor = color )
		ax.list_plot_waveform_spike.append(lines)
		
		return ax

	#------------------------------------------------------------------------------
	def plot_on_signal(self, ax = None, 
				elec = None,
				plot_signal = True,
				color = 'b',
				title = None, label = None,
				 f_low = 300. , f_hight = 3000.) :

		flag = False
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			flag = True
		if elec is None :
			elec = Electrode()
			elec.load_from_db(self.id_electrode)
		
		if f_low is None :f_low =0
		else :f_low = f_low/(elec.fs/2)
		if f_hight is None :f_hight =1
		else :f_hight = f_hight/(elec.fs/2)
		sig = fft_filter(elec.signal,f_low =f_low,f_hight=f_hight )
		
		if plot_signal == True :
			ax.plot(elec.t(),sig , 'b' , label=None)
		if label is None :
			label = self.label
		if self.pos_spike().size > 0 :
			ax.plot(elec.t()[self.pos_spike()],sig[self.pos_spike()],'o',markerfacecolor = color , markeredgecolor = 'k' , label = label)
		
		if title is not None :
			ax.set_title(title)
		if flag :
			ax.set_xlim((elec.t()[0],elec.t()[-1]))
		
		return ax


#------------------------------------------------------------------------------
def plot_all_spike_waveform(id_electrode ) :
	fig = pylab.figure()
	ax = fig.add_subplot(1,1,1)
	id_spiketrains , = sql("""SELECT id_spiketrain FROM spiketrain WHERE id_electrode = %s""", (id_electrode))
	cmap = pylab.get_cmap('jet',len(id_spiketrains))
	for c,id_spiketrain in  enumerate(id_spiketrains) :
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)
		sptr.plot_waveform(ax = ax, color = cmap(c) )
	ax.set_title('Spike waveform - id_electrode='+str(id_electrode))
	#pylab.legend()
	

	
#------------------------------------------------------------------------------
list_arg = ('abs_thresh', 'std_thresh')
list_def = ( None , 3)
def threshold_detection(electrode,sig_f,
					abs_thresh = None, std_thresh = 3):

	if abs_thresh is None :
		abs_thresh = abs(sig_f).mean() + std_thresh*abs(sig_f).std()
	pos_spike = where((abs(sig_f[1:-1]) >=abs_thresh) & 
								(
								(sig_f[1:-1] >sig_f[:-2]) & (sig_f[1:-1] >=sig_f[2:] ) |
								(sig_f[1:-1] <sig_f[:-2]) & (sig_f[1:-1] <=sig_f[2:] )
								))[0] +1
	val_max = sig_f[pos_spike]
	return pos_spike , val_max
threshold_detection.list_arg = list_arg
threshold_detection.list_def = list_def



#------------------------------------------------------------------------------
list_arg = ('minspike',)
list_def = (3,)
def intuitive_clustering(electrode,val_max,
						minspike  = 3
						):
	n_hist = 30
	thresh_hist = .1/n_hist
		
	nb , x = histogram(val_max,n_hist)
	cren = zeros((n_hist,))
	cren[nb>thresh_hist] = 1
	cren[0] = 0
	cren = r_[cren,0]
	limit_inf = x[diff(cren) == 1]
	limit_sup = x[diff(cren) == -1]
	nb_cluster = limit_inf.size
	if nb_cluster==0 : nb_cluster=1
	limit = empty((nb_cluster+1))
	for i in range(nb_cluster-1) :
		limit[i+1] =  (limit_sup[i] + limit_inf[i+1])/2
	limit[0] = -inf
	limit[-1] = inf
	cluster_spike = ones(val_max.size)*nan
	spike_by_cluster = ones(nb_cluster)*nan
	for i in range(nb_cluster) :
		cluster_spike[ (val_max>=limit[i]) & (val_max <=limit[i+1])] = i
		spike_by_cluster[i] = where(cluster_spike == i )[0].size
		
	#regroup if cluster too little (<minspike)
	toolittle ,= where(spike_by_cluster <minspike)
	while toolittle.size != 0 :
		nb_cluster = spike_by_cluster.size
		if nb_cluster == 1 :break
		i = toolittle[0]
		if i==0 :
			clust_regroup = 1
		elif i==nb_cluster-1 :
			clust_regroup = nb_cluster-2
		elif (val_max[cluster_spike == i].mean() - limit[i]) >(limit[i+1] - val_max[cluster_spike == i].mean()) :
			clust_regroup = i+1
		else :
			clust_regroup = i-1
		cluster_spike[cluster_spike == i] = clust_regroup
		cluster_spike[cluster_spike > i] -= 1
		if clust_regroup > i :
			limit = r_[limit[:i+1] , limit[i+2:]]
		else :
			limit = r_[limit[:i] , limit[i+1:]]
		nb_cluster -=1
		spike_by_cluster = ones(nb_cluster)*nan
		for c in range(nb_cluster) :
			spike_by_cluster[c] = where(cluster_spike == c )[0].size
		toolittle ,= where(spike_by_cluster <minspike)
	return cluster_spike
intuitive_clustering.list_arg = list_arg
intuitive_clustering.list_def = list_def


#------------------------------------------------------------------------------
def create_spiketrains_from_electrode(electrode , 
				detection_method = threshold_detection ,
				clustering_method = intuitive_clustering,
				f_low =300,f_hight=None,
				oversampling = 4,
				max_size_waveform = 0.002,
				refractory_period = .002,
				**karg
				
				#abs_thresh = None, std_thresh = 3,
				
				#keep_first = True,
				#keep_higher = False
				) :
	
	if f_low is None :f_l =0
	else :f_l = f_low/(electrode.fs/2)
	if f_hight is None :f_h =1
	else :f_h = f_hight/(electrode.fs/2)
		
	sig = electrode.signal
	sig_f = fft_filter(sig,f_low =f_l,f_hight=f_h )

	#detection
	dic = {}
	for  i, kw in enumerate(detection_method.list_arg) :
		if kw in karg.keys() :
			dic.update({kw : karg[kw] })
		else :
			dic.update({kw : detection_method.list_def[i] })
	pos_spike,pos_max = detection_method(electrode,sig_f,**dic)

	
	
	#refractory period elimination
	pos_spike.sort()
	pos_spike = pos_spike[ ((pos_spike -max_size_waveform*electrode.fs) > 0) & ((pos_spike +max_size_waveform*electrode.fs) < sig_f.size)]
	tooclose, = where(diff(pos_spike) < refractory_period*electrode.fs)
	while tooclose.size != 0 :
		for tc in tooclose :
			if abs(sig_f[pos_spike[tc]]) < abs(sig_f[pos_spike[tc+1]]) :
				pos_spike[tc] = -1
			else :
				pos_spike[tc+1] = -1
		pos_spike = pos_spike[pos_spike!=-1]
		pos_spike.sort()
		tooclose, = where(diff(pos_spike) < refractory_period*electrode.fs)
	
	#waveform extraction
	half_size = round(max_size_waveform*electrode.fs*oversampling)
	full_size=2*half_size
	waveform = ones((pos_spike.size,full_size), dtype = sig.dtype)*nan
	for i in range(pos_spike.size) :
		so = resample(sig_f[pos_spike[i]-max_size_waveform*electrode.fs:pos_spike[i]+max_size_waveform*electrode.fs],old_fs = electrode.fs, new_fs = electrode.fs*oversampling)
		pos_max = argmax(abs(so[half_size-oversampling:half_size+oversampling]))
		decal = pos_max-oversampling
		if decal>=0 :
			so = r_[so[decal:], zeros(decal)*nan ]
		else :
			so = r_[nan*zeros(-decal) ,  so[:decal] ]
			
		if so[half_size]>0 :
			so2 = -so
		else :
			so2 = so

		ds = diff(so2)
		ds1 = r_[0, ds]
		ds2 = r_[ds, 0]
		
		#local first max before center
		win_left = where((ds1*ds2<0) & (ds1 > 0) & (arange(full_size)<half_size))[0]
		if win_left.size==0 :
			win_left = 0
		else:
			win_left = win_left[-1]
		
		#local first min after center
		win_right = where((ds1*ds2<0) & (ds1 < 0) & (arange(full_size)>half_size))[0]
		if win_right.size==0 :
			win_right = full_size
		else:
			win_right = win_right[0]
		
		
		waveform[i,win_left:win_right] = so[win_left:win_right]


	val_max = sig_f[pos_spike]
	
	#clustering
	dic = {}
	for  i, kw in enumerate(clustering_method.list_arg) :
		if kw in karg.keys() :
			dic.update({kw : karg[kw] })
		else :
			dic.update({kw : clustering_method.list_def[i] })
	
	cluster_spike = clustering_method(electrode, val_max, **dic)
	nb_cluster = unique(cluster_spike).size
	
	id_spiketrains = []
	for c in range(nb_cluster) : 
		sptr = SpikeTrain(id_trial = electrode.id_trial , id_electrode = electrode.id_electrode)
		sptr.fs = electrode.fs
		sptr.shift_t0 =  electrode.shift_t0
		sptr.oversampling = oversampling
		sptr.f_low = f_low
		sptr.f_hight = f_hight
		sptr.label = u''
		sptr.coment = u''
		id_spiketrain = sptr.save_to_db()
		id_spiketrains.append(id_spiketrain)
		ind = where(cluster_spike==c)[0]
		isi = r_[diff(pos_spike[ind])/float(electrode.fs) , Inf]
		for s in range(len(ind)) :
			sp = Spike()
			sp.id_spiketrain = id_spiketrain
			sp.id_electrode = electrode.id_electrode
			sp.id_trial = electrode.id_trial
			sp.pos = pos_spike[ind[s]]
			sp.val_max = val_max[ind[s]]
			sp.waveform = squeeze(waveform[ind[s],:])
			sp.isi = isi[s]
			sp.save_to_db()
	return id_spiketrains


#------------------------------------------------------------------------------
#	Various fonctions for detecting extracting waveform ...
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def compute_spike_detection_multi_treshold(sig_f , list_tresh):
	"""
	spike detection with manual multi treshold
	"""
	list_pos_spike = []
	#negative spike
	neg_tresh = list_tresh[list_tresh<0]
	neg_tresh.sort()
	neg_tresh = r_[neg_tresh[::-1],-inf]
	for t in range(neg_tresh.size-1) :
		pos = where(	( sig_f[1:-1] <= neg_tresh[t] ) & 
							( sig_f[1:-1] > neg_tresh[t+1] ) & 
							(sig_f[1:-1] <sig_f[:-2]) & (sig_f[1:-1] <=sig_f[2:] )
							)[0] +1
		list_pos_spike.append(pos)
	#positive spike
	pos_tresh = list_tresh[list_tresh>=0]
	pos_tresh.sort()
	pos_tresh = r_[pos_tresh,inf]
	for t in range(pos_tresh.size-1) :
		pos = where(	( sig_f[1:-1] >= pos_tresh[t] ) & 
							( sig_f[1:-1] < pos_tresh[t+1] ) & 
							(sig_f[1:-1] >sig_f[:-2]) & (sig_f[1:-1] >=sig_f[2:] )
							)[0] +1
		list_pos_spike.append(pos)
	
	return list_pos_spike

#------------------------------------------------------------------------------
def eliminate_refractory_period(pos_spike,sig_f ,fs,refractory_period) :
	"""
	Eliminate spikes too close
	"""
	#refractory period elimination
	pos_spike.sort()
	#pos_spike = pos_spike[ ((pos_spike -max_size_waveform*fs) > 0) & ((pos_spike +max_size_waveform*fs) < sig_f.size)]
	tooclose, = where(diff(pos_spike) < refractory_period*fs)
	while tooclose.size != 0 :
		for tc in tooclose :
			if abs(sig_f[pos_spike[tc]]) < abs(sig_f[pos_spike[tc+1]]) :
				pos_spike[tc] = -1
			else :
				pos_spike[tc+1] = -1
		pos_spike = pos_spike[pos_spike!=-1]
		pos_spike.sort()
		tooclose, = where(diff(pos_spike) < refractory_period*fs)
	return pos_spike


#------------------------------------------------------------------------------
def waveform_extraction(pos_spike,sig_f, fs , max_size_waveform,oversampling, full_window=True) :
	"""
	waveform extraction from the filtered  or not signal
	"""
	half_size = round(max_size_waveform*fs*oversampling)
	full_size=2*half_size
	#waveform = zeros((pos_spike.size,full_size), dtype = sig_f.dtype)*nan
	waveform = zeros((pos_spike.size,full_size), dtype = sig_f.dtype)
	for i in range(pos_spike.size) :
		l1 = max(0,pos_spike[i]-round(max_size_waveform*fs))
		l2 = min(sig_f.size,pos_spike[i]+round(max_size_waveform*fs))
		lsig = sig_f[l1:l2]
		if pos_spike[i]-max_size_waveform*fs <0 :
			lsig = r_[ones((-(pos_spike[i]-max_size_waveform*fs)) , dtype = sig_f.dtype)*lsig[0] , lsig]
		if pos_spike[i]+max_size_waveform*fs > sig_f.size :
			lsig = r_[ lsig , ones( ( (pos_spike[i]+max_size_waveform*fs)-sig_f.size) , dtype = sig_f.dtype)*lsig[-1] ]
			
		so = resample(lsig,old_fs = fs, new_fs = fs*oversampling)
		pos_max = argmax(abs(so[half_size-oversampling:half_size+oversampling]))
		decal = pos_max-oversampling
		if decal>=0 :
			#so = r_[so[decal:], zeros(decal)*nan ]
			so = r_[so[decal:], ones(decal)*so[decal]-1 ]
		else :
			#so = r_[nan*zeros(-decal) ,  so[:decal] ]
			so = r_[ones(-decal)*so[decal] ,  so[:decal] ]
		if full_window :
			waveform[i,:so.size] = so
		else :
			if so[half_size]>0 :
				so2 = -so
			else :
				so2 = so
			ds = diff(so2)
			ds1 = r_[0, ds]
			ds2 = r_[ds, 0]
			
			#local first max before center
			win_left = where((ds1*ds2<0) & (ds1 > 0) & (arange(so.size)<(so.size)/2.))[0]
			if win_left.size==0 :
				win_left = 0
			else:
				win_left = win_left[-1]
			
			#local first min after center
			win_right = where((ds1*ds2<0) & (ds1 < 0) & (arange(so.size)>(so.size)/2.))[0]
			if win_right.size==0 :
				win_right = full_size
			else:
				win_right = win_right[0]
			waveform[i,win_left:win_right] = so[win_left:win_right]
	return waveform



#------------------------------------------------------------------------------
#	PLOT
#------------------------------------------------------------------------------
def plot_all_spike_on_signal(id_electrode = None , elec = None,  ax = None ,
							signal_filtered = True) :
	if id_electrode is not None and elec is None : 
		elec = Electrode()
		elec.load_from_db(id_electrode)
	if ax is None :
		fig = pylab.figure()
		ax = fig.add_subplot(1,1,1)
	id_spiketrains , = sql("""SELECT id_spiketrain FROM spiketrain WHERE id_electrode = %s""", (elec.id_electrode))
	cmap = pylab.get_cmap('jet',len(id_spiketrains))
	for c,id_spiketrain in  enumerate(id_spiketrains) :
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)
		sptr.plot(ax = ax, color = cmap(c) ,
				plot_signal = (c==0),
				 elec =elec , signal_filtered = signal_filtered, label = 'id_spiketrain='+str(sptr.id_spiketrain))
	ax.set_title('Spike - id_electrode='+str(elec.id_electrode))
	#pylab.legend()

#------------------------------------------------------------------------------
def plot_all_spike_waveform(id_electrode = None ,   ax = None ) :
	if ax is None :
		fig = pylab.figure()
		ax = fig.add_subplot(1,1,1)
	id_spiketrains , = sql("""SELECT id_spiketrain FROM spiketrain WHERE id_electrode = %s""", (id_electrode))
	cmap = pylab.get_cmap('jet',len(id_spiketrains))
	for c,id_spiketrain in  enumerate(id_spiketrains) :
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)
		sptr.plot_waveform(ax = ax, color = cmap(c) )
	ax.set_title('Spike waveform - id_electrode='+str(id_electrode))
	#pylab.legend()

#------------------------------------------------------------------------------
def plot_isi(id_spiketrain = None,
			query=None, values=(),
			nbin =1000, bins=None,
			fig = None ) :
	
	if id_spiketrain is not None :
		query = """SELECT isi FROM spike 
				WHERE isi IS NOT NULL  
				AND spike.id_spiketrain = %s """
		values = (id_spiketrain)
	elif query is not None :
		pass
	else :
		return
	
	if fig is None :
		fig = pylab.figure()
	ax = fig.add_subplot(111)
	
	isi, = sql(query,values,Array=True)
	if bins is None :
		ny,nx = histogram(isi,nbin)
	else :
		ny = histogram2(isi,bins)
		nx = bins+(bins[1]-bins[0])/2
	ax.plot(nx,ny)
	ax.set_xlabel('Interval Inter Spike (s)')

#------------------------------------------------------------------------------
def plot_crosscorrelogram(id_spiketrain1,id_spiketrain2, 
			resolution = 0.01,
			fig =None):
	
	query = """SELECT spike1.pos/spiketrain1.fs-spike2.pos/spiketrain2.fs
	 FROM spike AS spike1, spike AS spike2, spiketrain AS spiketrain1, spiketrain AS spiketrain2
	 WHERE spike1.id_spiketrain = %s AND
	 spike2.id_spiketrain = %s AND
	 spike1.id_spiketrain = spiketrain1.id_spiketrain AND
	 spike2.id_spiketrain = spiketrain2.id_spiketrain 
	"""
	values=(id_spiketrain1 , id_spiketrain2)
	diff_pos, = sql(query,values,Array=True)
	b = arange(0+resolution/2.,abs(diff_pos).max(),resolution)
	bins =r_ [-b[::-1] , b]
	ny = histogram2(diff_pos,bins)
	nx = bins+(bins[1]-bins[0])/2
	
	if fig is None :
		fig = pylab.figure()
	ax = fig.add_subplot(111)
	ax.plot(nx,ny)
	ax.set_xlabel('Crosscorelogram window (s)')
	

#------------------------------------------------------------------------------
def plot_sliding_crosscorrelogram(id_spiketrain1,id_spiketrain2, 
			sliding_center = None,
			resolution = 0.001, 
			win_size = 0.1,
			fig = None):
	
	
	b = arange(0+resolution/2.,win_size/2,resolution)
	bins =r_ [-b[::-1] , b]
	
	sliding_crosscorrelogram = zeros((bins.size , sliding_center.size))
	for i,center in enumerate(sliding_center) : 
		query = """SELECT spike1.pos/spiketrain1.fs-spike2.pos/spiketrain2.fs
				FROM spike AS spike1, spike AS spike2, spiketrain AS spiketrain1, spiketrain AS spiketrain2
				WHERE spike1.id_spiketrain = %s AND
				spike2.id_spiketrain = %s AND
				spike1.id_spiketrain = spiketrain1.id_spiketrain AND
				spike2.id_spiketrain = spiketrain2.id_spiketrain AND 
				spike1.pos/spiketrain1.fs >= %s AND
				spike2.pos/spiketrain2.fs >= %s AND
				spike1.pos/spiketrain1.fs <= %s AND
				spike2.pos/spiketrain2.fs <= %s 
				"""
		
		values=(id_spiketrain1 , id_spiketrain2 , center-win_size/2,center-win_size/2,center+win_size/2,center+win_size/2)
		diff_pos, = sql(query,values,Array=True)
		ny = histogram2(diff_pos,bins)
		ny[-1] = 0
		sliding_crosscorrelogram[:,i] = ny
	if fig is None :
		fig = pylab.figure()
	ax = fig.add_subplot(111)
	xl = sliding_center[0] , sliding_center[-1]
	yl = bins[0] , bins[-1]
	ax.imshow(sliding_crosscorrelogram, interpolation='nearest',extent = xl+yl , origin ='lower' )
	ax.set_xlabel('Time (s)')
	ax.set_ylabel('Crosscorelogram window (s)')

				
				

