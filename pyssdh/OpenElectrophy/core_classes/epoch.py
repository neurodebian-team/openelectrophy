# -*- coding: utf-8 -*-


"""
class epoch :  for storing discrete event and trigger

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



from pyssdh.core.sql_util import *
from scipy import *

import pylab



class Epoch(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg) :
		database_storage.__init__(self , **karg)
		
	#------------------------------------------------------------------------------
	table_name = 'epoch'
	list_field = [		'id_trial',
					'type',
					'label',
					'num',
					'subnum',
					'begin',
					'zero',
					'end'
				]
	field_type = [	'INDEX',
					'TEXT',
					'TEXT',
					'INT',
					'INT',
					'FLOAT',
					'FLOAT',
					'FLOAT'
				]
	list_table_child = []
	#------------------------------------------------------------------------------
	def new(self,**karg):
		for field , value in karg.iteritems() :
			self.__dict__[field] = value

	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'segment' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [ self.plot_segment ]
		list_func_plot[pos](ax=ax , **param )


	#------------------------------------------------------------------------------
	def plot_segment(self, ax , color = None ) :
		if 'nb_plot_epoch' not in dir(ax) :
			ax.nb_plot_epoch = 0
			ax.list_plot_epoch = []
		ax.nb_plot_epoch +=1
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_epoch)
			list_color = []
			for l,line in enumerate(ax.list_plot_epoch) :
				line.set_color(cmap(l))
			color = cmap(ax.nb_plot_epoch)
		xl = ax.get_xlim()
		yl = ax.get_ylim()
		if self.end is None and self.begin is not None :
			xb = self.begin
			lines = ax.plot([xb,xb] , list(yl) , '-' , color = color)
		#elif  self.end is not None and self.begin is not None :
		else  :
			xb = self.begin
			xe = self.end
			y1 = yl[0] + diff(yl)*0.95
			y2 = yl[0] + diff(yl)*0.98
			lines = ax.plot( [xb, xe] , [y1,y1] , color = color , linewidth = 3)
			ax.plot( [xb, xb] , [y1,y2] , color = 'k' , linewidth = 1)
			ax.plot( [xe, xe] , [y1,y2] , color = 'k' , linewidth = 1)
			if self.zero is not None :
				xz = self.zero
				ax.plot( [xz, xz] , [y1,y2] , color = 'k' , linewidth = 1)
			t = ''
			if self.label is not None :
				t += self.label+ ' - '
			if self.type is not None :
				t += self.type
			#ax.text([xb], y1, t,verticalalignment= 'top', horizontalalignment='left')
		ax.list_plot_epoch.append(lines[0])


	#------------------------------------------------------------------------------
	
	list_type_plot = [ 'segment'  ]
	list_option_default = [
							[ ]
						]
	list_option_param = [
							[ ]
						]
	#------------------------------------------------------------------------------




#------------------------------------------------------------------------------	
def plot_all(id_trial , ax = None,  color = 'r') :
	flag = False
	if ax is None :
		fig = pylab.figure()
		ax = fig.add_subplot(111)
		flag = True
	xl = ax.get_xlim()
	yl = ax.get_ylim()
	query = """SELECT id_epoch FROM epoch WHERE id_trial = %s"""
	id_epochs, = sql(query,id_trial)
	for id_epoch in id_epochs :
		ep = epoch()
		ep.load_from_db(id_epoch)
		x = ep.begin
		ax.plot([x,x] , list(yl) , '-' , color = color)
		x = ep.end
		ax.plot([x,x] , list(yl) , '-' , color = color)
		x = ep.zero
		ax.plot([x,x] , list(yl) , '--' , color = color)
		ax.text(x,list(yl)[1],ep.label,
				horizontalalignment='left',
		        	verticalalignment='top',
				fontsize=9,
				rotation=90)
		
	if not(flag) :
		ax.set_xlim(xl)
		ax.set_ylim(yl)

		
		
