# -*- coding: utf-8 -*-


"""
class electrode :  for managing brut signal of each electrode

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import os,stat

from scipy import *

from pyssdh.core.sql_util import *
from pyssdh.OpenElectrophy.computing import *

from timefreq import *

#from trial import *

import pylab



class Electrode(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg ) :
		database_storage.__init__(self , **karg)
		
	#------------------------------------------------------------------------------
	table_name = 'electrode'
		
	list_field = [		'id_trial',
					'signal',
					'fs',
					'name',
					'num_channel',
					'label', 
					'shift_t0' ,
					'unit'
				]
				
	field_type = [	'INDEX',
					'NUMPY',
					'FLOAT',
					'TEXT',
					'INT',
					'TEXT',
					'FLOAT' ,
					'TEXT'
				]
	list_table_child = [ 'spiketrain' , 'oscillation'   ]
	#------------------------------------------------------------------------------	
				
				
	#------------------------------------------------------------------------------
	# File section
	#------------------------------------------------------------------------------
	def load_from_file_raw(self,filename , fs , filetype ,
						num_channel=1 ,
						num_total = 1 ,
						memorytype = 'f8',
						label = u'' ,
						unit = u'' ,
						file_electrode_name = None, 
						shift_t0 = 0.
						) :
		
		filesize = os.stat(filename)[stat.ST_SIZE]
		#fid = io.fopen(filename)
		#sig = fid.read((filesize/num_total/dtype(filetype).itemsize,num_total),filetype,memorytype)   #,bs=True
		#fid.close()
		
		fid = open(filename , 'rb')
		s = fid.read(filesize)
		filetype = 'i2'
		memorytype = 'f8'
		sig = fromstring(s, dtype = filetype )
		sig = sig.reshape((filesize/num_total/dtype(filetype).itemsize,num_total)).astype(memorytype)
		fid.close()

		
		#t = arange(0,sig.shape[0],1)/fe
		self.signal = squeeze(sig[:,num_channel-1]).copy()
		self.fs = float(fs)
		self.num_channel = num_channel
		self.label = label
		self.unit = unit
		self.shift_t0 = shift_t0
		if file_electrode_name == None :
			self.name = 'electrode '+str(num_channel)+' '+filename
		else :
			import csv
			line = csv.reader(file(file_electrode_name),dialect='excel-tab')
			elec_name = [ l[0] for l in line]
			self.name = elec_name[num_channel-1]
	
	#------------------------------------------------------------------------------
	def new_from_param(self,sig = array([]), fs =None , num_channel=1 , label = u'' , shift_t0 = 0., unit = u'' , file_electrode_name = None ) :
		self.signal = squeeze(sig).copy()
		self.fs = float(fs)
		self.num_channel = num_channel
		self.label = label
		self.shift_t0 = shift_t0
		self.unit = unit
		if file_electrode_name == None :
			self.name = 'electrode '+str(num_channel)
		else :
			import csv
			line = csv.reader(file(file_electrode_name),dialect='excel-tab')
			elec_name = [ l[0] for l in line]
			self.name = elec_name[num_channel-1]
		
		
	

	#------------------------------------------------------------------------------
	def t(self) :
		return arange(0,self.signal.size,1)/self.fs  + self.shift_t0

	#------------------------------------------------------------------------------
	def resample_copy(self,new_fs) :
		elec_r = Electrode()
		for field in self.list_field :
			elec_r[field]  = self[field]
		elec_r.fs = float(new_fs)
		elec_r.label = self.label + u' resampled'
		elec_r.signal = resample(self.signal,old_fs = self.fs,new_fs = new_fs)
		
		return elec_r
	
	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'bandwidth' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [ self.plot_bandwidth , self.plot_filtered , self.plot_timefrequency ]
		list_func_plot[pos](ax=ax , **param )
		
	#------------------------------------------------------------------------------
	def plot_bandwidth(self, ax = None, sig = None, color = None,
				title = None, label = None ) :

		flag = False
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			flag = True
			
			
		if 'nb_plot_electrode_sig' not in dir(ax) :
			ax.nb_plot_electrode_sig = 0
			ax.list_plot_electrode_sig = []
		
		ax.nb_plot_electrode_sig +=1
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_electrode_sig)
			list_color = []
			for l,line in enumerate(ax.list_plot_electrode_sig) :
				line.set_color(cmap(l))
			color = cmap(ax.nb_plot_electrode_sig)
			
		if title is None :
			title = self.name
		if sig is None :
			sig = self.signal
		lines = ax.plot(self.t(),sig , color = color , label=label)
		
		ax.list_plot_electrode_sig.append(lines[0])
		ax.set_title(title)
		
		if flag :
			ax.set_xlim((self.t()[0],self.t()[-1]))
		return ax
		
	#------------------------------------------------------------------------------
	def plot_filtered(self , ax = None,  color = None,title = None, label = None,
							 f_low = 300. , f_hight = 3000.) :
		if f_low is None :f_low =0
		else :f_low = f_low/(self.fs/2)
		if f_hight is None :f_hight =1
		else :f_hight = f_hight/(self.fs/2)
		sig = fft_filter(self.signal,f_low =f_low,f_hight=f_hight )
		
		self.plot_bandwidth(ax = ax, sig = sig, color = color,
				title = title, label = label )
		


	#------------------------------------------------------------------------------
	def plot_timefrequency(self , ax = None , **param) :
		tf = TimeFreq(self , **param )
		tf.plot(ax=ax , **param)
	
	#------------------------------------------------------------------------------
	list_type_plot = [ 'bandwidth' , 'filtered' , 'time frequency' ]
	list_option_default = [
							[ ],
							[ 300., 3000.],
							[5.			,	100.,		.5 ,					-inf ,			inf,				0.000001,	2.5,		0.,		False,	nan,		nan		]
						]
	list_option_param = [
							[ ],
							[ 'f_low' , 'f_hight'],
							['f_start'	,	'f_stop',		'df_sub' ,			't_start',		't_stop',			'dt',			'f0',		'normalisation', 'colorbar'	,	'color_min',	'color_max'	]
						]
	#------------------------------------------------------------------------------
	
	
