# -*- coding: utf-8 -*-


"""
class serie :  father of all classes

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


from pyssdh.core.sql_util import *

import datetime

class Serie(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self, **karg) :
		database_storage.__init__(self, **karg)
		
		
	#------------------------------------------------------------------------------
	table_name = 'serie'
	
	list_field = [		'thedatetime',
					'info'
				]
				
	field_type = [	'DATETIME',
					'TEXT'
				]
	list_table_child = [ 'trial' ]
	#------------------------------------------------------------------------------
		
		
