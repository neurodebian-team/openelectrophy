# -*- coding: utf-8 -*-


"""

class for the time-frequencies map with morlet scalogram

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


from scipy import *
from scipy import weave
from scipy.weave import converters

from pyssdh.core.sql_util import *
from electrode import *
from pyssdh.OpenElectrophy.computing import *

import pylab


class TimeFreq :
	#------------------------------------------------------------------------------
	def __init__(self , 
			electrode , 
			f_start,f_stop,
			fs_sub = None,
			df_sub=None,
			df = None,
			t_start = None, t_stop = None, dt=None,
			f0=2.5, normalisation = 0.,
			*arg , **karg) :
		"""
		electrode : the electrode objet
		f_start , f_stop =  parameters for the frequencie of study window
		df_sub = prcision in frequencie domain for the subsample electrode
		df = precision in frequencie domain for the full band electrode
		"""
		
		#self.electrode = copy(electrode)
		self.electrode = electrode
		self.fs = self.electrode.fs
		self.f_start = f_start
		self.f_stop = f_stop
		
		if fs_sub is None : fs_sub = 2*f_stop
		self.fs_sub = fs_sub
		
		if df_sub is None :df_sub = (f_stop-f_start)/100.
		self.df_sub = df_sub
		
		if df is None : df = df_sub
		self.df = df
		
		if t_start is None or (t_start < self.electrode.t()[0]) :
			self.t_start = self.electrode.t()[0]
		else :
			self.t_start = t_start
		if t_stop is None or (t_stop > self.electrode.t()[-1]) :
			self.t_stop = self.electrode.t()[-1]
		else :
			self.t_stop = t_stop
		
		#if dt is None or dt < 1./self.fs_sub :
		#	dt = 1./self.fs_sub
		if (dt is None) or (dt < 1./self.fs) :
			dt = 1./self.fs
		#if (dt*self.fs_sub) != round((dt*self.fs_sub)) :
		#	dt = ceil((self.fs_sub*dt))/fs
		if (dt*self.fs) != round((dt*self.fs)) :
			dt = ceil((self.fs*dt))/self.fs
		self.dt = dt
		
		
		self.f0 , self.normalisation = f0 , normalisation
		
		self.tf_map_data = None
		
		self.list_oscillation_data = None
		
	#------------------------------------------------------------------------------
	def tf_map(self , recompute = False) :
		if self.tf_map_data is None or recompute :
			sig_r = self.electrode.resample_copy(self.fs_sub)
			self.tf_map_data =  self.compute_morletscalo(sig_r, 
					t_start = self.t_start, t_stop = self.t_stop, dt=self.dt,
					f_start=self.f_start,f_stop=self.f_stop,df=self.df,
					f0 = self.f0 , normalisation = self.normalisation )
					
		return self.tf_map_data
		
	#------------------------------------------------------------------------------
	def abs_tf_map_baseline_rescaled(self , recompute = False ,
									win_ref1 = -inf,
									win_ref2 = 0.,
									baseline_method = 'log_ratio'
									) :
		"""
		transform the tf_map with a baseline period
		all method are line by line (freq by freq)
		method :
			log_ratio		each pixel is ratio in log10 with the mean in win_ref
			difference		each pixel is the difference with the mean in win_ref
			z-score			each pixel is the z-score with the mean et std in win_ref
		"""
		tf = abs(self.tf_map(recompute = recompute))
		t = self.t_sub()
		ind, = where((t>=win_ref1) & (t<=win_ref2))
		m = mean(tf[ind,:], axis=0 )
		m = tile(m[newaxis,:],(tf.shape[0],1))
		s = std(tf[ind,:], axis = 0 )
		s = tile(s[newaxis,:],(tf.shape[0],1))
		if baseline_method is None :
			return tf
		if baseline_method == 'log_ratio' :
			return log10(tf/m)
		if baseline_method == 'difference' :
			return tf-m
		if baseline_method == 'z-score' :
			return (tf-m)/s
		
	#------------------------------------------------------------------------------
	def t(self) :
		return arange(self.t_start , self.t_stop , self.dt)
		
	#------------------------------------------------------------------------------
	def t_sub(self) :
		return arange(self.t_start , self.t_stop , 1./self.fs_sub)
		
	#------------------------------------------------------------------------------
	def f(self) :
		return arange(self.f_start , self.f_stop , self.df_sub)
	
	
	
	#------------------------------------------------------------------------------
	def compute_morletscalo(self,
		electrode,
		t_start = None, t_stop = None, dt=None,
		f_start=None,f_stop=None,df=None,
		f0=2.5, normalisation = 0.) :
		"""
		compute the morlet scalogram from the electrode objet
		
		t_start = None, t_stop = None, dt=None, are parameters for time windows
		f_start=None,f_stop=None,df=None, are parameters for the frequencie windows
		
		f0=2.5, normalisation = 0. are parameters for the wavelet
		
		the complexe result is in tf_map
		"""
		
		
		sig = electrode.signal
		fs = electrode.fs
		#fs = self.fs_sub
		size = electrode.signal.size
		
		#the time window
		if dt is None or dt < 1./fs:
			dt = 1./fs
		if t_start is None :
			t_start = self.t()[0]
		if t_stop is None :
			t_stop = self.t()[-1]
		if (dt*fs) != round((dt*fs)) :
			dt = ceil((fs*dt))/fs
		win_t = (r_[max(0,round((t_start-self.electrode.shift_t0)*fs)):min(round((t_stop-self.electrode.shift_t0)*fs),size):(dt*fs)]).astype('d')
		size_t = win_t.size
		
		#the frquencie window
		if f_start==None :
			f_start=0
		if f_stop==None :
			f_stop=fs/2.
		if df ==None :
			nbin = 100
		else:
			nbin = int(round(fs/2./df))
		win_f =(r_[round(f_start*2./fs*nbin):round(f_stop*2./fs*nbin):1]).astype('d')
		size_f = win_f.size
				
		map_real = zeros((win_t.size,win_f.size),'f')
		map_imag = zeros((win_t.size,win_f.size),'f')
		
		#generation of filter
		K=0.001
		M= ceil(f0*nbin*2*sqrt(2.0*log(1/K)))
		sizefilter=M+nbin+1
		tau = arange(sizefilter)
		hstar_real = exp(-1*(tau/(nbin*2*f0))**2/2.0)*cos(pi*tau/nbin)
		hstar_imag = -exp(-1*(tau/(nbin*2*f0))**2/2.0)*sin(pi*tau/nbin)
		
		expo = normalisation
		
		code = """
				#define MYMAX(x,y)  ((x) > (y) ? (x) : (y))
				#define MYMIN(x,y)  ((x) < (y) ? (x) : (y))
				
				for (int f=0;f<size_f;f++)
				{
					int ff = win_f(f);
					double factor=pow((double) (ff+1)/(f0*nbin*2),expo+1);
					for (int t=0;t<size_t;t++)
					{
						int tt=win_t(t);
						int lentaupos=MYMIN((int) (ceil((double) M/(ff+1))),size-tt)+1;
						int lentauneg=MYMIN((int) (ceil((double) M/(ff+1))),tt-1)+1;
						/* positive frequencies */
						for (int taupos=0;taupos<lentaupos;taupos++)
						{
							map_real(t,f) += factor*hstar_real(taupos*(ff+1))*sig(tt+taupos-1);
							map_imag(t,f) += factor*hstar_imag(taupos*(ff+1))*sig(tt+taupos-1);
						}
						/* positive frequencies */
						for (int tauneg = 1; tauneg<lentauneg;tauneg++)
						{
							map_real(t,f) += factor*hstar_real(tauneg*(ff+1))*sig(tt-tauneg-1);
							map_imag(t,f) += -1.0*factor*hstar_imag(tauneg*(ff+1))*sig(tt-tauneg-1);
						}
					}
				}
				"""
		
		err = weave.inline(code,
			['sig','size','map_real','map_imag','win_f','win_t' ,'size_t','size_f',
				'f0','nbin', 'hstar_real','hstar_imag','expo','M'],
			compiler = 'gcc',
			type_converters=converters.blitz)
		
		return map_real+map_imag*1j
		
	#------------------------------------------------------------------------------
	def list_oscillation(self,abs_tresh = None, std_tresh = 6) :
		if self.list_oscillation_data is None :
			self.list_oscillation_data = self.compute_oscillation(
						abs_tresh = None, std_tresh = std_tresh)
		return self.list_oscillation_data
	
	#------------------------------------------------------------------------------
	def find_oscillation_from_max(self,list_max_t , list_max_f , abs_tresh) :
		list_oscillation = [ ]
		for num_line in range(len(list_max_t)) :
			maxt,maxf,abs_max_tf = self.exact_max(list_max_t[num_line] , list_max_f[num_line])
			ind_x = argmin(abs(self.t_sub() - maxt))
			ind_y = argmin(abs(self.f() - maxf))
			tresh = (abs_tresh/abs(self.tf_map()[ind_x,ind_y]))*abs_max_tf
			tresh = float(tresh)
			line_t,line_f,line_tf = self.extract_one_line_from_max(maxt,maxf, tresh)
			osci = Oscillation()
			osci.id_trial = self.electrode.id_trial
			osci.id_electrode = self.electrode.id_electrode
			osci.fs = self.electrode.fs
			osci.shift_t0 = self.electrode.shift_t0
			osci.df = self.df
			osci.time_start = self.electrode.t()[line_t[0]]
			osci.time_stop = self.electrode.t()[line_t[-1]]
			osci.freq_start = line_f[0] * self.df
			osci.freq_stop = line_f[-1] * self.df
			osci.time_max = maxt
			osci.freq_max = maxf
			osci.val_max = abs(self.tf_map()[ind_x,ind_y])
			osci.line_t = line_t
			osci.line_f = line_f
			osci.line_val = line_tf
			list_oscillation.append(osci)
		return list_oscillation
	
	#------------------------------------------------------------------------------
	def compute_oscillation(self , 
						abs_tresh = None, std_tresh = 6,
						min_dt = 6,min_df = 6,
						min_size_cycle = 0) :
		"""
		compute oscillation line edge
		2 possibility for the treshold :
			abs_tresh  = the absolute treshold
			std_tresh = treshold is given by mean(tf_map)+std_tresh=std(tf_map)
		min_dt = 6,min_df = 6		minimum distance between 2 max on the timefrequency map
		"""
		list_max_t , list_max_f = self.local_max_2D_sub(abs_tresh = abs_tresh, std_tresh = std_tresh,
			min_dt = min_dt,min_df = min_df  )
			
		if abs_tresh is None :
			abs_tresh = abs(self.tf_map()).mean()+std_tresh*abs(self.tf_map()).std()
		list_oscillation = self.find_oscillation_from_max(list_max_t , list_max_f , abs_tresh)

		list_oscillation = self.clean_line(list_oscillation,min_size_cycle = min_size_cycle)
		return list_oscillation
	
	#------------------------------------------------------------------------------
	def local_max_2D_sub(self,
			abs_tresh = None, std_tresh = None,
			min_dt = None,min_df = None,
			f1 = -inf, f2 = inf , t1 = -inf , t2 = inf  ) :
		
		x = abs(self.tf_map())
		indx,indy = local_max_2D(x, abs_tresh = abs_tresh , std_tresh =std_tresh ,
								min_dx = min_dt, min_dy = min_df  )
		maxt = self.t_sub()[indx]
		maxf = self.f()[indy]
		
		ind,  =where( (maxt>=t1) & (maxt<=t2) & (maxt>=f1) & (maxt<=f2) )
		
		return maxt[ind] , maxf[ind]
		

	#------------------------------------------------------------------------------
	def extract_one_line_from_max(self, start_t,start_f, tresh) :
		"""
		ridge extraction of an ocillation from is local max
		"""
		
		size_sig = self.electrode.signal.size
		sig = self.electrode.signal.astype('f8')
		fs = self.electrode.fs

		step = int(round(self.dt*self.fs))
		
		f_start = self.f_start
		f_stop = self.f_stop
		df = self.df
		f0 = self.f0
		normalisation = self.normalisation
	
		if f_start==None :
			f_start=0
		if f_stop==None :
			f_stop=fs/2.
		if df ==None :
			nbin = 100
			df = fs/2./nbin
		else:
			nbin = int(round(fs/2./df))
			
		
		# to avoid bord effect
		f_start = f_start - df*3.
		f_stop = f_stop + df*3.
		
		if f_start<0. :f_start=0
		if f_stop> fs/2. : f_stop = fs/2.
		
		#print 'f_start , f_stop' , f_start , f_stop
		
		win_f =(r_[round(f_start*2./fs*nbin):round(f_stop*2./fs*nbin):1]).astype('d')
		limit_inf_f = int(win_f[0])
		limit_sup_f = int(win_f[-1])
		
		#generation of filter
		K=0.001
		M= ceil(f0*nbin*2*sqrt(2.0*log(1/K)))
		sizefilter=M+nbin+1
		tau = arange(sizefilter)
		hstar_real = exp(-1*(tau/(nbin*2*f0))**2/2.0)*cos(pi*tau/nbin)
		hstar_imag = -exp(-1*(tau/(nbin*2*f0))**2/2.0)*sin(pi*tau/nbin)
		
		expo = normalisation
		
		
		code = """
	
	/* the neigthbourg 3 point*/
	double tf_real[3];
	double tf_imag[3];
	double tf_mod[3];
	
	int dir_freq;
	
	/* first point*/
	//morletconv_xf(max_t,max_f, &tf_real[1] ,&tf_imag[1], & tf_mod[1] );
	morletconv_xf(max_t,max_f, &tf_real[1] ,&tf_imag[1], &tf_mod[1] , \
		f0 , nbin, expo, M, size_sig , \
		&hstar_real , &hstar_imag , &sig );
	
	
	while (1)
	{	
		//printf("%d  %d\\n",max_t,max_f);
		//printf("%d  \\n",size_line);
		//printf("%d  %d   %f\\n",max_t,max_f,tf_mod[1]);
		//printf("%d  %d",max_t,max_f);
		
		/* store the new point point*/
		line_t(size_line)  = max_t;
		line_f(size_line)  = max_f;
		line_tf_real(size_line) = tf_real[1];
		line_tf_imag(size_line) = tf_imag[1];
		size_line =1+size_line;
		
		/* electrode limit*/
		if (max_t >= size_sig-abs(direction) || max_t <= abs(direction))
			break;

		/* freq limit*/
		if (max_f <=(limit_inf_f+2) || max_f>=(limit_sup_f-3))
			break;
	
		/* treshhold */
		if (tf_mod[1] <= tresh)
			break;
		
		/* calculate 3 tf point*/
		for (int i=0;i<3;i++)
		{
			morletconv_xf(max_t+direction, max_f-1+i, &tf_real[i] ,&tf_imag[i], &tf_mod[i]  , \
				f0 , nbin, expo, M, size_sig , \
				&hstar_real , &hstar_imag , &sig );
		}
		
		if (tf_mod[1] >= tf_mod[0] && tf_mod[1] >= tf_mod[2] )
		{	/* same freq*/
			max_t  += direction;
			continue;
		}
		if (tf_mod[2] >= tf_mod[0] && tf_mod[2] >= tf_mod[1] )
		{
			/* freq go up*/
			dir_freq =1;
		}
		if (tf_mod[0] >= tf_mod[1] && tf_mod[0] >= tf_mod[2] )
		{	
			/* freq go down*/
			dir_freq =-1;
		}
		while (1)
		{
			if (max_f <=(limit_inf_f+2) || max_f>=(limit_sup_f-3))
				break;
			max_f += dir_freq;
			/* calculate 3 tf point*/
			for (int i=0;i<3;i++)
			{
				morletconv_xf(max_t+direction, max_f-1+i, &tf_real[i] ,&tf_imag[i], &tf_mod[i] , \
				f0 , nbin, expo, M, size_sig , \
				&hstar_real , &hstar_imag , &sig );
			}
			if (tf_mod[1] >= tf_mod[0] && tf_mod[1] >= tf_mod[2] )
				break;
		}
		max_t  += direction;
	}
	"""
	
		support_code = """
	
	#define MYMAX(x,y)  ((x) > (y) ? (x) : (y))
	#define MYMIN(x,y)  ((x) < (y) ? (x) : (y))
		
	void morletconv_xf(int tt, int ff, double* tf_real,double* tf_imag, double* tf_mod ,
				double f0 ,int nbin, double expo, double  M, int size_sig , 
				blitz::Array<double,1>* hstar_real , blitz::Array<double,1>* hstar_imag ,blitz::Array<double,1>* sig
				)
	{
		*tf_real=0.0;
		*tf_imag=0.0;
		
		double factor=pow((double) (ff+1)/(f0*nbin*2),expo+1);
		int lentaupos=MYMIN((int) (ceil((double) M/(ff+1))),size_sig-tt)+1;
		int lentauneg=MYMIN((int) (ceil((double) M/(ff+1))),tt-1)+1;
		/* positive frequencies */
		for (int taupos=0;taupos<lentaupos;taupos++)
		{
			*tf_real += factor*(*hstar_real)(taupos*(ff+1))*(*sig)(tt+taupos-1);
			*tf_imag += factor*(*hstar_imag)(taupos*(ff+1))*(*sig)(tt+taupos-1);
		}
		/* negative frequencies */
		for (int tauneg = 1; tauneg<lentauneg;tauneg++)
		{
			*tf_real += factor*(*hstar_real)(tauneg*(ff+1))*(*sig)(tt-tauneg-1);
			*tf_imag += -1.0*factor*(*hstar_imag)(tauneg*(ff+1))*(*sig)(tt-tauneg-1);
		}
		*tf_mod = pow(pow(*tf_real,2)+pow(*tf_imag,2),0.5);
	}	
	
	
		"""
		
		
		variable = ['sig','size_sig',
			'max_f','max_t',
			'limit_sup_f', 'limit_inf_f',
			'size_line','line_t','line_f','line_tf_real','line_tf_imag',
			'direction','tresh',
			'f0','nbin', 'hstar_real','hstar_imag','expo','M']
		
		#computation of the 2 half line : left and rigth
		for direction in [-step, step ] :
			max_t, max_f = int(round((start_t-self.electrode.shift_t0)*fs))+1 , int(round(start_f/df))+1
			size_line = 0
			line_t,line_f = zeros(sig.shape,dtype= 'i4'),zeros(sig.shape,dtype= 'i4')
			line_tf_real , line_tf_imag = zeros(sig.shape,dtype= 'f4'),zeros(sig.shape,dtype= 'f4')
			line_tf_real , line_tf_imag =line_tf_real*nan , line_tf_imag*nan 
			weave.inline(code,
				variable,
				support_code=support_code,
				compiler = 'gcc',
				type_converters=converters.blitz)

			size_line = line_tf_real[-isnan(line_tf_real)].size
			if direction<0 :
				if size_line>=2 :
					total_line_t = line_t[1:size_line][::-1]
					total_line_f = line_f[1:size_line][::-1]
					total_line_tf = line_tf_real[1:size_line][::-1] +line_tf_imag[1:size_line][::-1] *1j
				else :
					total_line_t = array([] , dtype='i4')
					total_line_f = array([] , dtype='i4')
					total_line_tf = array([] , dtype='f4')
			else :
				total_line_t = r_[total_line_t , line_t[:size_line]]
				total_line_f = r_[total_line_f , line_f[:size_line]]
				total_line_tf = r_[total_line_tf ,  line_tf_real[:size_line] +line_tf_imag[:size_line] *1j]
		return total_line_t, total_line_f, total_line_tf


	#------------------------------------------------------------------------------
	def exact_max(self,maxt_sub , maxf_sub) :

		"""
		the local max2D is done on subsampled time frequencie map 
		but the ridge line is done on full sampling rate so we calculate the exact
		position of local maxima in an box arround the approximative maxima
		"""
				
		#sig = self.electrode.signal
		#f0 = self.f0
		#normalisation = self.normalisation
		
		#nbin = int(round(self.fs/2./self.df))
		#nbin_sub = int(round(self.fs_sub/2./self.df_sub))
		
		t_start =max(self.electrode.t()[0],maxt_sub-(1./self.fs_sub)) 
		t_stop = min(self.electrode.t()[-1],maxt_sub+(1./self.fs_sub)) 
		f_start=max(0.,maxf_sub -self.df_sub)
		f_stop=min(self.fs_sub/2.,maxf_sub +self.df_sub)
		tf_box = self.compute_morletscalo(self.electrode, 
			t_start = t_start, t_stop = t_stop, dt = self.dt ,
			f_start=f_start, f_stop=f_stop,df=self.df,
			f0=self.f0, normalisation =self.normalisation) # box arround the approx max
		ind_t,ind_f = where(abs(tf_box).max() <= abs(tf_box)) # max in the box
		

		#exact max
		maxt = t_start + ind_t[0]*self.dt
		maxf = f_start + ind_f[0]*self.df
		abs_max_tf = abs(tf_box[ind_t[0],ind_f[0]])
		
		# to align on a grid if step is not 1
		step = int(round(self.dt*self.fs))
		if step != 1:
			maxt -= (maxt -self.electrode.t()[0])%self.dt
			
		
		
		return maxt,maxf,abs_max_tf
	
	
	#------------------------------------------------------------------------------
	def clean_line(self,list_oscillation,
		eliminate_simultaneous = True,
		regroup_overlap = True , 
		eliminate_partial_overlap = True,
		min_size_cycle = 0) :
		
		i = 0
		while i<len(list_oscillation) :
			#print 'i',i
			#checking for a minimum size in cycle
			if where((angle(list_oscillation[i].line_val[:-1]) <=0)&(angle(list_oscillation[i].line_val[1:]) >=0))[0].size <min_size_cycle :
				list_oscillation = list_oscillation[:i] + list_oscillation[i+1:]
				
				continue
			j = i+1
			while j<len(list_oscillation) :
				if intersect1d(list_oscillation[i].line_t,list_oscillation[j].line_t).size != 0 :
					#overlap
					# the 2 lines have same absisse for some point
					ind_i, = where(setmember1d(list_oscillation[i].line_t,list_oscillation[j].line_t))
					ind_j, = where(setmember1d(list_oscillation[j].line_t,list_oscillation[i].line_t))
					
					if ((list_oscillation[i].line_f[ind_i] != list_oscillation[j].line_f[ind_j]).sum() == 0) and regroup_overlap :
						# the 2 lines have same absisse and same freq for some point : we regroup them
						ind_keep_j = where(setmember1d(list_oscillation[j].line_t,list_oscillation[i].line_t) == False)
						list_oscillation[i].line_t = r_[list_oscillation[i].line_t , list_oscillation[j].line_t[ind_keep_j]]
						list_oscillation[i].line_f = r_[list_oscillation[i].line_f , list_oscillation[j].line_f[ind_keep_j]]
						list_oscillation[i].line_val = r_[list_oscillation[i].line_val , list_oscillation[j].line_val[ind_keep_j]]
						ind_sort = argsort(list_oscillation[i].line_t )
						list_oscillation[i].line_t = list_oscillation[i].line_t[ind_sort]
						list_oscillation[i].line_f = list_oscillation[i].line_f[ind_sort]
						list_oscillation[i].line_val = list_oscillation[i].line_val[ind_sort]
						del list_oscillation[j]
						continue
						
					if ((list_oscillation[i].line_f[ind_i] == list_oscillation[j].line_f[ind_j]).sum() == 0) and \
							eliminate_simultaneous :
						# the 2 lines have some same absisse and but not same freq for some point : we keep the higher max
						if abs(list_oscillation[i].val_max)<abs(list_oscillation[j].val_max) :
							list_oscillation[i] = list_oscillation[j]
						del list_oscillation[j]
						continue
					
					if (((list_oscillation[i].line_f[ind_i] != list_oscillation[j].line_f[ind_j]).sum() != 0)  and \
							((list_oscillation[i].line_f[ind_i] == list_oscillation[j].line_f[ind_j]).sum() != 0)) and \
							eliminate_partial_overlap  :
						# the 2 lines have some same absisse and same freq for some point  
						# and for other point same abscise  not same freq : we keep the higher max
						if abs(list_oscillation[i].val_max)<abs(list_oscillation[j].val_max) :
							list_oscillation[i] = list_oscillation[j]
						del list_oscillation[j]
						continue
				j+=1
			i+=1
		return list_oscillation
					
	#------------------------------------------------------------------------------
	def save_oscillations(self) :
		
		if is_table('oscillation') :
			query = """DELETE oscillation FROM oscillation 
					WHERE
					id_electrode = %s """
			sql(query,(self.electrode.id_electrode ))
		for osci  in self.list_oscillation_data :
			osci.save_to_db()
	
	#------------------------------------------------------------------------------
	def load_oscillations(self) :
		self.list_oscillation_data =load_oscillations(id_electrode = self.electrode.id_electrode)
			
	#------------------------------------------------------------------------------
	def plot(self, ax = None, 
				title = None, label = None ,
				colorbar = False,
				color_min = nan,
				color_max = nan,
				baseline_method = None,
				win_ref1 = -inf,
				win_ref2 = 0.,
				**karg ) :
		"""
		colorbar				show colorbar
		color_min, color_max 	color scale ( nan -> auto )
		
		baseline_method		rescale map with a baseline window
		win_ref1,win_ref2 		window for baseline if map rescaled
		"""
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
		if title is None :
			title = self.electrode.name
		#xl = self.electrode.t()[0], self.electrode.t()[-1]
		xl = self.t_start , self.t_stop
		yl = self.f_start , self.f_stop
		
		if baseline_method == None :
			tf_map = abs(self.tf_map())
		else :
			tf_map = self.abs_tf_map_baseline_rescaled(win_ref1 = win_ref1,
									win_ref2 = win_ref2,
									baseline_method = baseline_method)

		im = ax.imshow(tf_map.transpose(), interpolation='nearest',extent = xl+yl , origin ='lower' , aspect = 'normal')
		
		if not(isnan(color_min)) and not(isnan(color_max)) :
			#im = ax.get_images()[0]
			im.set_clim(color_min,color_max)
		if colorbar :
			ax.get_figure().colorbar(im)
		ax.set_xlim(xl)
		ax.set_ylim(yl)
		
		if title is not None :
			ax.set_title(title)
		return ax
		
		
	
	#------------------------------------------------------------------------------
	def plot_line(self, ax = None, col ='m',
				title = None) :
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
		if title is None :
			title = self.electrode.name
		for oscillation in self.list_oscillation_data :
			#ax.plot((oscillation.line_t+1.)/self.electrode.fs,(oscillation.line_f+1.)*self.df,col)
			ax.plot(self.electrode.t()[oscillation.line_t],(oscillation.line_f+1.)*self.df,col)
		if titl is not None :
			ax.set_title(title)
		return ax
	
	#------------------------------------------------------------------------------
	def plot_oscillation(self, ax = None,  col = 'b',
				titl = None, label = None ) :
		flag = False
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			flag = True
		xl,yl = ax.get_xlim() , ax.get_ylim()
		ax.plot(self.electrode.t(),self.electrode.signal , color = col , label=label)
		for oscillation in self.list_oscillation_data :
			#ax.plot((oscillation.line_t+1.)/self.electrode.fs,cos(angle(oscillation.line_val))*(abs(array(yl)).min()),'m')
			ax.plot(self.electrode.t()[oscillation.line_t],cos(angle(oscillation.line_val))*(abs(array(yl)).min()),'m')
		if flag :
			ax.set_xlim(xl)
			ax.set_ylim(yl)
		return ax
	
	
#------------------------------------------------------------------------------
def load_oscillations(id_electrode=None,
					list_id_oscillation=None) :
	if id_electrode is not None:
		list_oscillation_data = []
		query = """ SELECT oscillation.id_id_oscillation
				FROM oscillation
				WHERE oscillation.id_electrode = %s
				"""
		list_id_oscillation = sql(query,(id_electrode))[0]
	list_oscillation_data = []
	for id_oscillation in  list_id_oscillation :
		osci = Oscillation()
		osci.load_from_db(id_oscillation)
		list_oscillation_data.append(osci)
	return list_oscillation_data





#------------------------------------------------------------------------------
class Oscillation(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self ,**karg) :
		database_storage.__init__(self , **karg)
	#------------------------------------------------------------------------------
	table_name = 'oscillation'
	list_field = [		'id_trial',
					'id_electrode',
					'fs',
					'shift_t0',
					'df',
					'time_start',
					'time_stop',
					'freq_start',
					'freq_stop',
					'time_max',
					'freq_max',
					'val_max',
					'line_t',
					'line_f',
					'line_val'
				]
	field_type = [	'INDEX',
					'INDEX',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					'NUMPY',
					'NUMPY',
					'NUMPY'
				]
	#------------------------------------------------------------------------------
	def t(self) :
		query = 'SELECT signal_shape FROM electrode WHERE id_electrode = %s'
		sh, = sql1(query,self.id_electrode)
		sig_size = int(sh[1:-2])
		return arange(0,sig_size,1)/self.fs  + self.shift_t0
	#------------------------------------------------------------------------------
	list_type_plot = [ 'oscillation' ]
	list_option_default = [
							[ ]
						]
	list_option_param = [
							[ ]
						]
	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'oscillation' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [ self.plot_oscillation ]
		list_func_plot[pos](ax=ax , **param )
	
	#------------------------------------------------------------------------------
	def plot_oscillation(self , ax , color = 'm') :
		if 'nb_plot_electrode_sig' in dir(ax) :
			xl,yl = ax.get_xlim() , ax.get_ylim()
			return ax.plot(self.t()[self.line_t],cos(angle(self.line_val))*(abs(array(yl)).min()),color = color)
		else :
			#l = ax.plot(self.t()[self.line_t],(self.line_f+1.)*self.df,color = color)
			l = ax.plot(self.t()[self.line_t],(self.line_f)*self.df,color = color)
			
			ax.plot([self.time_max] , [self.freq_max], 'x', color = color)
			return l


