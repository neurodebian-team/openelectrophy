# -*- coding: utf-8 -*-

"""
arbitrary class for managing an abrbitrary signal

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *
import pylab
import os,stat

from pyssdh.core import *
from pyssdh.OpenElectrophy.core_classes import *

# declaration of trial child

if 'auxilary'  not in Trial.list_field :
	Trial.list_table_child += [ 'auxilary' ]




import time



#------------------------------------------------------------------------------
class Auxilary(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , id_trial = None ,) :
		database_storage.__init__(self)
		self.id_trial = id_trial
		
	table_name = 'auxilary'
	list_field = [		'id_trial',
					'signal',
					'fs' , 
					'shift_t0', 
					'unit'
				]
	field_type = [	'INDEX',
					'NUMPY',
					'FLOAT',
					'FLOAT',
					'TEXT'
				]

	#------------------------------------------------------------------------------
	def new_from_param(self,sig = array([]), fs =None ,  shift_t0 = 0., unit = u'' ) :
		self.signal = squeeze(sig).copy()
		self.fs = float(fs)
		self.shift_t0 = shift_t0
		self.unit = unit
		
	#------------------------------------------------------------------------------
	def t(self) :
		return arange(0,self.signal.size,1)/self.fs + self.shift_t0
	
	#------------------------------------------------------------------------------
	def plot(self, ax = None, col = None ,
				titl = None, label = None ) :
		
		flag = False
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			flag = True
			
		if 'nb_plot_auxilary_sig' not in dir(ax) :
			ax.nb_plot_auxilary_sig = 0
			ax.list_plot_auxilary_sig = []
		
		ax.nb_plot_auxilary_sig +=1
		if col is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_auxilary_sig)
			list_color = []
			for l,line in enumerate(ax.list_plot_auxilary_sig) :
				line.set_color(cmap(l))
			col = cmap(ax.nb_plot_auxilary_sig)
			
		if titl is None :
			titl = u'Auxilary'
		lines = ax.plot(self.t(),self.signal , color = col , label=label)
		ax.list_plot_auxilary_sig.append(lines[0])
		ax.set_title(titl)
		if flag :
			ax.set_xlim((self.t()[0],self.t()[-1]))
			
		return ax

