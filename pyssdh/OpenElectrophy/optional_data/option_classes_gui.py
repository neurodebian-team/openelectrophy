# -*- coding: utf8 -*-
#
#
# module for managing extra data ex : repsiration, ...
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 09.2006
#


from respiration.dialog_edit_respiration import *

dict_optional_data_browsable = 	{
								'respiration' : True ,
								'epochrespiration' : False ,
								'auxilary' : True ,
								'licktrain' : True,
								'lick' : False
								}

dict_optional_data_shown_field = {
								'respiration' : [ 'name', ] ,
								'epochrespiration' : [ ] ,
								'auxilary' : [ ] ,
								'licktrain' : [ 'start', 'stop'],
								'lick' : False
								}



dict_optional_data_context_menu = {
								'respiration' : { 'Edit cycle' : OpenWindowEditRespiration } ,
								'epochrespiration' : { } ,
								'auxilary' : { } ,
								'licktrain' : {},
								'lick' : {}
									}

