# -*- coding: utf-8 -*-

"""
classe for licking stuying

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



from scipy import *
import pylab
import os,stat

from pyssdh.core import *
from pyssdh.OpenElectrophy.core_classes import *

# declaration of trial child

if 'licktrain'  not in Trial.list_field :
	Trial.list_table_child += [ 'licktrain' ]



#------------------------------------------------------------------------------
class LickTrain(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg) :
		database_storage.__init__(self , **karg)
		
	table_name = 'licktrain'
	list_field = [		'id_trial',
					'info',
					'start',
					'stop'
				]
	field_type = [		'INDEX',
					'TEXT' ,
					'FLOAT',
					'FLOAT'
				]
	list_table_child = [ 'lick' ]
	#------------------------------------------------------------------------------
	def get_lick_time(self) :
		query = """
				SELECT lick.start
				FROM lick
				WHERE id_licktrain = %s
				"""
		start, = sql(query , (self.id_licktrain) , Array = True)
		return start

#------------------------------------------------------------------------------
class Lick(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg) :
		database_storage.__init__(self , **karg)
		
	table_name = 'lick'
	list_field = [		'id_licktrain',
					'num',
					'start',
					'stop'
				]
	field_type = [		'INDEX',
					'INT' ,
					'FLOAT',
					'FLOAT'
				]

