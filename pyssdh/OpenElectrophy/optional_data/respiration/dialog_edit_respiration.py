# -*- coding: utf8 -*-

"""
dialog for edition of segmentation of respiration

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from pyssdh.core.gui import *
from pyssdh.OpenElectrophy.core_classes import *

from scipy import * 
import pylab

from respiration import *

class WindowEditRespiration(QWidget):
	def __init__(self, parent=None , id_respiration=None , n_segment = 5 ):
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		
		#design
		mainLayout = QVBoxLayout()
		
		v2 = QVBoxLayout()
		self.canvas = MyMplCanvas()
		self.fig = self.canvas.fig
		self.ax = self.canvas.ax
		v2.addWidget(self.canvas)
		self.figToolBar = MyNavigationToolbar(self.canvas , self.canvas , direction = 'h')
		v2.addWidget(self.figToolBar)
		mainLayout.addLayout(v2)
		
		h3 = QHBoxLayout()
		
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		h.addStretch(10)
		self.buttonDeleteAll = QPushButton('Delete all')
		self.buttonDeleteAll.setIcon(dict_icon['emptytrash'])
		h.addWidget(self.buttonDeleteAll)
		self.buttonDelete = QPushButton('Delete cycle')
		self.buttonDelete.setIcon(dict_icon['emptytrash'])
		h.addWidget(self.buttonDelete)
		self.spinboxNumDelete = QSpinBox()
		h.addWidget(self.spinboxNumDelete)
		h.addStretch(10)
		self.buttonInsert = QPushButton('Insert cycle after :')
		self.buttonInsert.setIcon(dict_icon['redo'])
		h.addWidget(self.buttonInsert)
		self.spinboxNumInsert = QSpinBox()
		h.addWidget(self.spinboxNumInsert)
		h.addStretch(10)
		frame.setLayout(h)
		h3.addWidget(frame)

		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		h.addStretch(10)
		self.buttonCompute = QPushButton('Recompute')
		self.buttonCompute.setIcon(dict_icon['exec'])
		h.addWidget(self.buttonCompute)
		self.comboNSegment = QComboBox()
		self.comboNSegment.setEditable(False)
		self.comboNSegment.addItem('5')
		self.comboNSegment.addItem('2')
		self.connect(self.comboNSegment,SIGNAL('currentIndexChanged( int )'),self.changeSegment)
		
		h.addWidget(self.comboNSegment)
		h.addStretch(10)
		frame.setLayout(h)
		h3.addWidget(frame)


		mainLayout.addLayout(h3)
		
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		self.buttonSave = QPushButton('Save to databse')
		self.buttonSave.setIcon(dict_icon['filesave'])
		h.addWidget(self.buttonSave)
		self.buttonLoad = QPushButton('Reload from database')
		self.buttonLoad.setIcon(dict_icon['reload'])
		h.addWidget(self.buttonLoad)
		h.addStretch(10)
		self.buttonQuit = QPushButton('Quit')
		self.buttonQuit.setIcon(dict_icon['fileclose'])
		h.addWidget(self.buttonQuit)
		frame.setLayout(h)
		mainLayout.addWidget(frame)
		
		
		self.connect(self.buttonSave , SIGNAL('clicked()') , self.save_to_db)
		self.connect(self.buttonLoad , SIGNAL('clicked()') , self.reload_from_db)
		self.connect(self.buttonInsert , SIGNAL('clicked()') , self.insert)
		self.connect(self.buttonDeleteAll , SIGNAL('clicked()') , self.deleteAll)
		self.connect(self.buttonDelete , SIGNAL('clicked()') , self.delete)
		self.connect(self.buttonCompute , SIGNAL('clicked()') , self.compute)
		self.connect(self.buttonQuit , SIGNAL('clicked()') , self.close)
		
		self.n_segment = n_segment
		
		self.id_respiration = id_respiration
		self.resp = Respiration()
		#plotting
		self.reload_from_db()
		#self.refresh_plot()
		
		n_cycle = len(self.resp.respiratory_cycle[0])
		self.spinboxNumDelete.setRange(1,n_cycle)
		self.spinboxNumInsert.setRange(1,n_cycle)
		
		self.setLayout(mainLayout)
		
		
	#def ok(self) :
	#	print 'ok'
		
	def cancel(self) :
		pass
		#print 'cancel'
		
	def after_move(self) :
		self.refresh_limit()
	
	def sync_cursor_resp(self) : 
		self.resp.respiratory_cycle = [ ]
		for n in range(self.n_segment) :
			self.resp.respiratory_cycle.append(array([ ]))
			for c in range(len(self.list_cursor)) :
				v = int((self.list_cursor[c][n].coor()-self.resp.shift_t0)*self.resp.fs)
				self.resp.respiratory_cycle[-1] = r_[self.resp.respiratory_cycle[-1],v]
		if len(self.list_cursor)>0:
			v = int((self.last_cursor.coor()-self.resp.shift_t0)*self.resp.fs)
			end = r_[self.resp.respiratory_cycle[0][1:] ,  v]
		else :
			end = array([ ])
		self.resp.respiratory_cycle.append(end)
	
	def save_to_db(self) :
		self.sync_cursor_resp()
		self.resp.save_respiratory_epoch()
		
	
	def reload_from_db(self) :
		self.resp.load_from_db(self.id_respiration)
		self.resp.load_respiratory_epoch(self.n_segment)
		#~ self.refresh_plot()
		self. replot()
		n_cycle = len(self.resp.respiratory_cycle[0])
		self.spinboxNumDelete.setRange(1,n_cycle)
		self.spinboxNumInsert.setRange(1,n_cycle)
	
	def changeSegment(self, pos):
		self.n_segment = int(self.comboNSegment.currentText())
		self.reload_from_db()
		
	
	def replot(self):
		if 'list_cursor' in dir(self) :
			for lc in self.list_cursor :
				for c in lc :
					#~ print 'delete'
					c.delete()
			del self.list_cursor
		if 'last_cursor' in dir(self) :
			self.last_cursor.delete()
			del self.last_cursor
		self.n_segment = int(self.comboNSegment.currentText())
		self.ax.clear()
		self.resp.plot_bandwidth(ax= self.ax , title = u'')
		self.refresh_plot( refresh_xlim = True )

	
	def refresh_plot(self, refresh_xlim = True) :
		

		if 'list_cursor' in dir(self) :
			for lc in self.list_cursor :
				for c in lc :
					#~ print 'delete'
					c.delete()
		if 'last_cursor' in dir(self) :
			self.last_cursor.delete()
		
		#~ if 'list_dragingcursor' in dir(self.canvas):
			#~ print len(self.canvas.list_dragingcursor),self.canvas.list_dragingcursor,'len(self.canvaslist_dragingcursor)',type(self.canvas.list_dragingcursor)
			#~ print len(self.canvas.list_dragingcursor),'len(self.canvaslist_dragingcursor)',type(self.canvas.list_dragingcursor)
			#~ self.canvas.list_dragingcursor = [ ]
		
		#~ self.resp.plot_bandwidth(ax= self.ax , title = u'')
		if refresh_xlim :
			self.ax.set_xlim((self.resp.t()[0],self.resp.t()[-1]))
		cmap = pylab.get_cmap('jet',len(self.resp.respiratory_cycle[0]))
		self.list_cursor = [ ]
		for c in range(len(self.resp.respiratory_cycle[0])) :
			cycle = [ ]
			for n in range(self.n_segment) :
				if n==0 : 
					linestyle = '-'
					lw = 2
				else : 
					linestyle = '--'
					lw = 1
				x = self.resp.respiratory_cycle[n][c]/self.resp.fs+self.resp.shift_t0
				mc = DragingCursor(self.ax , direction = 'ver' , 
							coor = x ,useblit=False,
							#label='c'+str(c+1)+'n'+str(n+1),
							label=str(c+1),
							func_after_move = self.after_move,
							color=cmap(c), linewidth=lw , linestyle = linestyle )
				cycle.append(mc)
			self.list_cursor.append(cycle)
		
		if len(self.resp.respiratory_cycle[-1]) >=1 :
			print 'yep'
			x = self.resp.respiratory_cycle[-1][-1]/self.resp.fs+self.resp.shift_t0
			self.last_cursor = DragingCursor(self.ax , direction = 'ver' , 
								coor = x ,useblit=False,
								label='',
								func_after_move = self.after_move,
								color=cmap(c), linewidth=2 , linestyle = '-' )
		self.refresh_limit()
		self.canvas.draw_idle()
		
	def refresh_limit(self) :
		for c in range(len(self.list_cursor)) :
			for n in range(self.n_segment) :
				if (c==0) and (n ==0) :
					self.list_cursor[c][n].limit1 = None
				elif n ==0 :
					self.list_cursor[c][n].limit1 = self.list_cursor[c-1][self.n_segment-1].coor()
				else : 
					self.list_cursor[c][n].limit1 = self.list_cursor[c][n-1].coor()
					
				if (c== len(self.list_cursor)-1 )  and  (n ==self.n_segment-1)  :
					self.list_cursor[c][n].limit2 = self.last_cursor.coor()
				elif n ==self.n_segment-1 :
					self.list_cursor[c][n].limit2 = self.list_cursor[c+1][0].coor()
				else : 
					self.list_cursor[c][n].limit2 = self.list_cursor[c][n+1].coor()
				
	def insert(self) :
		self.sync_cursor_resp()
		n_cycle = len(self.resp.respiratory_cycle[0])
		
		print n_cycle
		if n_cycle == 0 :
			for i in range(self.n_segment+1) :
				self.resp.respiratory_cycle[i] = r_[int(i*self.resp.fs*.2) ]
		else:
			c = self.spinboxNumInsert.value() - 1
			s = self.resp.respiratory_cycle[self.n_segment-1][c]
			e = self.resp.respiratory_cycle[self.n_segment][c]
			print s, e
			for n in range(self.n_segment) :
				self.resp.respiratory_cycle[n] = r_[self.resp.respiratory_cycle[n][:c+1] , s+(n+.5)*((e-s)/self.n_segment), self.resp.respiratory_cycle[n][c+1:] ]
			self.resp.respiratory_cycle[self.n_segment] = r_[self.resp.respiratory_cycle[self.n_segment][:c] ,  self.resp.respiratory_cycle[0][c+1]  , self.resp.respiratory_cycle[self.n_segment][c:] ]
			
		self.spinboxNumDelete.setRange(1,n_cycle+1)
		self.spinboxNumInsert.setRange(1,n_cycle+1)
			
		self.refresh_plot(refresh_xlim = False)
		
	def delete(self) :
		self.sync_cursor_resp()
		c = self.spinboxNumDelete.value() - 1
		for n in range(self.n_segment) :
			self.resp.respiratory_cycle[n] = r_[self.resp.respiratory_cycle[n][:c] , self.resp.respiratory_cycle[n][c+1:] ]
		n_lim = max(c-1,0)
		self.resp.respiratory_cycle[self.n_segment] = r_[self.resp.respiratory_cycle[self.n_segment][:n_lim] , self.resp.respiratory_cycle[self.n_segment][n_lim+1:] ]

		n_cycle = len(self.resp.respiratory_cycle[0])
		self.spinboxNumDelete.setRange(1,n_cycle)
		self.spinboxNumInsert.setRange(1,n_cycle)
			
		self.refresh_plot(refresh_xlim = False)
		
	def deleteAll(self):
		
		self.resp.respiratory_cycle= [  array([]) for n in range(self.n_segment) ]
		self.replot()
		
	def compute(self) : 
		self.n_segment = int(self.comboNSegment.currentText())
		self.resp.compute_segmentation(n_segment = self.n_segment)
		n_cycle = len(self.resp.respiratory_cycle[0])
		self.spinboxNumDelete.setRange(1,n_cycle)
		self.spinboxNumInsert.setRange(1,n_cycle)
		self.refresh_plot()

def OpenWindowEditRespiration(parent , id_respiration) :
	w = WindowEditRespiration(parent = parent , id_respiration = id_respiration)
	w.show()

