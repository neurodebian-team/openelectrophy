# -*- coding: utf-8 -*-

"""
class for studying the respiratory cycle

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *
from scipy import interpolate
from scipy import stats
import pylab
import os,stat


from pyssdh.core.sql_util import *
from pyssdh.core.ndsummer import *
from pyssdh.OpenElectrophy.core_classes import *


# declaration of trial child
if 'respiration'  not in Trial.list_field :
	Trial.list_table_child += [ 'respiration' ]

import time



class EpochRespiration(Epoch) :
	table_name = 'epochrespiration'
	list_field = [	'id_trial',
					'type',
					'label',
					'num',
					'subnum',
					'begin',
					'zero',
					'end',
					'id_respiration'
				]
	field_type = [	'INDEX',
					'TEXT',
					'TEXT',
					'INT',
					'INT',
					'FLOAT',
					'FLOAT',
					'FLOAT',
					 'INDEX'
				]
	list_table_child = []
	def __init__(self) :
		Epoch.__init__(self)
	


#--------------------------------------------------------------------------------------
class Respiration(database_storage) :
	#------------------------------------------------------------------------------
	def __init__(self , **karg ) :
		database_storage.__init__(self , **karg)
		
	table_name = 'respiration'
	list_field = [			'id_trial',
					'name',
					'signal',
					'fs' , 
					'shift_t0', 
					'unit'
				]
	field_type = [			'INDEX',
					'TEXT',
					'NUMPY',
					'FLOAT',
					'FLOAT',
					'TEXT'
				]
	list_table_child = ['epochrespiration']
	
	#------------------------------------------------------------------------------
	# File section
	#------------------------------------------------------------------------------
	def load_from_file_raw(self,
						filename , fs , filetype ,
						num_channel=1 ,
						num_total = 1 ,
						memorytype = 'f8',
						unit = u'',
						shift_t0 = None
						) :
		
		filesize = os.stat(filename)[stat.ST_SIZE]
		#fid = io.fopen(filename)
		#sig = fid.read((filesize/num_total/dtype(filetype).itemsize,num_total),filetype,memorytype)   #,bs=True
		#fid.close()
		
		fid = open(filename , 'rb')
		s = fid.read(filesize)
		filetype = 'i2'
		memorytype = 'f8'
		sig = fromstring(s, dtype = filetype )
		sig = sig.reshape((filesize/num_total/dtype(filetype).itemsize,num_total)).astype(memorytype)
		fid.close()
		
		
		#t = arange(0,sig.shape[0],1)/fe
		self.signal = squeeze(sig[:,num_channel]).copy()
		self.fs = float(fs)
		self.id_respiration = None
		self.signal_memorytype = memorytype
		self.shift_t0 = shift_t0
		self.unit = unit
		
		self.respiratory_cycle= []
	
	#------------------------------------------------------------------------------
	def new_from_param(self,sig = array([]), fs =None ,  shift_t0 = 0., unit = u'' ) :
		self.signal = squeeze(sig).copy()
		self.fs = float(fs)
		self.shift_t0 = shift_t0
		self.unit = unit
	
	#------------------------------------------------------------------------------
	def compute_segmentation5(self,
								freq_max = 5,
								freq_lowpass = 30,
								) :
		
		sig_max = fft_filter(self.signal,f_hight = (freq_max*2)/self.fs)
		sig_low = fft_filter(self.signal,f_hight = (freq_lowpass*2)/self.fs)
		
		#tresh1 = (sig_max.max()-sig_max.min())*.6 + sig_max.min()
		#tresh1 = (sig_max.max()-sig_max.mean())*.5 + sig_max.mean()
		tresh1 = sig_max.mean() + sig_max.std()
	
		
		#local max
		list_max, = where((sig_max[1:-1] >=tresh1) & 
						(sig_max[1:-1] > sig_max[:-2]) &
						(sig_max[1:-1] >= sig_max[2:]) )
		#local min
		list_min = list_max[:-1].copy()
		list_start = list_max[:-1].copy()
		list_trans = list_max[:-1].copy()
		list_plat = list_max[:-1].copy()
		
		for m, ma in enumerate(list_max[:-1]) :
			ind = argmin(self.signal[ma:list_max[m+1]])
			list_min[m] = list_min[m] + ind
		
		for c in range(list_min.size) :
			s1 = sig_low[list_max[c]:list_min[c]]
			s2 = sig_low[list_min[c]:list_max[c+1]]
			
			#searching the plateau :
			#n_point_hist=100
			
			score1 = stats.scoreatpercentile(s1 , 10)
			score2 = stats.scoreatpercentile(s1 , 90)
			s1bis = s1[ (s1>=score1) & (s1<=score2) ]
			
			n_point_hist=s1.size/12
			his , val = histogram(s1bis,n_point_hist)
			his = his/his.sum().astype('f')
			ind_max_his = argmax(his)
			plateau = val[ind_max_his]


			#~ n_point_hist=s1.size/12
			#~ his , val = histogram(s1,n_point_hist)
			#~ his = his/his.sum().astype('f')
			#~ ind_max_his = argmax(his[int(n_point_hist/10):-int(n_point_hist/10)])
			#~ ind_max_his = ind_max_his + int(n_point_hist/10) -1
			#~ plateau = val[ind_max_his]

			
			ds = r_[diff(s1), 0]
			d2s = r_[0, diff(diff(s1)), 0 ]
			d2s_bis = [0, diff(diff(s1)[1:]),0, 0 ]

			#searching end of the plateau
			ind_plateau, = where((abs(s1-plateau) <= 0.3*abs(s1[-1]-plateau)) &
									(abs(ds)<.4*abs(ds.min())))
			if ind_plateau.size == 0 :
				#cant find the plateau
				if c>2 :
					#
					list_start[c] = mean((list_start[:c]-list_max[:c]).astype('f')/(list_max[1:c+1]-list_max[:c])) \
						*(list_max[c+1]-list_max[c]) \
						+list_max[c]
					list_trans[c] = mean((list_trans[:c]-list_max[:c]).astype('f')/(list_max[1:c+1]-list_max[:c])) \
						*(list_max[c+1]-list_max[c]) \
						+list_max[c]
					list_plat[c] = mean((list_plat[:c]-list_max[:c]).astype('f')/(list_max[1:c+1]-list_max[:c])) \
						*(list_max[c+1]-list_max[c]) \
						+list_max[c]
				else : 
					list_start[c] = 0.5*(list_max[c+1]-list_max[c])+list_max[c]
					list_trans[c] = 0.8*(list_max[c+1]-list_max[c])+list_max[c]
					list_plat[c] = 0.1*(list_max[c+1]-list_max[c])+list_max[c]
			else :
				#local max
				ind_maxl, = where((s1[1:-1] >s1[:-2]) & (s1[1:-1] >= s1[2:] ))
				#local min
				ind_minl, = where((s1[1:-1] <s1[:-2]) & (s1[1:-1] <= s1[2:] ))
				#inflexion point
				ind_infl, = where( (abs(ds)<0.5*max((abs(ds))))&
									(d2s >0) &  (d2s_bis<=0) )
									
				#end of plateau
				index = intersect1d(ind_maxl,ind_plateau)
				if index.size >=1 :
					list_start[c] = index[-1] +list_max[c]
				elif index.size ==0 :
					index2 = intersect1d(ind_infl ,ind_plateau )
					if index2.size ==0 :
						#list_start[c] = ind_plateau[ind_plateau.size/2] +list_max[c]
						list_start[c] = ind_plateau[-1] +list_max[c]
					else :
						list_start[c] = index2[-1] +list_max[c]
				
				#begin of plateau
				index = intersect1d(ind_minl,ind_plateau)
				if index.size >=1 :
					list_plat[c] = index[0] +list_max[c]
				elif index.size ==0 :
					index2 = intersect1d(ind_infl ,ind_plateau )
					if index2.size ==0 :
						#list_start[c] = ind_plateau[ind_plateau.size/2] +list_max[c]
						list_plat[c]= ind_plateau[0] +list_max[c]
					else :
						list_plat[c] = index2[0] +list_max[c]

						
				val_trig = s1[list_start[c]-list_max[c]]

				ind, = where((s2[:-1] < val_trig) & (s2[1:] >= val_trig))
				if ind.size != 0 :
					list_trans[c] = ind[-1]+list_min[c]
				else :
					list_trans[c] = (list_start[c] +list_max[c+1] ) /2
			if c>=1 :
				list_max[c] = argmax(self.signal[list_start[c-1]:list_start[c]]) + list_start[c-1]
		
		list_end = list_start[1:] -1
		list_start = list_start[:-1]
		list_min = list_min[:-1]
		list_trans = list_trans[:-1]
		list_max = list_max[1:-1]
		list_plat = list_plat[1:]
		
		self.respiratory_cycle = [list_start,list_min, list_trans ,list_max,  list_plat , list_end]
		
	#------------------------------------------------------------------------------
	def compute_segmentation2(self,
								freq_max = 5,
								freq_lowpass = 30,
								) :
		self.compute_segmentation5(freq_max =freq_max ,freq_lowpass = freq_lowpass)
		self.respiratory_cycle = [ self.respiratory_cycle[0] , self.respiratory_cycle[2] , self.respiratory_cycle[5]]
	
	#------------------------------------------------------------------------------
	def compute_segmentation(self,
							n_segment = 5 ,
							freq_max = 5,
							freq_lowpass = 30
							) :
			
		if n_segment == 5 :
			self.compute_segmentation5(freq_max =freq_max ,freq_lowpass = freq_lowpass)
		elif n_segment == 2 : 
			self.compute_segmentation2(freq_max =freq_max ,freq_lowpass = freq_lowpass)
		else :
			self.respiratory_cycle = [[]]
		
	#------------------------------------------------------------------------------
	def save_respiratory_epoch(self) :
		
		n_segment = len(self.respiratory_cycle)-1
		typ = 'repiration '+str(n_segment)+' segment' 
		
		if is_table('epochrespiration') :
			query = """DELETE epochrespiration FROM epochrespiration
					WHERE
					id_respiration = %s AND
					type = %s
					"""
			sql(query,(self.id_respiration , typ))
		
		if len(self.respiratory_cycle[0]) == 0:
			return
		
		shift, = where(self.respiratory_cycle[0] > -self.shift_t0*self.fs)
		shift = shift[0]
		
		
		for c in range(len(self.respiratory_cycle[0])) :
			for n in range(n_segment) :
				ep = EpochRespiration()
				d = {
						'id_trial' : self.id_trial,
						'id_respiration' : self.id_respiration,
						'type' : typ,
						'label' : 'Cycle '+str(c - shift)+' '+str((n+1))+'/'+str(n_segment) ,
						'num' : c - shift,
						'subnum' : n,
						'zero' : self.t()[self.respiratory_cycle[n][c]] ,
						'begin' :  self.t()[self.respiratory_cycle[n][c]] ,
						'end' : self.t()[self.respiratory_cycle[n+1][c]-1]
					}
				ep.new(  **d)
				ep.save_to_db()
				
	#------------------------------------------------------------------------------
	def load_respiratory_epoch(self, n_segment) :
		self.respiratory_cycle = [None]*(n_segment+1) 
		EpochRespiration().create_table()
		for s in range(n_segment) :
			query = """SELECT begin,end
				FROM epochrespiration
				WHERE
				type =  %s AND
				subnum = %s AND
				id_respiration = %s
				ORDER BY num
				"""
			values = ('repiration '+str(n_segment)+' segment' , s , self.id_respiration)
			beg , end = sql(query,values,Array = True , dtype = 'f')
			self.respiratory_cycle[s] = self.time_to_index(beg)
			self.respiratory_cycle[s+1] = self.time_to_index(end)
			
	#------------------------------------------------------------------------------
	def get_meancycle(self,n_segment ):
		EpochRespiration().create_table()
		query = """SELECT avg(end-begin)
			FROM epochrespiration
			WHERE
			type = 'repiration """+str(n_segment)+""" segment' 
			GROUP BY subnum
			ORDER BY subnum
			"""
		size_mean, = sql(query,Array = True , dtype = 'f')
		return size_mean
	
	
	#------------------------------------------------------------------------------
	def resize_signal_on_respiration(self,sig,fs,shift_t0,n_segment,axis=0) :
		self.load_respiratory_epoch(n_segment)
		n_segment = len(self.respiratory_cycle)-1
		
		
		size_mean = self.get_meancycle(n_segment)
		size_mean *=fs
		size_mean = size_mean.round()
		#~ print 'size_mean', size_mean
		t = arange(sig.shape[axis] , dtype='f')/fs+shift_t0
		new_time_basis = [ ]
		for c in range(len(self.respiratory_cycle[0])) :
			if  ( self.t()[self.respiratory_cycle[0][c]] > t[0] ) and ( self.t()[self.respiratory_cycle[-1][c]] < t[-1] ) :
				for s in range(n_segment) :
					start =self.t()[self.respiratory_cycle[s][c]] 
					stop = self.t()[self.respiratory_cycle[s+1][c]] 
					tn = arange(size_mean[s], dtype = 'f')/size_mean[s] * ( stop - start ) + start
					new_time_basis += tn.tolist()
		new_time_basis = array(new_time_basis)
		
		
		#~ print new_time_basis
		#~ print self.t()[self.respiratory_cycle[0][0]]
		#~ print self.t()[self.respiratory_cycle[-1][-1]]
		
		
		
		kind = 'linear'
		
		#~ print t[0] , t[-1]
		#~ print t.shape, sig.shape
		inter = interpolate.interp1d( t , sig , axis =axis , kind= kind)
		sig_r = inter(new_time_basis )
		
		return sig_r
		
		
		
	
	#------------------------------------------------------------------------------
	def resize_signal_on_respiration_old(self,sig,fs,shift_t0,n_segment,axis=0) :
		self.load_respiratory_epoch(n_segment)
		n_segment = len(self.respiratory_cycle)-1
		
		t_sig = arange(0,sig.shape[axis],1)/float(fs)+shift_t0
		
		size_mean = self.get_meancycle(n_segment)
		size_mean *=fs
		size_mean = size_mean.round()
		sh = list(sig.shape)
		sh[axis] = 0
		sig_r = empty(sh )
		factor = .0
		#~ print 'size_mean', size_mean, sum(size_mean)
		for c in range(len(self.respiratory_cycle[0])) :
			for s in range(n_segment) :
				# to avoid bord effect we take 10% mor than the segment left and right
				size_local = (self.respiratory_cycle[s+1][c]-self.respiratory_cycle[s][c])/float(self.fs)*float(fs)
				#~ size_local = floor(size_local)
				if size_local ==0 : continue

				n_start = max(0 ,argmin(abs(self.t()[self.respiratory_cycle[s][c]]-t_sig))- factor*size_local)
				n_stop = min(sig.shape[axis] , argmin(abs(self.t()[self.respiratory_cycle[s+1][c]]-t_sig))+factor*size_local)
				if n_stop-n_start <= 1 : n_stop +=1
				new_shape = list(sig.shape)
				
				new_shape[axis] = round(size_mean[s] *((n_stop-n_start)/size_local))# ??????????
				new_shape[axis] = ceil(size_mean[s] *((n_stop-n_start)/size_local))# ??????????
				
				
				sl = [ slice(None) for n in range(sig.ndim) ]
				sl[axis] = slice(n_start,n_stop)
				s1 = sig[sl].copy()
				sig_r_local = resample(s1 ,
					new_shape = new_shape,
					axis =axis )
				sl = [ slice(None) for n in range(sig.ndim) ]
				n_start2 = floor((argmin(abs(self.t()[self.respiratory_cycle[s][c]]-t_sig))-n_start)/(n_stop-n_start) * new_shape[axis])
				sl[axis] = slice(n_start2 ,  n_start2 + size_mean[s])
				
				
				
				if sig_r_local[sl].shape[axis] != size_mean[s] :
					print 'oulala'
					print '!=',sig_r_local[sl].shape[axis] , size_mean[s]
					print 'sl', sl
					print 's',s
					print  'sig.shape' , sig.shape
					print  'new_shape' , new_shape
					print 'size_local',size_local
					print 'n_start', n_start, 'n_stop', n_stop , 'n_stop - n_start', n_stop - n_start
					print 'sig_r_local.shape', sig_r_local.shape
					print 'sig_r_local[sl].shape', sig_r_local[sl].shape
					print 'n_start2',n_start2, n_start2 + size_mean[s]
					print 'size_mean[s]', size_mean[s]
					print 'c', c
					sh2 = list(sig.shape)
					sh2[axis] = size_mean[s]
					sig_r_local = zeros(sh2)
				else :
					sig_r_local = sig_r_local[sl]	
				
				sig_r = concatenate((sig_r,sig_r_local),axis=axis)
		
		return sig_r
		
		
	#------------------------------------------------------------------------------
	def event_time_to_cycle(self,event,n_segment) :
		
		"""
		event in s.
		"""
		n_segment = len(self.respiratory_cycle)-1
		real_time = []
		cycle = []
		size_mean = self.get_meancycle(n_segment)
		size_mean /= size_mean.sum()
		if len(self.respiratory_cycle[0]) ==0:
			return empty(len(event))*nan
		shift_c0 = where(self.respiratory_cycle[0] > -self.shift_t0*self.fs)[0][0]
		for c in range(len(self.respiratory_cycle[0])) :
			for s in range(n_segment) :
				real_time.append(self.respiratory_cycle[s][c]/self.fs + self.shift_t0)
				cycle.append(size_mean[:s].sum()+c-shift_c0)
		inter = interpolate.interp1d(real_time , cycle , bounds_error = 0 , kind = 'linear')
		
		#pylab.figure()
		#pylab.plot(real_time , array(cycle), 'o-')
		return inter(event)
		#return inter(event)*self.get_meancycle(n_segment).sum()

		
		"""
		n_segment = len(self.respiratory_cycle)-1
		
		size_mean = self.get_meancycle(n_segment)
		
		size_cycle = size_mean.sum()
		event_r = event[(event >=self.respiratory_cycle[0][0]/self.fs) & (event <self.respiratory_cycle[-1][-1]/self.fs) ] .copy()
		for c in range(len(self.respiratory_cycle[0])) :
			for s in range(n_segment) :
				l1 , l2 = self.respiratory_cycle[s][c]/self.fs   ,  self.respiratory_cycle[s+1][c]/self.fs
				ind = (event_r>=l1) & (event_r<l2)
				#TODO decalage
				event_r[ind] = size_cycle*c +size_mean[:s].sum() +  (event_r[ind]-l1)/(l2-l1)*size_mean[s]
		return event_r
		"""
	#------------------------------------------------------------------------------
	def event_cycle_to_time(self,event_r,n_segment) :
		"""
		event in cycle
		"""
		n_segment = len(self.respiratory_cycle)-1
		real_time = []
		cycle = []
		size_mean = self.get_meancycle(n_segment)
		size_mean /= size_mean.sum()
		shift_c0 = where(self.respiratory_cycle[0] > -self.shift_t0*self.fs)[0][0]
		for c in range(len(self.respiratory_cycle[0])) :
			for s in range(n_segment) :
				real_time.append(self.respiratory_cycle[s][c]/self.fs + self.shift_t0)
				cycle.append(size_mean[:s].sum()+c-shift_c0)
		inter = interpolate.interp1d(cycle , real_time ,   bounds_error = 0, kind = 'linear')
		return inter(event_r)
		
		
		"""
		size_mean = self.get_meancycle(n_segment)
		
		size_cycle = size_mean.sum()
		
		event = ones_like(event_r)*nan
		
		for c in range(len(self.respiratory_cycle[0])) :
			for s in range(n_segment) :
				lr1 = float(self.respiratory_cycle[s][c] - self.respiratory_cycle[0][c] ) /(self.respiratory_cycle[-1][c] - self.respiratory_cycle[0][c] )+c
				lr2 = float(self.respiratory_cycle[s+1][c] - self.respiratory_cycle[0][c] ) /(self.respiratory_cycle[-1][c] - self.respiratory_cycle[0][c] )+c
				print lr1,lr2
				
				l1 , l2 = self.respiratory_cycle[s][c]/self.fs   ,  self.respiratory_cycle[s+1][c]/self.fs
				print l1,l2
				ind = (event_r>=lr1) & (event_r<lr2)
				#TODO decalage
				event[ind] = (event_r[ind]-lr1)/(lr2-lr1)*(l2-l1) + l1
		"""
		return event
			
	
			
	#------------------------------------------------------------------------------
	def t(self) :
		"""
		convert index to time in s.
		"""
		return arange(0,self.signal.size,1)/self.fs+self.shift_t0
	
	#------------------------------------------------------------------------------
	def time_to_index(self , time) :
		"""
		convert time in s.to index
		"""
		time=((time-self.shift_t0)*self.fs).round().astype('i')
		time[time<0]=0
		time[time>=self.signal.size]=self.signal.size-1
		return time
		
	#------------------------------------------------------------------------------
	def t_resized(self , fs, limit1 = -inf, limit2 = inf) :
		n_segment = len(self.respiratory_cycle)-1
		size_mean = self.get_meancycle(n_segment)
		size_mean *=fs
		size_mean = size_mean.round()
		size_cycle = size_mean.sum()
		#print self.respiratory_cycle
		#print self.respiratory_cycle[0]
		#print -self.shift_t0*self.fs
		shift, = where(self.respiratory_cycle[0] > -self.shift_t0*self.fs)
		shift = shift[0]
		t_resized = array([] , dtype='f')
		#~ print 'size_cycle', size_cycle, 'nb_cycle', len(self.respiratory_cycle[0])
		for c in range(len(self.respiratory_cycle[0])) :
			if  ( self.t()[self.respiratory_cycle[0][c]] > limit1 ) and ( self.t()[self.respiratory_cycle[-1][c]] < limit2 ) :
				t_resized = r_[t_resized , arange(0,size_cycle)/size_cycle  + c - shift ]
		return t_resized

	#------------------------------------------------------------------------------
	list_type_plot = [ 'bandwidth' , 'cycle_in_time'  ]
	list_option_default = [
							[ ],
							[5]
						]
	list_option_param = [
							[ ],
							['n_segment'	]
						]
						
	#------------------------------------------------------------------------------
	def plot(self, ax = None , type_plot = 'bandwidth' , option = None ) :
		pos = self.list_type_plot.index(type_plot)
		if option == None :
			option = self.list_option_default[pos]
		param = dict(zip(self.list_option_param[pos] , option))
		list_func_plot = [ self.plot_bandwidth , self.plot_cycle_in_time ]
		list_func_plot[pos](ax=ax , **param )

	#------------------------------------------------------------------------------
	def plot_bandwidth(self, ax = None, color = None ,
				title = None, label = None ) :
		
		flag = False
		if ax is None :
			fig = pylab.figure()
			ax = fig.add_subplot(111)
			flag = True
			
		if 'nb_plot_respiration_sig' not in dir(ax) :
			ax.nb_plot_respiration_sig = 0
			ax.list_plot_respiration_sig = []
		
		ax.nb_plot_respiration_sig +=1
		if color is None :
			cmap = pylab.get_cmap('jet',ax.nb_plot_respiration_sig)
			list_color = []
			for l,line in enumerate(ax.list_plot_respiration_sig) :
				line.set_color(cmap(l))
			color = cmap(ax.nb_plot_respiration_sig)
			
		if title is None :
			title = u'Respiration'
		lines = ax.plot(self.t(),self.signal , color = color , label=label)
		ax.list_plot_respiration_sig.append(lines[0])
		ax.set_title(title)
		if flag :
			ax.set_xlim((self.t()[0],self.t()[-1]))
			
		return ax
		
	#------------------------------------------------------------------------------
	def plot_cycle_in_time(self,ax , n_segment = 5 , color = 'r' , limit = [-inf , inf] ) :
		#xl = ax.get_xlim()
		#yl = ax.get_ylim()
		self.load_respiratory_epoch(n_segment)
		for c in range(len(self.respiratory_cycle[0])) :
			for n in range(n_segment) :
				if n==0 : linestyle = '-'
				else : linestyle = '--'
				x = self.t()[self.respiratory_cycle[n][c]]
				if (x >= limit[0]) and (x <= limit[1]) :
					ax.axvline(x, linestyle=linestyle , color = color)
					#ax.plot([x,x] , list(yl) , linestyle=linestyle , color = color)
		#ax.set_xlim(xl)
		#ax.set_ylim(yl)
		
	#------------------------------------------------------------------------------
	def plot_cycle_in_cycle(self,ax , n_segment = 5 , color = 'r' , limit = [-inf , inf]) :
		#xl = ax.get_xlim()
		#yl = ax.get_ylim()
		self.load_respiratory_epoch(n_segment)
		#size_mean = self.get_meancycle(n_segment)
		#size_cycle = size_mean.sum()
		#shift, = where(self.respiratory_cycle[0] > -self.shift_t0*self.fs)
		#shift = shift[0]
		for c in range(len(self.respiratory_cycle[0])) :
			for n in range(n_segment) :
				if n==0 : linestyle = '-'
				else : linestyle = '--'
				#num = c - shift
				#fra = size_mean[:n].sum()/size_cycle
				#x = num + fra
				x = self.t()[self.respiratory_cycle[n][c]]
				x = self.event_time_to_cycle([x],n_segment)[0]
				if (x >= limit[0]) and (x <= limit[1]) :
					ax.axvline(x,linestyle = linestyle , color = color)
					#ax.plot([x,x] , list(yl) , mark , color = color)
		#ax.set_xlim(xl)
		#ax.set_ylim(yl)

#------------------------------------------------------------------------------
def regroup_csummer_by_cycle(csum , lim = [-inf,inf]) :
	csum2 = CSummer()
	csum2.axis = csum.axis
	csum2.yl = csum.yl
	n_point_cycle = int(csum.t.size)/int((ceil(csum.t[-1]) - csum.t[0]))
	begin = int(where(csum.t >= lim[0])[0][0])
	end = int(where(csum.t < lim[1])[0][-1]+1)
	csum2.n = zeros(n_point_cycle)
	csum2.t = arange(n_point_cycle,dtype='f')/n_point_cycle
	for i, b in enumerate(range(begin,end,n_point_cycle)) :
		csum2.n += csum.n[b:b+n_point_cycle]
		sl = [ slice(None) for n in range(csum.sum_sig.ndim) ]
		sl[csum2.axis] = slice(b,b+n_point_cycle)
		if i==0 :
			csum2.sum_sig = csum.sum_sig[sl]
			csum2.sum2_sig = csum.sum2_sig[sl]
		else :
			csum2.sum_sig += csum.sum_sig[sl]
			csum2.sum2_sig += csum.sum2_sig[sl]
	return csum2

#------------------------------------------------------------------------------
def regroup_dsummer_by_cycle(dsum ,cycle, lim = [-inf,inf]) :
	dsum2 = DSummer(dsum.size_win , n_win = cycle.size)
	n_point_cycle = int(cycle.size/(ceil(cycle[-1]) - cycle[0]))
	begin = int(where(cycle >= lim[0])[0][0])
	end = int(where(cycle < lim[1])[0][-1]+1)
	dsum2.n = sum(dsum.n[begin:end].reshape((n_point_cycle , ((end-begin)/n_point_cycle))),1)
	dsum2.sum_event = zeros((n_point_cycle),dtype='f')
	for i, b in enumerate(range(begin,end,n_point_cycle)) :
		dsum2.sum_event += dsum.sum_event[b:b+n_point_cycle]
	return dsum2


def test_resp() :
	
	
	#host, login , password = 'neuro009.univ-lyon1.fr' , 'multisite' , 'youplaboum'
	host, login , password = 'localhost' , 'multisite' , ''
	nom_base = 'NBuonviso200606_test_sam_1'
	init_connection(host, login, password,nom_base=nom_base)

	#sql('DROP TABLE epoch')
	#sql("DROP TABLE respiration")
	
	rep = r'./'
	fichiers= [r'tem16a04.IOT' ]
	#fichiers= [r'tem19a07.IOT']
	#fichiers= [r'Tem23a01.IOT']
	import os
	#rep = r'./fichier test/'
	#rep = r'./fichier test2/'
	#fichiers = os.listdir(rep)

	for fichier in fichiers :
		print fichier
		id_trial = 100
		resp = Respiration(id_trial = id_trial)
		resp.load_from_file_raw(rep+fichier, 10000.,'int16', num_channel = 15, num_total = 16 , shift_t0 = -5. )
		
		
		#sql("DROP TABLE respiration")
		#resp.save_to_db()
	
		#resp.load_from_db(1)
		
		#coul = ['or' , 'og' , 'om' , 'ob' , 'ok' , '^r']
		coul = ['or' , 'og' , 'om' , 'ob' , 'ok' , 'or']
		
		import pylab
		n_segment = 5
		resp.compute_segmentation(n_segment = n_segment)
		resp.save_respiratory_epoch()
		resp.load_respiratory_epoch(n_segment)
		#ax = resp.plot()
		#ax = resp.plot_cycle_limit_resized(ax , n_segment = n_segment)
		#pylab.show()
		
		sig_r = resp.resize_signal_on_respiration(resp.signal,resp.fs ,resp.shift_t0,  n_segment)
		
		event = rand(10)*15-5
		event_r = resp.event_time_to_cycle(event, n_segment)
		event2 = resp.event_cycle_to_time(event_r , n_segment)
		print event
		print event_r
		print event2
		
		
		
		#pylab.figure()
		#pylab.plot(resp.t(),resp.signal)
		#pylab.hold('on')
		ax=resp.plot()
		#pylab.show()
		
		resp.plot_cycle_limit(ax , n_segment = n_segment)
		
		#for c , point in enumerate(resp.respiratory_cycle) :
		#	pylab.plot(resp.t()[point] , resp.signal[point] , coul[c])
		
		#pylab.plot(event,zeros(event.shape),'^k')
		
		fig = pylab.figure()
		ax = fig.add_subplot(1,1,1)
		#t=arange(0,sig_r.size,1)/resp.fs
		t = resp.t_resized( resp.fs)
		
		pylab.plot(t,sig_r)
		#pylab.plot(event_r[-isnan(event_r)],zeros(event_r[-isnan(event_r)].shape),'^k')
		ax = resp.plot_cycle_limit_resized(ax , n_segment = n_segment)
		
		
		
		elec = Electrode()
		elec.load_from_file_raw(rep+fichier , 10000.,'int16', num_channel = 5, num_total = 16 , shift_t0 = -5.)
		
		f_start,f_stop = 5.,100.
		df = 1./3

		tf = TimeFreq(elec , f_start,f_stop, df_sub = 1/3. ,df =1/3.  , f0=1.5, normalisation = 0.)
		
		
		pylab.matshow(abs(tf.tf_map().transpose()), extent = (elec.t()[0] , elec.t()[-1] , f_start , f_stop) )
		pylab.xlim( elec.t()[0] , elec.t()[-1])
		pylab.ylim(f_start , f_stop)
		resp.plot_cycle_limit(pylab.gca() , n_segment = n_segment)

		t1 = time.time()
		tf_r = resp.resize_signal_on_respiration(abs(tf.tf_map()),200. ,-5., 5 , axis=0)
		t2 = time.time()
		print 'respfreq in',t2-t1

		xl = resp.t_resized(200.)[0] , resp.t_resized(200.)[-1]
		pylab.matshow(tf_r.transpose(), extent =  xl+(f_start , f_stop) )
		pylab.xlim(xl)
		pylab.ylim(f_start , f_stop)
		resp.plot_cycle_limit_resized(pylab.gca() , n_segment = n_segment)
		
		
		pylab.show()

		