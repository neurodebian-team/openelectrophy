# -*- coding: utf8 -*-

"""
all classes for managing extra data ex : repsiration, position ...

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



import os

from respiration.respiration import *
from auxilary import *
from licking import *

dict_optional_data_class = { 
							'respiration' : Respiration ,
							'epochrespiration' : EpochRespiration ,
							'auxilary' : Auxilary ,
							'licktrain' : LickTrain ,
							'lick' : Lick
							}

list_optional_data_name = dict_optional_data_class.keys()
list_optional_data_class = dict_optional_data_class.values()

