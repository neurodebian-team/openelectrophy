# -*- coding: utf-8 -*-

"""
all importante classes in OpenElectrophy with associated option

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import pylab

from core_classes import *


# core classes of openElectrophy



dict_default_browsable = {
						'serie' : True,
						'trial' : True ,
						'electrode' : True ,
						'cell' : False,
						'spiketrain' : True,
						'spike' : False,
						'epoch' : True ,
						'oscillation' : True
						}

dict_default_shown_field = {
							'serie'  : [ 'info' ,'thedatetime' ] ,
							'trial' : [ 'info' ,'thedatetime' ] ,
							'electrode' : ['name' , 'num_channel' ,'label' ] ,
							'cell' : [ 'name' , 'info'   ],
							'spiketrain' : [ 'label' , 'coment' ] ,
							'spike' : [],
							'epoch' : [ 'label' , 'type', 'num' ],
							'oscillation': ['time_max','freq_max' ] ,
							}

# Adding optional data
#from optional_data.option_classes import dict_optional_data_class
from optional_data.option_classes import *
dict_hierarchic_class.update(dict_optional_data_class)

from dialogs import *
dict_context_menu = {
						'serie' : { 'create new cell' : CreateNewCell,
						 			'Edit spike on serie': OpenWindowEditSpikeSerie,
						 } ,
						#'trial' : { 'Move to new serie'  : MoveToNewSerie } ,
						'trial' : { 'Move to serie'  : ShowMenuMoveToSerie } ,
						'electrode' : {'Edit spike' : OpenWindowEditSpike ,
									 'Edit oscillation' : OpenWindowEditOscillation,
									 'Edit oscillation around epoch' : OpenWindowEditOscillationByEpoch,
									 } ,
						'cell' : { } ,
						'spiketrain' : { 'Move to cell' : ShowMenuMoveToCell } ,
						'spike' : { },
						'epoch' : { },
						'oscillation' : { }
					}




from optional_data.option_classes_gui import *
dict_default_browsable.update(dict_optional_data_browsable)
dict_default_shown_field.update(dict_optional_data_shown_field)
dict_context_menu.update(dict_optional_data_context_menu)


