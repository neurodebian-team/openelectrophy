# -*- coding: utf-8 -*-
#
# some function util for all modules
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 07.2006
#


from scipy import *
from scipy.fftpack import fft , ifft 
import time

#------------------------------------------------------------------------------
def fft_filter(sig,
		f_low =0,f_hight=1 ) :
	"""
	pass band filter using fft for real 1D signal
	"""
	n = sig.size
	N = 2**(ceil(log(n)/log(2)))
	SIG = fft(sig,N)
	
	n_low = floor((N-1)*f_low/2)+1;
	fract_low = 1-((N-1)*f_low/2-floor((N-1)*f_low/2));
	n_hight = floor((N-1)*f_hight/2)+1;
	fract_hight = 1-((N-1)*f_hight/2-floor((N-1)*f_hight/2));
	
	if f_low >0 :
		SIG[0] = 0
		
		SIG[1:n_low ] = 0
		SIG[n_low] *= fract_low
		
		SIG[-n_low] *= fract_low
		if n_low !=1 :
			SIG[-n_low+1:] = 0
	
	if f_hight <1 :
		SIG[n_hight] *= fract_hight
		SIG[n_hight+1:-n_hight] = 0	
		SIG[-n_hight] *= fract_hight
	
	return real(ifft(SIG)[0:n])
	

#------------------------------------------------------------------------------
def fft_passband_filter(sig,
		f_low =0,f_hight=1 ) :
	return fft_filter(sig, f_low =f_low, f_hight=f_hight )




#------------------------------------------------------------------------------
def fft_bandstop_filter(sig,f_low =0,f_hight=1 ) :
	"""
	band stop filter using fft for real 1D signal
	"""
	n = sig.size
	N = 2**(ceil(log(n)/log(2)))
	SIG = fft(sig,N)
	
	n_low = floor((N-1)*f_low/2)+1;
	fract_low = ((N-1)*f_low/2-floor((N-1)*f_low/2));
	n_hight = floor((N-1)*f_hight/2)+1;
	fract_hight = ((N-1)*f_hight/2-floor((N-1)*f_hight/2));
	
	if (f_low >0) and (f_hight <1) :
		SIG[n_low+1:n_hight-1] = 0
		SIG[n_low] *= fract_low
		SIG[n_hight] *= fract_hight
		
		SIG[-n_hight+1:-n_low-1] = 0
		SIG[-n_hight] *= fract_hight
		SIG[-n_low] *= fract_low
	
	return real(ifft(SIG)[0:n])



#------------------------------------------------------------------------------
def resample(sig,new_shape = None ,axis = 0,old_fs = None, new_fs = None) :
	return resample_fft(sig,new_shape = new_shape ,axis = axis,old_fs = old_fs, new_fs = new_fs)
	
#------------------------------------------------------------------------------
def resample_interpol(sig,new_shape = None ,axis = 0,old_fs = None, new_fs = None) :
	if new_shape == None :
		new_shape  = list(sig.shape)
		new_shape[axis] = int(round(sig.shape[axis]*new_fs/old_fs))
	
	
	#kind = 'nearest', 'linear', 'cubic', 'spline'
	kind ='linear'

	"""
	inter = interpolate.interp1d(arange(sig.shape[axis],dtype='f')/(sig.shape[axis]-1),sig , axis =axis , kind= kind)
	sig_r = inter(arange(new_shape[axis],dtype='f')/(new_shape[axis]-1) )
	return sig_r
	"""
	
	#t1 = time.time()
	sig_r = zeros(new_shape, dtype = sig.dtype)
	for n in range(sig.ndim) :
		if n ==axis : continue
		for i in range(sig.shape[n]) :
			sl = [ slice(None) for nd in range(sig.ndim) ]
			#sl[n] = slice(i,i+1)
			sl[n] = i
			inter = interpolate.interp1d(arange(sig[sl].size,dtype='f')/(sig[sl].size-1),sig[sl].squeeze() , kind = kind)
			sig_r[sl] = inter(arange(sig_r[sl].size,dtype='f')/(sig_r[sl].size-1))
	#t2 = time.time()
	#print 'linear' , sig.shape , new_shape , t2-t1
	
	return sig_r
	
#------------------------------------------------------------------------------
def resample_fft(sig,new_shape = None ,axis = 0,old_fs = None, new_fs = None) :
	if new_shape == None :
		new_shape  = list(sig.shape)
		new_shape[axis] = int(round(sig.shape[axis]*new_fs/old_fs))
	
	SIG = fft(sig,axis = axis)
	SIG_R = zeros(new_shape, dtype = SIG.dtype)
	if sig.shape[axis] <= new_shape[axis] :
		#over sampling : add zeros
		hs = sig.shape[axis]/2
	else :
		#sub sampling :
		hs = new_shape[axis]/2
	sl = [ slice(None) for n in range(sig.ndim) ]
	sl[axis] = slice(None,hs+1) #+1 for the continue value
	SIG_R[sl]=SIG[sl]
	sl = [ slice(None) for n in range(sig.ndim) ]
	if hs!=0:
		sl[axis] = slice(-hs,None)
		SIG_R[sl] = SIG[sl]
	sig_r = real(ifft(SIG_R,axis=axis))*new_shape[axis]/sig.shape[axis]
	return sig_r
		


#------------------------------------------------------------------------------
def local_max_2D(x , abs_tresh = None, std_tresh = None,
		min_dx = None,min_dy = None  ) :
	
	x_small, x_north,x_south,x_east , x_west  = x[1:-1 ,1:-1 ],x[:-2,1:-1] , x[2:,1:-1] , x[1:-1,2:] , x[1:-1,:-2]
	indx , indy = where( (x_small>=x_north) & (x_small>=x_south) & (x_small>=x_east)  & (x_small>=x_west) )
	indx , indy = indx +1 , indy +1
	if abs_tresh == None and std_tresh is not None :
		abs_tresh = abs(x).mean()+std_tresh*abs(x).std()
	if abs_tresh is not None :
		ind, = where(abs(x)[indx,indy]>abs_tresh)
		indx,indy = indx[ind],indy[ind]
	if min_dx is not None and min_dy is not None :
		INDX = abs(repeat([indx],indx.size,0)-repeat([indx],indx.size,0).transpose())
		INDY = abs(repeat([indy],indy.size,0)-repeat([indy],indy.size,0).transpose())
		to_eliminate = array([])
		N1,N2 = where( (INDX<min_dx) & (INDY<min_dy) )
		for i in range(N1.size) :
			n1,n2 = N1[i],N2[i]
			if n1==n2 : continue
			if x[indx[n1],indy[n1]] > x[indx[n2],indy[n2]] :
				to_eliminate = r_[to_eliminate , n2]
			else :
				to_eliminate = r_[to_eliminate , n1]
		to_eliminate = sort(unique(to_eliminate))
		for n in to_eliminate :
			indx = r_[indx[:n] , indx[n+1:]]
			indy = r_[indy[:n] , indy[n+1:]]
			to_eliminate -=1
			
	return indx,indy


#------------------------------------------------------------------------------
def combinate_all_pair(x1 , x2 , once = True) :
	m1 = tile(arange(x1.size , dtype='i')[:,newaxis] , (1,x2.size) )
	m2 = tile(arange(x2.size , dtype='i')[newaxis,:] , (x1.size,1) )
	if once :
		ind = m1>m2
		m1 = m1[ind].reshape((ind[ind].size))
		m2 = m2[ind].reshape((ind[ind].size))
	else :
		m1.reshape((m1.size))
		m2.reshape((m2.size))
	return x1[m1] , x2[m2]
	
	
