# -*- coding: utf-8 -*-
#
# module for detecting burst
#
# samuel Garcia
# Laboratoire Neurosciensptr et Systemes sensoriels
# 07.2006
#

from scipy import *


#------------------------------------------------------------------------------
def sliding_burst_detection(time_spike, func_method , win_size =120. , step = 60.  ) :
	"""
	function for  burst detection with a sliding window
	"""
	centers_win = arange(time_spike[0]+win_size/2. , time_spike[-1], step)
	burst_start  = array([])
	burst_len = array([])
	for center in centers_win :
		print center
		ind_win, = where((time_spike >= center - win_size/2.) & (time_spike <= center + win_size/2.))
		if ind_win.size==0 : continue
		ts = time_spike[ ind_win ].copy()
		b_start , b_len = func_method(ts)
		b_start += ind_win[0]
		burst_start = r_[burst_start , b_start]
		burst_len = r_[burst_len , b_len]
		
		
	for i in xrange(len(burst_start)) :
		if burst_start[i] == -1 : continue
		#searching overlaping
		ind, = where( ((burst_start<= burst_start[i]) & (burst_start[i] <burst_start+burst_len)) | 
					((burst_start< burst_start[i]+burst_len[i]) & (burst_start[i]+burst_len[i]  < burst_start+burst_len )) )
		#print ind
		#we keep merge the 2
		for j in ind :
			if j==i : continue
			#print j,i
			#print burst_start[j],burst_start[i]
			#print burst_len[j],burst_len[i]
			if burst_start[j]<burst_start[i] :
				burst_start[i] = burst_start[j]
			burst_end = max(burst_start[j]+burst_len[j] , burst_start[i]+burst_len[i])
			burst_len[i] = burst_end - burst_start[i]
			#print 'du coup' ,burst_start[i] , burst_len[i]
			
			burst_len[j] = 0
			burst_start[j] = -1
	
	
	ind_ok, = where((burst_start != -1) & (burst_len!=1)  )
	burst_start = burst_start[ind_ok]
	burst_len = burst_len[ind_ok]
	
	return burst_start , burst_len

#------------------------------------------------------------------------------
# List of method
#------------------------------------------------------------------------------
def detection_with_hist(time_spike , factor = 2) :
	"""
	Very simple method for burst detection : 
		find the max in histogram of distribution and 
		limit = max * factor
	"""
	list_burst = [[]]
	evolution_limit = [nan]
	fen = arange(0,5.,0.004)
	ts = time_spike
	
	ts.sort()
	ds = diff( ts )
		
	#lim1
	#moy = mean(ds)
	#sd = std(ds)
	#lim = moy+sd
	
	#lim2
	#y = stats.histogram2(ds , fen)
	#ind = argmax(y[:-1])
	#lim = fen[:-1][ind] * 2.
	
	#lim3
	#y = stats.histogram2(ds , fen)
	#ind = argmax(y[:-1])
	#lim = fen[:-1][ind] + std(ds)
	
	#lim4
	y = stats.histogram2(ds , fen)
	ind = argmax(y[:-1])
	lim = fen[:-1][ind]*factor
	
	print 'lim' , lim
	
	print 'hist max' , fen[:-1][ind] , 'moy' , mean(ds) , 'sd' , std(ds)
	
	#fig = pylab.figure()
	#ax = fig.add_subplot(1,1,1)
	#ax.plot(fen , y)
	#a, un1 , un2, = stats.gamma.fit(ds)
	#gam = stats.gamma(a)
	#y2 = gam.pdf(fen)
	#ax.plot(fen+0.002 , y2 , 'r')
	#print 'a' , a
	
	ind, = where(ds>lim)
	ind = ind
	
	burst_start = r_[0 , ind+1]
	burst_len = r_[ ind+1 , time_spike.size-1] - burst_start
	
	return burst_start , burst_len 


burst_methods = { 'detection_with_hist' : detection_with_hist }

#------------------------------------------------------------------------------
# Some function util for extract spike in burst
#------------------------------------------------------------------------------
def which_bursting(time_spike , burst_start , burst_len   ) :
	"""
	Return index of spike in burst
	"""
	bursting = array([] , dtype = 'i')
	for b in xrange(burst_start.size) :
		bursting = r_[ bursting , arange(burst_start[b] , burst_start[b] + burst_len[b] , dtype = 'i')]
	return bursting
#------------------------------------------------------------------------------
def which_bursting_without_last(time_spike , burst_start , burst_len   ) :
	"""
	Return index of spike in burst exept the last one
	"""
	bursting = array([] , dtype = 'i')
	for b in xrange(burst_start.size) :
		bursting = r_[ bursting , arange(burst_start[b] , burst_start[b] + burst_len[b]-1, dtype = 'i')]
	return bursting
#------------------------------------------------------------------------------
def which_last_of_burst(time_spike , burst_start , burst_len   ) :
	"""
	Return index of all last spiek of burst
	"""
	bursting = array([] , dtype = 'i')
	for b in xrange(burst_start.size) :
		bursting = r_[ bursting , burst_start[b] + burst_len[b]-1 ]
	return bursting

