# -*- coding: utf-8 -*-

"""
methods for spike detection

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



from scipy import *

from pyssdh.OpenElectrophy.computing.util import *
from pyssdh.OpenElectrophy.core_classes.spike import *

class StdThreshold :
	list_param = 	['sign',	'std_thresh',		'refractory_period'					]
	default_param =	[ '+-',			3.,					0.004								]
	list_label = 	[ 'Sign (+/-/+-)', 		'std tresh',	'Refractory period (s.)'	]

	name = 'Std threshold detection'
	
	def compute(self , sig_f , fs  , 
				sign=None, std_thresh = None, refractory_period = None ) :
		if sign == '-' :
			sig_f2 = -sig_f
		elif sign =='+' :
			sig_f2 = sig_f
		elif sign == '+-' or  sign == '-+' :
			sig_f2 = abs(sig_f)
		
		abs_thresh = abs(sig_f2).mean() + std_thresh*abs(sig_f2).std()
		pos_spike = where(	( sig_f2[1:-1] >=abs_thresh ) & 
							(sig_f2[1:-1] >sig_f2[:-2]) & (sig_f2[1:-1] >=sig_f2[2:] )
						)[0] +1
		pos_spike = eliminate_refractory_period(pos_spike,sig_f ,fs,refractory_period)
		return pos_spike





class MedianThreshold :
	list_param = 	['sign',	'median_thresh',		'refractory_period'					]
	default_param =	[ '+-',			3.,					0.004								]
	list_label = 	[ 'Sign (+/-/+-)', 		'median tresh',	'Refractory period (s.)'	]

	name = 'Median threshold detection'
	
	def compute(self , sig_f , fs  , 
				sign=None, median_thresh = None, refractory_period = None ) :
		if sign == '-' :
			sig_f2 = -sig_f
		elif sign =='+' :
			sig_f2 = sig_f
		elif sign == '+-' or  sign == '-+' :
			sig_f2 = abs(sig_f)
		
		abs_thresh = median(abs(sig_f)/.6745)
		pos_spike = where(	( sig_f2[1:-1] >=abs_thresh ) & 
							(sig_f2[1:-1] >sig_f2[:-2]) & (sig_f2[1:-1] >=sig_f2[2:] )
						)[0] +1
		pos_spike = eliminate_refractory_period(pos_spike,sig_f ,fs,refractory_period)
		return pos_spike





list_method = [ StdThreshold,  MedianThreshold ]

