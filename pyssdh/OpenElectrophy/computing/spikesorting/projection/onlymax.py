# -*- coding: utf-8 -*-

"""
projection method for spike sorting : kepp only max

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

class OnlyMax:
	list_param = 	[ 	]
	default_param =	[ 	]
	list_label = 	[ 	]

	name = 'Only Max'

	def compute( self , waveform , fs ) :
		i = int(waveform.shape[1]/2)
		return waveform[:,i:i+1]

