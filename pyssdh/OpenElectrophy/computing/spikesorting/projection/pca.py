# -*- coding: utf-8 -*-

"""
projection method for spike sorting : pca

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import mdp

class PCA:
	list_param = 	[ 'n'			]
	default_param =	[ 2				]
	list_label = 	[ 'n component']

	name = 'Principal Component Analysis'

	def compute( self , waveform , fs , n =2) :
		
		pca_mat = mdp.pca(waveform , 
						output_dim = n)
		return pca_mat
		
