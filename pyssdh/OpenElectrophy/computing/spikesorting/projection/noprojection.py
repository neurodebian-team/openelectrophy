# -*- coding: utf-8 -*-

"""
projection method for spike sorting : no projetcion

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

class NoProjection:
	list_param = 	[ 'left_win',	'right_win'	]
	default_param =	[ -.001,		.001		]
	list_label = 	[ 'Left window', 		'Right window'	]

	name = 'No projection'

	def compute( self , waveform , fs , left_win=None , right_win = None) :
		i = int(waveform.shape[1]/2)
		print i
		print i-int(fs*left_win) , i+int(fs*right_win)
		return waveform[:,i+int(fs*left_win):i+int(fs*right_win)]

