# -*- coding: utf-8 -*-

"""
projection method for spike sorting : ica

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

import mdp

class ICA:
	list_param = 	[ 'n'			]
	default_param =	[ 2				]
	list_label = 	[ 'n component']

	name = 'Independant Component Analysis'

	def compute( self , waveform , fs , n =2) :
		
		ica_mat = mdp.fastica(waveform , whitened = 0,
						white_comp = n,
						g = 'tanh' ,
						approach= 'symm'  )
		return ica_mat
		
