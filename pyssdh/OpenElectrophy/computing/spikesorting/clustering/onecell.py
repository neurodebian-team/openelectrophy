# -*- coding: utf-8 -*-

"""
clusering method for spike sorting : one cell

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *
from superparamagneticclustering import superparamagneticclustering

class OneCell :
	list_param =	[]
	default_param =	[]
	list_label = 	[]

	name = 'One cell (no clustering)'
	
	def compute(self , waveform,  **karg) :
		cluster = zeros(waveform.shape[0], dtype = 'i')
		return cluster
	
