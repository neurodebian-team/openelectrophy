# -*- coding: utf-8 -*-

"""
clusering method for spike sorting : superparamagneticclustering

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *
from superparamagneticclustering import superparamagneticclustering

class SC :
	list_param =	['Tmin',			'Tmax',		'deltaT',		'schwelle',		'regroup_small'				]
	default_param =	[ 0.,				0.03,			0.01,			0.3	,		5							]
	list_label = 	['Temp min',		'Temp max',		'Temp step',	'schwelle',	'regroup small cluster'		]

	name = 'SuperParamagneticClustering'
	
	def compute(self , waveform,  **karg) :
		allcluster = superparamagneticclustering(waveform , **karg )
		
		regroup_small = karg['regroup_small']
		
		cluster = allcluster[:,int(allcluster.shape[1]/2)]
		
		u = unique(cluster)
		nb, c = histogram(cluster, u)
		for i in u[ nb<karg['regroup_small'] ]:
			cluster[cluster == i] = u.size
		
		return cluster
	
