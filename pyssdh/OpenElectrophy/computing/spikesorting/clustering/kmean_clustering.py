# -*- coding: utf-8 -*-

"""
clusering method for spike sorting : k-means

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *

from scipy import cluster

class KMean :
	list_param =	['n',			]
	default_param =	[ 2	]
	list_label = 	[ 'N cluster'	]

	name = 'K-means'
	
	def compute(self , waveform , n=2 ) :
		codebook , distor =  cluster.vq.kmeans(waveform , n)
		code, distor = cluster.vq.vq(waveform , codebook)
		return code

