# -*- coding: utf-8 -*-

"""
methods for filtering signal for spike detection

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


from scipy import *
from scipy import signal


from pyssdh.OpenElectrophy.computing.util import *

class MethodFFT :
	list_param =	['f_low',			'f_hight',			]
	default_param =	[300.,				inf,				]
	list_label = 	['Low Freq Cut',	'Hight Freq Cut'	]

	name = 'Filtering with FFT'
	
	def compute(self , sig , fs , f_low = None , f_hight = None ) :
		sig_f = fft_filter(sig, f_low = f_low/(fs/2.) , f_hight = f_hight/(fs/2.) )
		return sig_f
	
	

class MedianFilter :
	list_param =	[ 'win_size',			]
	default_param =	[ .05,					]
	list_label = 	['Windows size (s.)'	]

	name = 'Soustraction of Median Filter'
	

	def compute(self , sig , fs , win_size = None ) :
		sig_f = sig - signal.medfilt(sig , kernel_size = int(win_size/2.*fs)*2+1)
		return sig_f
	
	
class NoFiltering :
	list_param =	[ 	]
	default_param =	[ 	]
	list_label = 	[	]

	name = 'No filtering'

	def compute(self , sig , fs  ) :
		return sig.copy()




class ButterworthFiltering :
	list_param =	['f_low',			'f_hight',			'N',		]
	default_param =	[300.,				inf,				5,			]
	list_label = 	['Low Freq Cut',	'Hight Freq Cut',	'Order N',	]

	name = 'N order Butterworth Filter'
	
	def compute(self , sig , fs , f_low = None , f_hight = None, N = 5 ) :
		if isinf(f_hight):
			f_hight = .95*fs/2.
		#Wn = [f_low/(fs/2.) , f_hight/(fs/2.)]
		#b,a = signal.iirfilter(N, Wn, btype = 'band', analog = 0, ftype = 'butter', output = 'ba')
		
		Wn = f_low/(fs/2.)
		b,a = signal.iirfilter(N, Wn, btype = 'high', analog = 0, ftype = 'butter', output = 'ba')
		
		sig_f = signal.lfilter(b, a, sig, zi = None)
		return sig_f


class BesselFiltering :
	list_param =	['f_low',			'f_hight',			'N',		]
	default_param =	[300.,				inf,				5,			]
	list_label = 	['Low Freq Cut',	'Hight Freq Cut',	'Order N',	]

	name = 'N order Bessel Filter'
	
	def compute(self , sig , fs , f_low = None , f_hight = None, N = 5 ) :
		if isinf(f_hight):
			f_hight = .95*fs/2.
		#Wn = [f_low/(fs/2.) , f_hight/(fs/2.)]
		#b,a = signal.iirfilter(N, Wn, btype = 'band', analog = 0, ftype = 'bessel', output = 'ba')
		
		Wn = f_low/(fs/2.)
		b,a = signal.iirfilter(N, Wn, btype = 'high', analog = 0, ftype = 'bessel', output = 'ba')
		
		
		sig_f = signal.lfilter(b, a, sig, zi = None)
		return sig_f



list_method = [ MethodFFT  , MedianFilter, NoFiltering, ButterworthFiltering, BesselFiltering ]

