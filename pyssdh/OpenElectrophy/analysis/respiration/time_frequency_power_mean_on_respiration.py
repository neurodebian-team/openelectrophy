# -*- coding: utf-8 -*-
#
# analysis spike rate
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.core.ndsummer import *
from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.optional_data.option_classes import *

analysis_name = 'Time Frequency Power Mean on respiration'

list_table_query = 	[ 'electrode' 			]
list_table_title = 	[ 'List of electrode'	]


list_param = 		['n_segment',		'f_start'	,	'f_stop',		'df_sub' ,			'f0',		'normalisation'	]
default_param  =	[5,				5.			,	100.,		.5 ,					2.5,		0.			]
list_label =		['N segment',	'Freq start' ,	'Freq stop' , 	'Freq precision' , 	'f0',		'Normalisation'	]


n_fig = 1

help = """


"""


list_stim = [ 'pre-stim' , 'stim' , 'post-stim' ]

def compute(list_query_table ,  **karg ) :
	
	n_segment = karg['n_segment']
	
	query_electrode = list_query_table[0]
	
	list_id_electrode, = sql(query_electrode)
	
	
	
	list_csum = [ CSummer(axis=0 , yl = (karg['f_start'],karg['f_stop'])) for i in range(3) ]
	
	for id_electrode in list_id_electrode :
		print id_electrode
		
		elec = Electrode()
		elec.load_from_db(id_electrode)
		print elec.id_trial
		
		id_respiration, = sql1('SELECT id_respiration FROM respiration WHERE id_trial = %s' , elec.id_trial)
		resp = Respiration()
		resp.load_from_db(id_respiration)
		resp.load_respiratory_epoch(karg['n_segment'])
		
		
		print 'calculating timefreq map for ', elec.name , ':',
		import time
		t1 = time.time()
		tfreq = TimeFreq(elec , karg['f_start'],karg['f_stop'],
								df_sub = karg['df_sub'] , df =karg['df_sub']  ,
								f0 = karg['f0'] , normalisation = karg['normalisation'] )
		tfreq.tf_map()
		t2 = time.time()
		print t2-t1
		
		print 'calculating respfreq map for ', elec.name , ':',
		t1 = time.time()
		tfreq_r = resp.resize_signal_on_respiration(abs(tfreq.tf_map())**2  ,karg['f_stop']*2,elec.shift_t0,n_segment,axis=0)
		t2 = time.time()
		print t2-t1
		
		
		for i,stim in enumerate(list_stim) :
			query ="""
					SELECT begin, end
					FROM epoch
					WHERE
					id_trial = %s AND
					label = %s
					"""
			begin,end = sql1(query, (elec.id_trial , stim))
			
			print begin,end
			
			
			query ="""
					SELECT MIN(epochrespiration.num)
					FROM epoch , epochrespiration
					WHERE
					epoch.id_trial = epochrespiration.id_trial
					AND epoch.id_trial = %s 
					AND epoch.label = %s
					AND epoch.begin < epochrespiration.begin
					AND epochrespiration.subnum = 0
					"""
			cycle_begin, = sql1(query, (elec.id_trial , stim))
			
			query ="""
					SELECT MAX(epochrespiration.num)+1
					FROM epoch , epochrespiration
					WHERE
					epoch.id_trial = epochrespiration.id_trial
					AND epoch.id_trial = %s 
					AND epoch.label = %s
					AND epoch.end > epochrespiration.end
					AND epochrespiration.subnum = %s
					"""
			cycle_end, = sql1(query, (elec.id_trial , stim, n_segment-1))
			
			
			
			#a revoir
			cycle = resp.t_resized(fs=karg['f_stop']*2)
			#cycle_begin = ceil( cycle[cycle>begin][0] )
			#cycle_end = ceil( cycle[cycle<end][-1] )
			print cycle_begin , cycle_end
			print cycle
			
			ind, = where( (cycle>=cycle_begin) & (cycle<cycle_end+1) )
			print ind
			cycle = cycle[ ind ]
			print cycle
			list_csum[i].add(tfreq_r[ind,:] , cycle)
		
	result = { 
			'list_csum' : list_csum ,
			'resp' : resp ,
			'n_segment' : n_segment
			}
	return result


def plot( list_csum =None , n_segment = 5, resp= None, list_fig = None ) :
	import pylab
	if list_fig is None :
		fig = pylab.figure()
	else :
		fig = list_fig[0]
	
	
	lim = empty(0)
	list_ax = []
	list_im = []
	for i in range(3) :
		ax = fig.add_subplot(1,3,i+1)
		list_ax.append(ax)
		regroup_csummer_by_cycle(list_csum[i]).plot(ax_moy = ax , plot_std = False)
		im = ax.get_images()[0]
		list_im.append(im)
		lim = r_[lim , im.get_clim() ]
		resp.plot_cycle_in_cycle(ax , n_segment = n_segment ,  limit = [0,1])
		ax.set_xlim([0,1])
		ax.set_title(list_stim[i])
	for im in list_im :
		im.set_clim(0,amax(lim))
			
	#ax = fig.add_axes([0.05 , 0.97 , .9 , 0.02 ] )
	ax = fig.add_axes([0.05 , 0.03 , .9 , 0.02 ] )
	fig.colorbar(list_im[0] ,ax ,orientation='horizontal')
	
