# -*- coding: utf-8 -*-
#
# analysis spike rate
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.core.ndsummer import *
from pyssdh.OpenElectrophy.my_classes import *
from pyssdh.OpenElectrophy.optional_data.option_classes import *

analysis_name = 'Instantaneous sliding spike rate on respiration'

list_table_query = 	[ 'spiketrain' 		]
list_table_title = 	[ 'List of spiketrain'	]


list_param = 		['cycle_start',		'cycle_stop',		'n_point_cycle',		'n_segment',		'size_win'					]
default_param  =	[-10,				20,				100,				5,				.02						]
list_label = 			['Cycle start (s)',		'Cycle stop',		'Nbin/cycle',			'N segment',		'Sliding Window size (s)'		]

n_fig = 1

help = """


"""



def compute(list_query_table , cycle_start = -10 , cycle_stop = 20 , n_point_cycle = 100,
					n_segment = 5 , size_win = .02) :
	
	query_spiketrain = list_query_table[0]
	
	list_id_spiketrain, = sql(query_spiketrain)
	
	cycle = arange(cycle_start,cycle_stop,1./n_point_cycle)
	dsum =  DSummer(size_win , n_win = cycle.size)
	
	for id_spiketrain  in list_id_spiketrain :
		print id_spiketrain
		
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)

		event = sptr.time_spike()
	
		id_respiration, = sql1('SELECT id_respiration FROM respiration WHERE id_trial = %s' , sptr.id_trial)
		resp = Respiration()
		resp.load_from_db(id_respiration)
		resp.load_respiratory_epoch(n_segment)
		center_win_r = resp.event_cycle_to_time(cycle , n_segment)
		dsum.add(event,  center_win=center_win_r)
	
	result = { 
			'n' : dsum.n ,
			'sum_event' : dsum.sum_event ,
			'cycle' : cycle
			}
	return result



def plot( n = None , sum_event = None ,  cycle=None ,  list_fig = None ) :
	import pylab
	if list_fig is None :
		fig = pylab.figure()
	else :
		fig = list_fig[0]
	ax = fig.add_subplot(111)
	
	ax.plot(cycle , sum_event/n)

