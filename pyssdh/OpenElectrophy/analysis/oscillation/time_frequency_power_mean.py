# -*- coding: utf-8 -*-
#
# analysis spike rate
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.OpenElectrophy.my_classes import *


analysis_name = 'Time Frequency Power Mean'

list_table_query = 	[ 'electrode' 			]
list_table_title = 	[ 'List of electrode'	]


list_param = 		['f_start'	,	'f_stop',		'df_sub' ,			't_start',		't_stop',			'dt',				'f0',	'normalisation'	]
default_param  =	[5.			,	100.,		.5 ,					-inf ,			inf,				0.000001,	2.5,	0.				]
list_label = 			['Freq start' ,	'Freq stop' , 	'Freq precision' , 	'Time start' ,	'Time stop' ,		'Time precision',	'f0',	'Normalisation'	]


n_fig = 1

help = """


"""

def compute(list_query_table , **karg ) :
	
	query_electrode = list_query_table[0]
	
	list_id_electrode, = sql(query_electrode)
	
	csum = CSummer(axis=0 , yl = (karg['f_start'],karg['f_stop']))
	
	for id_electrode in list_id_electrode :
		print id_electrode

		elec = Electrode()
		elec.load_from_db(id_electrode)
		tfreq = TimeFreq(elec , **karg )
		
		
		print abs(tfreq.tf_map()).shape , tfreq.t().shape
		#print csum.sum_sig.shape , csum.sum_sig.t.shape
		csum.add( abs(tfreq.tf_map())**2 , tfreq.t() )
		print csum.sum_sig.shape 
		
	result = { 
			'csum' : csum 
			}
	return result
	



def plot( csum =None ,  list_fig = None ) :
	import pylab
	if list_fig is None :
		fig = pylab.figure()
	else :
		fig = list_fig[0]
	
	csum.plot(fig = fig)
	
