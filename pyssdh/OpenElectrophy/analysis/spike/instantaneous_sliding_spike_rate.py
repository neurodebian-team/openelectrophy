# -*- coding: utf-8 -*-
#
# analysis spike rate
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.OpenElectrophy.my_classes import *


analysis_name = 'Instantaneous sliding spike rate'

list_table_query = 	[ 'spiketrain' 		]
list_table_title = 	[ 'List of spiketrain'	]


list_param = 		['t_start',			't_stop',		'step',					'size_win'				]
default_param  =	[0.,					5.,			.01,					.02						]
list_label = 			['Time start (s)',		'Time stop',	'Step (s.)',				'Sliding Window size (s)'		]

n_fig = 1

help = """


"""



def compute(list_query_table , t_start = 0. , t_stop = 5., step = .01 , size_win = .02 ) :
	
	query_spiketrain = list_query_table[0]
	
	list_id_spiketrain, = sql(query_spiketrain)
	
	center_win = arange(t_start , t_stop , step)
	dsum =  DSummer(size_win , center_win = center_win)
	
	for id_spiketrain in list_id_spiketrain :
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)
		
		time_spike =  sptr.time_spike()
		dsum.add(time_spike)
	
	result = { 
			'n' : dsum.n ,
			'sum_event' : dsum.sum_event ,
			'center_win' : dsum.center_win
			}
	return result



def plot( n = None , sum_event = None ,  center_win=None ,  list_fig = None ) :
	import pylab
	if list_fig is None :
		fig = pylab.figure()
	else :
		fig = list_fig[0]
	ax = fig.add_subplot(111)
	
	ax.plot(center_win , sum_event/n)

