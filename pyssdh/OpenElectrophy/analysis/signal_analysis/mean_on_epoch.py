# -*- coding: utf-8 -*-
#
# analysis spike rate
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.core.ndsummer import *
from pyssdh.OpenElectrophy.my_classes import *



analysis_name = 'Signal mean on epoch'

list_table_query = 	[ 'electrode', 		'epoch'	]
list_table_title = 	[ 'List of spiketrain',	'List of epoch']


list_param = 		['f_low',			'f_hight',		't1',		't2'	]
default_param  =	[0.1,			80., 		-.5,		1.	]
list_label = 		['Filter f_low',		'Filter f_hight',	't1 (s)',	't2 (s)']

n_fig = 1

help = """


"""



def compute(list_query_table , f_low = .01, f_hight=80.,t1 = -.5, t2 = 1. ) :
	
	query_electrode = list_query_table[0]
	query_epoch = list_query_table[1]
	
	id_electrodes, = sql(query_electrode)
	all_id_epochs, = sql(query_epoch)
	list_csum = []
	for id_electrode in id_electrodes :
		csum = CSummer(axis=0)
		elec = Electrode()
		elec.load_from_db(id_electrode)
		
		query = """
			SELECT id_epoch
			FROM epoch
			WHERE id_trial = %s
			"""
		id_epochs2, = sql(query , elec.id_trial)
		id_epochs = intersect1d(id_epochs2,all_id_epochs)
		for id_epoch in id_epochs :
			ep = Epoch()
			ep.load_from_db(id_epoch)
			
			ind, = where((elec.t() >= ep.zero+t1) & (elec.t() <= ep.zero+t2))
			elec_s = Electrode()
			elec_s.signal = elec.signal[ind]
			elec_s.shift_t0 = t1
			elec_s.fs = elec.fs
			elec_s.label = elec.label
			
			if f_low is None :f1 =0
			else :f1 = f_low/(elec_s.fs/2)
			if f_hight is None :f2 =1
			else :f2 = f_hight/(elec_s.fs/2)
			sig = fft_filter(elec_s.signal,f_low =f1,f_hight=f2 )
			csum.add(sig,elec_s.t())
			list_csum.append(csum)
			
	result = { 
			'list_csum' : list_csum ,
			}
	return result



def plot( n = None , list_csum = None , list_fig = None ) :
	import pylab
	if list_fig is None :
		list_fig = [ pylab.figure() for i in range(len(list_csum))]
	for f,fig in enumerate(list_fig) :
		ax = fig.add_subplot(111)
		list_csum[f].plot(ax_moy = ax)

