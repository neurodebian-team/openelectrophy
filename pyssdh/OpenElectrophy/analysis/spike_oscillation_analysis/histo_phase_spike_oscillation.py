# -*- coding: utf-8 -*-
#
# analysis ISI
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 03.2007
#

from pyssdh.OpenElectrophy.my_classes import *

analysis_name = 'Histogram phase spike on oscillation'

list_table_query = 	[ 'spiketrain' 		, 	'oscillation'				]
list_table_title = 	[ 'List of spiketrain',		'List of oscillation'		]


list_param = 		['nbin',					'same_electrode'								]
default_param  =	[36,						True										]
list_label = 		['Nb bin on histogram',	'Combine spike/oscillation same electrode'		]

n_fig = 1

help = """


"""



def compute(list_query_table , nbin = 36 , same_electrode = True ) :
	
	query_spiketrain = list_query_table[0]
	query_oscillation = list_query_table[1]
	
	phase_spike = empty((0))
	
	list_id_spiketrain, = sql(query_spiketrain)
	list_id_oscillation, = sql(query_oscillation )
	
	for id_spiketrain in list_id_spiketrain :
		sptr = SpikeTrain()
		sptr.load_from_db(id_spiketrain)
		pos_spike = sptr.pos_spike()
		
		if same_electrode:
			query = """ SELECT oscillation.id_oscillation
					FROM oscillation
					WHERE oscillation.id_electrode = %s
					"""
			all_oscillation, = sql(query,(sptr.id_electrode),Array=True)
		else:
			query = """ SELECT oscillation.id_oscillation
					FROM oscillation
					WHERE oscillation.id_trial = %s
					"""
			all_oscillation, = sql(query,(sptr.id_trial),Array=True)

		select_oscillation = intersect1d(list_id_oscillation,all_oscillation)
		list_oscillation = load_oscillations(list_id_oscillation = select_oscillation)
		
		for osci in list_oscillation :
			
			if (osci.line_t[1]-osci.line_t[0] > 1) :
				# we need to oversample the oscillation to full fs
				print "Resample oscillation: ",osci.id_oscillation
				fake_fs = osci.line_t[1]-osci.line_t[0]
				osci.line_t = arange(osci.line_t[0],osci.line_t[-1])
				valreal = resample_fft(array(osci.line_val).real,new_shape = array(osci.line_t).shape ,axis = 0,old_fs = fake_fs, new_fs = osci.fs)
				valimag = resample_fft(array(osci.line_val).imag,new_shape = array(osci.line_t).shape ,axis = 0,old_fs = fake_fs, new_fs = osci.fs)
				osci.line_val = valreal + valimag * 1J
				osci.line_f = resample_fft(osci.line_f,new_shape = array(osci.line_t).shape ,axis = 0,old_fs = fake_fs, new_fs = osci.fs)
							
			phase_spike = r_[phase_spike , angle(osci.line_val[setmember1d(osci.line_t,pos_spike)]) ]
			
	bins = arange(-pi,pi,2*pi/nbin)
	hist_val = histogram2(phase_spike,bins)
	
	
	result = { 
			'bins' : bins ,
			'hist_val' : hist_val 
			}
	return result
		
		
def plot( bins = None , hist_val = None ,   list_fig = None ) :
	import pylab
	if list_fig is None :
		fig = pylab.figure()
	else :
		fig = list_fig[0]
	ax = fig.add_subplot(111)
	
	bins2 = r_[bins,bins+2*pi]
	ax.bar(bins2, r_[hist_val,hist_val], width = diff(bins)[0])
	ax.plot(bins2 , cos(bins2)*max(hist_val)/2.+max(hist_val)/2. , color = 'm' , linewidth = 3)
	ax.set_xlim([-pi,3*pi])

