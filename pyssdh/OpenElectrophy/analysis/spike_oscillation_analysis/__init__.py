# -*- coding: utf-8 -*-
#
# module for recursif importing analysis
#
# samuel Garcia
# Laboratoire Neuroscience et Systemes sensoriels
# 09.2006
#



import sys
import os


sys.path.append(os.path.dirname(__file__))
list_analysis = [] #hierarchic
dict_analysis = { } #flat
for filename in os.listdir(os.path.dirname(__file__)) :
	if filename[-3:] == '.py' and filename != '__init__.py' :
		#print filename
		m = __import__(filename[:-3])
		list_analysis.append(m)
		dict_analysis[m.analysis_name] = m
	if os.path.isdir(os.path.join(os.path.dirname(__file__), filename ) ) :
		if os.path.exists( os.path.join(os.path.dirname(__file__), os.path.join(filename,'__init__.py') ) )  :
			sub = __import__(filename)
			list_analysis.append(sub.list_analysis)
			dict_analysis.update(sub.dict_analysis)

name = 'relation spike/oscillation analysis'

list_analysis = (name , list_analysis )


__all__ = [ 'list_analysis' , 'dict_analysis' ]




