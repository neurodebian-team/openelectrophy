# -*- coding: utf-8 -*-

"""
QT treeview for exploring the SQL database
See also sqlqtmodel

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from sqlqtmodel import SqlTreeModel
from dialogs import *

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from icon import dict_icon

def construct_hierarchy(dict_hierarchic_class , dict_default_browsable) :
	
	#find the first parent
	possible_parent = dict_hierarchic_class.keys()
	for table in dict_hierarchic_class.keys() :
		my_class = dict_hierarchic_class[table]()
		for table_child in my_class.list_table_child :
			if table_child in possible_parent :
				possible_parent.remove(table_child)
				
	def give_descendence(table , dict_hierarchic_class) :
		my_class = dict_hierarchic_class[table]()
		descendence = [ ]
		for table_child in my_class.list_table_child :
			if dict_default_browsable[table_child] :
				descendence += [give_descendence(table_child , dict_hierarchic_class)]
		return [ table , descendence ]

	child_hierarchy = [ ]
	for parent in possible_parent :
		if dict_default_browsable[parent] :
			child_hierarchy += [give_descendence(parent , dict_hierarchic_class)]
	
	child_hierarchy = [ ['root' , child_hierarchy  ]]
	return child_hierarchy
		

class MainExplorer(QWidget):
	def __init__(self, parent=None,
						list_title = [],
						list_child_hierarchy =[ ['root', []]  ],
						list_dict_field = [ { 'root': [] }] ,
						list_dict_query = [ { 'root' : [] } ] ,
						list_showQueryEdit = [ False ],
						dict_hierarchic_class = None,
						dict_context_menu = None,
						):
		QWidget.__init__(self, parent)
		self.dict_context_menu = dict_context_menu
		self.dict_hierarchic_class = dict_hierarchic_class
		
		mainLayout = QVBoxLayout()
		h = QHBoxLayout()
		self.labelBaseName = QLabel()
		h.addWidget(self.labelBaseName)
		btRefresh = QPushButton()
		btRefresh.setMaximumSize(28,28)
		btRefresh.setIcon(dict_icon['reload'])
		self.connect(btRefresh,SIGNAL('clicked()'),self.refreshView)
		h.addWidget(btRefresh)
		mainLayout.addLayout(h)
		
		self.tab = QTabWidget()
		
		self.listExplorer = []
		for i in range(len(list_title)) :
			self.listExplorer.append(ListExplorer(title = list_title[i] ,
												showQueryEdit = list_showQueryEdit[i],
												child_hierarchy = list_child_hierarchy[i] ,
												list_field = list_dict_field[i] ,
												list_query = list_dict_query[i] ,
												dict_hierarchic_class = dict_hierarchic_class
											))
			self.tab.addTab(self.listExplorer[-1],list_title[i])
		
		
		mainLayout.addWidget(self.tab)
		self.setLayout(mainLayout)
		
		#context menu
		for i in range(len(list_title)) :
			self.listExplorer[i].treeview.setContextMenuPolicy(Qt.CustomContextMenu)
			self.connect(self.listExplorer[i].treeview,
						SIGNAL('customContextMenuRequested( const QPoint &)'),self.contextMenuTreeview)
		
	def getPlotSelection(self) :
		tab = self.tab.currentIndex()
		return self.listExplorer[tab].getSelectionInText()
	
	def refreshView(self) :
		for t in range(len(self.listExplorer)) :
			self.listExplorer[t].refreshView()
		
	def changeBaseName(self, basename ) :
		self.labelBaseName.setText(basename)
		
	
	def contextMenuTreeview( self ):
		if self.dict_context_menu is None : return
		tab = self.tab.currentIndex()
		list_selected = self.listExplorer[tab].getSelectionInList()
		menu = QMenu()
		n = len(list_selected)
		actions = { }
		if n == 0 :
			return
		act_del = menu.addAction(self.tr('Delete'))
		if n == 1 :
			table , id_principal = list_selected[0]
			#edit context menu
			self.connect(act_del,SIGNAL('triggered()') ,self.listExplorer[tab].deleteSelection)
			act_edit = menu.addAction(self.tr('Edit fields'))
			#use custom menu with dict_context_menu
			if table in self.dict_context_menu.keys() :
				for k,v in self.dict_context_menu[table].iteritems() :
					act = menu.addAction(k)
					actions[k] = act
#					if v is not None :
#						self.connect(act,SIGNAL('triggered()') ,v)
			
		act = menu.exec_(self.cursor().pos())
		if act is None : return
		if act == act_del : return
		if act == act_edit :
			myclass = self.dict_hierarchic_class[table]()
			myclass.load_from_db(id_principal)
			w = DialogEditSqlField(parent=self , myclass = myclass)
			w.show()
			return
		k = actions.keys()[actions.values().index(act)]
		# execute the function assiociated
		if self.dict_context_menu[table][k] is not None :
			w= self.dict_context_menu[table][k](self,id_principal)
		
		
class MyTreeView(QTreeView):
	def __init__(self, parent =None) :
		QTreeView.__init__(self,parent)
		
		self.setFont(QFont("Verdana", 12))
		self.setIconSize(QSize(40,40))
		self.setSelectionMode(QAbstractItemView.ExtendedSelection)
		self.setDragEnabled(True)
		self.setAcceptDrops(True)
		self.setDropIndicatorShown(True)
		
	def dragEnterEvent(self, event ) :
		if event.mimeData().hasFormat("application/x-openelectrophy-figure"):
			if event.source() == self:
				event.accept()
			else:
				event.accept()
		else:
			event.accept()
	
	def supportedDropActions(self) :
		return Qt.CopyAction

	def dragLeaveEvent(self, event ) :
		event.ignore()
	
	def dragMoveEvent(self, event ) :
		if event.source() == self:
			event.ignore()
		else :
			event.ignore()
		
	def dropEvent(self,event):
		event.accept()
		


class ListExplorer(QFrame) :
	def __init__(self, parent=None , title=None , showQueryEdit = False ,
							child_hierarchy = [] ,  list_field = {} , list_query ={}  , dict_hierarchic_class = None) :
		QFrame.__init__(self, parent)
		
		self.title = title
		self.child_hierarchy ,  self.list_field  , self.list_query = child_hierarchy ,  list_field  , list_query
		self.dict_hierarchic_class = dict_hierarchic_class
		self.treemodel = SqlTreeModel(child_hierarchy = self.child_hierarchy ,  list_field = self.list_field , list_query =self.list_query)
		self.treeview = MyTreeView()
		self.treeview.setModel(self.treemodel)
		
		mainLayout = QVBoxLayout()
		sp = QSplitter(Qt.Vertical)
		sp.setOpaqueResize(False)
		mainLayout.addWidget(sp)
		
		if showQueryEdit :
			table =  child_hierarchy[0][1][0][0]
			self.queryBox = QueryBox(table = table)
			sp.addWidget(self.queryBox)
			self.connect(self.queryBox.bt_OkQuery,SIGNAL('clicked()'),self.bt_OkQuery_Clicked)
		
		#mainLayout.addWidget(self.treeview)
		sp.addWidget(self.treeview)
		
		#SupprShortcut = QShortcut(QKeySequence(QtCore.Qt.CTRL+QtCore.Qt.SHIFT+QtCore.Qt.Key_Backspace),self)
		DeleteShortcut = QShortcut(QKeySequence(Qt.Key_Delete),self)
		self.connect(DeleteShortcut, SIGNAL("activated()"), self.deleteSelection)

		
		self.setLayout(mainLayout)
		
	def bt_OkQuery_Clicked(self) :
		query =  self.queryBox.get_query()
		self.list_query[self.title] = query
		self.treemodel = SqlTreeModel(child_hierarchy = self.child_hierarchy ,  list_field = self.list_field , list_query =self.list_query)
		self.treeview.setModel(self.treemodel)
		
	def refreshView(self) :
		self.treemodel = SqlTreeModel(child_hierarchy = self.child_hierarchy , 
						 list_field = self.list_field , 
						 list_query =self.list_query)
		self.treeview.setModel(self.treemodel)
		
	def getSelectionInText(self) :
		data = ''
		for ind in  self.treeview.selectedIndexes() :
			if ind.column() == 0 :
				data += self.treemodel.data(ind , 'table_index' )+';'
		return data
	
	def getSelectionInList(self) :
		data = []
		for d in self.getSelectionInText().split(';') :
			if d =='' : continue
			table, id_principal = d.split(':')
			table = str(table)
			id_principal = int(id_principal)
			data += [ (table , id_principal ) ]
		return data
		
	def deleteSelection(self) :
		mb = QMessageBox.warning(self,self.tr('Delete'),self.tr("You want to delete this and all its descendence ?"), 
				QMessageBox.Ok ,QMessageBox.Cancel  | QMessageBox.Default  | QMessageBox.Escape,
				QMessageBox.NoButton)
		if mb == QMessageBox.Cancel : return
		mb = QMessageBox.warning(self,self.tr('Delete'),self.tr("Are you sure ?"), 
				QMessageBox.Ok ,QMessageBox.Cancel  | QMessageBox.Default  | QMessageBox.Escape,
				QMessageBox.NoButton)
		if mb == QMessageBox.Cancel : return
		mb = QMessageBox.warning(self,self.tr('Delete'),self.tr("Are you really sure ?"), 
				QMessageBox.Ok ,QMessageBox.Cancel  | QMessageBox.Default  | QMessageBox.Escape,
				QMessageBox.NoButton)
		if mb == QMessageBox.Cancel : return


		
		for table, id_principal in self.getSelectionInList() :
			#exec('my_class = '+table+'()')
			my_class = self.dict_hierarchic_class[table]()
			my_class.id_principal = id_principal
			#my_class.delete_from_db_and_child(dict_hierarchic_class)
			
			try :
				my_class.delete_from_db_and_child(self.dict_hierarchic_class)
			except :
				print 'yep'
				pass
			self.refreshView()



