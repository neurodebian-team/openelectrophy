# -*- coding: utf-8 -*-

"""
datamodel for the QTreeView 


Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


from PyQt4.QtCore import *
from PyQt4.QtGui import *

from numpy import *

import time

from sql_util import *


def explore_hierarchy(hierarchy, table) :
	for v in hierarchy :
		if v[0]== table : return v
		elif v[1]== []  : continue
		else :
			r = explore_hierarchy(v[1],table)
			if r is None : continue
			else :return r


#------------------------------------------------------------------------------
class PrincipalTreeItem :
	def __init__(self, table,id_principal, parent=None , 
					child_hierarchy = [] ,  list_field = {} , list_query ={} ):
		self.table = table
		self.id_principal = id_principal
		self.child_hierarchy ,  self.list_field  , self.list_query = child_hierarchy ,  list_field  , list_query
		
		self.parentItem = parent
		self.childItems = None
		self.data_col = []
		if self.table != 'root' :
			for i in range(len(self.table_column())) :
				#t1 = time.time()
				#print 'SELECT %s FROM %s WHERE id_%s = %s ' % (self.table_column()[i] , self.table , self.table , self.id_principal),
				res = sql1('SELECT %s FROM %s WHERE id_%s = %s ' % (self.table_column()[i] , self.table , self.table , self.id_principal))[0]
				#t2 = time.time()
				#print t2-t1
				self.data_col.append(res)

#------------------------------------------------------------------------------
	def table_child(self) :
		return [ t[0] for t in explore_hierarchy(self.child_hierarchy,self.table)[1] ]
	
	def table_column(self) :
		return self.list_field[self.table]

	
	def list_id(self , table_child, table,id_principal ) :
		if table == 'root' :
			#print 'SELECT id_%s  FROM %s WHERE 1' %(table_child, table_child,),
			#t1 = time.time()
			id_child, = sql('SELECT id_%s  FROM %s WHERE 1' %(table_child, table_child,) , Array = True, dtype = 'i')
			#t2 = time.time()
			#print t2-t1
		else :
			#print 'SELECT id_%s  FROM %s WHERE id_%s = %s' %(table_child, table_child,table,id_principal),
			#t1 = time.time()
			id_child, = sql('SELECT id_%s  FROM %s WHERE id_%s = %s' %(table_child, table_child,table,id_principal) , Array = True, dtype = 'i')
			#t2 = time.time()
			#print t2-t1
		if self.list_query[table_child] != '' :
			try :
				#id_child2, =  sql(self.list_query[table_child])
				#id_child = intersect1d(id_child,id_child2).tolist()
				#print self.list_query[table_child],
				#t1 = time.time()
				id_child2, =  sql(self.list_query[table_child] , Array= True , dtype = 'i')
				#t2 = time.time()
				#print t2-t1
				b = setmember1d(id_child2 , id_child )
				id_child = id_child2[b]
			except :
				pass
		return id_child.tolist()
	
	def childpos(self , childItem) :
		if self.childItems is None : self.construct_child()
		if childItem in self.childItems :
			return  self.childItems.index(childItem)
		else :
			return None
		
	#------------------------------------------------------------------------------
	def construct_child(self) :
		if self.childItems is None :
			#construction of the list
			list_child_table = []
			list_child_id = []
			for t in self.table_child() :
				if not(is_table(t)) : continue
				id_child = self.list_id(t, self.table,self.id_principal )
				list_child_id +=id_child
				list_child_table += [t] * len(id_child)
			self.childItems = []
			for i in range(len(list_child_table)):
				self.childItems.append( PrincipalTreeItem(list_child_table[i],list_child_id[i],parent = self,
										child_hierarchy = self.child_hierarchy ,  list_field = self.list_field , list_query =self.list_query) )
	
	#------------------------------------------------------------------------------
	def child(self, row):
		if self.childItems is None : self.construct_child()
		if row<len(self.childItems) :
			return self.childItems[row] 
		else:
			return None


	def childCount(self):
		if self.childItems is None : self.construct_child()
		return len(self.childItems)
		
	def columnCount(self):
		return len(self.table_column())+1

	def data(self, column):
		if self.table =='root' :
			res = ''
			fieldName ='' 
		elif column>len(self.table_column()):
			res = ''
			fieldName =''
		elif column==0 :
			res = str(self.id_principal)
			fieldName =self.table+' id'
		else :
			#print query2
			res = self.data_col[column-1]
#			res = sql1('SELECT %s FROM %s WHERE id_%s = %s ' % (self.table_column()[column-1] , self.table , self.table , self.id_principal))[0]
			fieldName = self.table_column()[column-1]
			
		return self.table, self.id_principal, fieldName,res


	def parent(self):
		return self.parentItem

	def row(self):
		if self.parentItem:
			return self.parentItem.childpos(self)
		return 0
#------------------------------------------------------------------------------	
		
		

#------------------------------------------------------------------------------
class SqlTreeModel(QAbstractItemModel):
	def __init__(self, parent=None ,child_hierarchy = [] ,  list_field = {} , list_query ={} ):
		QAbstractItemModel.__init__(self, parent)
		self.child_hierarchy ,  self.list_field  , self.list_query = child_hierarchy ,  list_field  , list_query
		self.rootItem = PrincipalTreeItem('root' , None,parent = None,
						child_hierarchy = self.child_hierarchy ,  list_field = self.list_field , list_query =self.list_query)


	def columnCount(self, parent):
		if parent.isValid():
			return parent.internalPointer().columnCount()
		else:
			return self.rootItem.columnCount()

	def data(self, index, role):
		if not index.isValid():
			return QVariant()
		item = index.internalPointer()
		table , id_principal , fieldName , res = item.data(index.column())
		if role ==Qt.DisplayRole :
			#return QVariant( QString( fieldName+' : '+str(res) ) )
			return QVariant( QString( fieldName+' : '+unicode(res) ) )
		elif role == Qt.DecorationRole :
			return QVariant() #TODO gerrer les icones
		elif role == 'table_index' :
			return table+':'+str(id_principal)
		else :
			return QVariant()

	def flags(self, index):
		if not index.isValid():
			return Qt.ItemIsEnabled

		return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsDragEnabled
	
	def mimeTypes(self) :
		types = QStringList()
		types << "application/x-openelectrophy-figure"
		return types
	
	def mimeData(self , indexes) :
		mimeData = QMimeData()
		
		data = ""
		for index in indexes :
			if index.column() == 0 :
				data += self.data(index, role='table_index')+';'
		
		mimeData.setData( "application/x-openelectrophy-figure",data)
		#print dir(mimeData)
		#print mimeData.data("application/x-openelectrophy-figure")
		return mimeData

	def headerData(self, section, orientation, role):
		if orientation == Qt.Horizontal and role == Qt.DisplayRole:
			table , id_principal , fieldName , res = self.rootItem.data(section)
			return QVariant(fieldName)

		return QVariant()

	def index(self, row, column, parent):
		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
		#print row , parentItem.child
		childItem = parentItem.child(row)
		if childItem:
			return self.createIndex(row, column, childItem)
		else:
			return QModelIndex()

	def parent(self, index):
		if not index.isValid():
			return QModelIndex()

		childItem = index.internalPointer()
		parentItem = childItem.parent()

 
		if parentItem == self.rootItem:
			return QModelIndex()
		return self.createIndex(parentItem.row(), 0, parentItem)

	def rowCount(self, parent):

		if not parent.isValid():
			parentItem = self.rootItem
		else:
			parentItem = parent.internalPointer()
		return parentItem.childCount()
		
		
		

