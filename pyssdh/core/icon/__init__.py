# -*- coding: utf-8 -*-

"""
for managing icon

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


import sys
import os

from PyQt4.QtCore import *
from PyQt4.QtGui import *


sys.path.append(os.path.dirname(__file__))
dict_icon = { }
for filename in os.listdir(os.path.dirname(__file__)) :
	if filename[-3:] == 'png' and filename != '__init__.py' :
		#print filename
		dict_icon[filename[:-4]] =QIcon(os.path.join(os.path.dirname(__file__),filename))
		
__all__ = ['dict_icon']
 

	
	