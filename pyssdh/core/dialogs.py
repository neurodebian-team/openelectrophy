# -*- coding: utf-8 -*-

"""
some Qt based dialogs:
		Widget for change dir
		Dialog and widget for GUI paramaters
		Widget for sql query
		Dialog for connection to MySQL

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



import sys
import os
import datetime
import re

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from sql_util import *

from icon import dict_icon

global applicationname
applicationname = 'pyssdh'
global usersetting
import usersettings
usersetting = usersettings.UserSettings(name = applicationname)

'2007-11-12 21:50:12.000120'

from scipy import *


#------------------------------------------------------------------------------
# Widget for choosing a File or a dir
#------------------------------------------------------------------------------
class ChooseFileDirWidget(QFrame) :
	def __init__( self , parent = None, family = None , type_choose = QFileDialog.Directory ) :
		QFrame.__init__(self , parent)
		self.family  = family
		self.type_choose = type_choose
		mainLayout = QHBoxLayout()
		self.setLayout(mainLayout)
		
		self.lineEdit = QLineEdit()
		mainLayout.addWidget(self.lineEdit)
		bt_change = QPushButton('...')
		bt_change.setMaximumSize(20,20)
		mainLayout.addWidget(bt_change)
		self.connect(bt_change,SIGNAL("clicked()"), self.change)
		
		if self.family is not None :
			usersetting.add_family( self.family, { 'path' : ''} )
			path  = usersetting.load_last(self.family)['path']
			self.lineEdit.setText(path)
	
	#------------------------------------------------------------------------------
	def change(self) :
		fd = QFileDialog()
		fd.setAcceptMode(QFileDialog.AcceptOpen)
		fd.setFileMode(self.type_choose)
		fd.setDirectory(os.path.split(str(self.lineEdit.text()))[0])
		if (fd.exec_()) :
			fileNames = fd.selectedFiles()
			l = u''
			for f in  fileNames :
				 l += unicode(f)+u';'
			self.lineEdit.setText(l)
			#self.lineEdit.setText(str(fd.selectedFiles()[0]))
			
			if self.family is not None :
				usersetting.save_last(self.family , {'path' : unicode(fd.selectedFiles()[0]) } )
	
	#------------------------------------------------------------------------------
	def get_choose(self) :
		return self.list_file()[0]
	
	#------------------------------------------------------------------------------
	def list_file(self) :
		l = unicode(self.lineEdit.text()).split(';')
		while '' in l :
			l.remove('')
		return l


#------------------------------------------------------------------------------
# Widget for choosing a directory
#------------------------------------------------------------------------------
class ChooseDirWidget(ChooseFileDirWidget) :
	def __init__( self , parent = None, family = None ) :
		ChooseFileDirWidget.__init__(self , parent = parent , family = family , type_choose = QFileDialog.Directory )
		
	#------------------------------------------------------------------------------
	def get_dir(self) :
		return self.get_choose()

	#------------------------------------------------------------------------------
	def set_dir(self, path) :
		l = unicode(path)+u';'
		self.lineEdit.setText(l)


#------------------------------------------------------------------------------
# Widget for choosing a directory
#------------------------------------------------------------------------------
class ChooseFileWidget(ChooseFileDirWidget) :
	def __init__( self , parent = None, family = None ) :
		ChooseFileDirWidget.__init__(self , parent = parent , family = family , type_choose = QFileDialog.AnyFile )
		
	#------------------------------------------------------------------------------
	def get_file(self) :
		return self.get_choose()



#------------------------------------------------------------------------------
# Dialog and widget for parameters
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
class ParamWidget(QFrame) :
	def __init__(self, list_param , default_param , list_label = None  , parent = None ,
					 family=None, list_type=None ):
		QFrame.__init__(self, parent)
		
		self.list_param = list_param
		self.default_param = default_param
		dict_param = { }
		for i in range(len(list_param)) :
			dict_param[list_param[i]] = default_param[i]
		self.family = family
		
		self.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		v1 = QVBoxLayout()
		self.setLayout(v1)
		
		if family is not None :
			usersetting.add_family(family, dict_param)
			self.family_param = usersetting.load(family)
			h1 = QHBoxLayout()
			v1.addLayout(h1)
			self.comboBox_param = QComboBox()
			h1.addWidget(self.comboBox_param)
			self.comboBox_param.addItems(['Default' , ]+self.family_param['name']  )
			self.connect(self.comboBox_param,SIGNAL('currentIndexChanged( int  )') , self.comboBoxChangeParam )
			buttonSave = QPushButton('+')
			buttonSave.setMaximumSize(20,20)
			#buttonSave.setIcon(Icon(dict_icon['filesave.png']))
			h1.addWidget(buttonSave)
			self.connect(buttonSave,SIGNAL('clicked()') ,self.saveNewParam )
			buttonDel = QPushButton('-')
			buttonDel.setMaximumSize(20,20)
			#buttonSave.setIcon(Icon(dict_icon['filesave.png']))
			h1.addWidget(buttonDel)
			self.connect(buttonDel,SIGNAL('clicked()') ,self.delSavedParam )
			
		qg = QGridLayout()
		v1.addLayout(qg)
		#self.setLayout(qg)
		
		self.list_widget = []
		construct_list_type=list_type is None
		if construct_list_type:
			self.list_type = []
		else:
			if len(list_type)<len(default_param):
				print "Length of list types is too short !"
			self.list_type=list_type
		
		if list_label == None :
			list_label = list_param
		for i in range(len(list_param)) :
			qg.addWidget(QLabel(list_label[i]),i,0)
			if type(default_param[i]) == type(True) :
				self.list_widget.append(QCheckBox())
				#self.list_widget[-1].setChecked(default_param[i])
			else :
				#self.list_widget.append(QLineEdit(str(default_param[i])))
				self.list_widget.append(QLineEdit())
			if construct_list_type:
				self.list_type.append(type(default_param[i]))
			qg.addWidget(self.list_widget[-1],i,1)
		self.set_param(dict_param)
	
	#------------------------------------------------------------------------------
	def set_param(self , dict_param) :
		for i in range(len(self.list_param)) :
			param = self.list_param[i]
			self.set_one_param(param , dict_param[param] )
	
	#------------------------------------------------------------------------------
	def get_param(self) :
		list_value = []
		for i in range(len(self.list_param)) :
			list_value.append(self.get_one_param(self.list_param[i]))
		return list_value
	
	#------------------------------------------------------------------------------
	def set_one_param(self, param , val ) :
		if param in self.list_param :
			i = self.list_param.index(param)
			if type(self.default_param[i]) == type(True) :
				self.list_widget[i].setChecked(val)
			elif val is None:
				self.list_widget[i].setText(unicode('None'))
			elif type(self.default_param[i]) == type(0.) :
				# patch for win32 for float(numpy.inf) -> '1.#IND'
				if isfinite(val) :
					self.list_widget[i].setText(unicode(val))
				else :
					list_infinite = [nan, inf, -inf]
					list_str_infinite = ['nan' , 'inf' , '-inf' ]
					if val in list_infinite :
						self.list_widget[i].setText(list_str_infinite[ list_infinite.index(val) ])
				#if isfinite(self.default_param[i]) :
					#self.list_widget[i].setText(unicode(val))
				#else :
					#list_infinite = [nan, inf, -inf]
					##list_str_infinite_bug  = [ str(i) for i in list_infinite ]
					#list_str_infinite = ['nan' , 'inf' , '-inf' ]
					#if val in list_infinite :
						#self.list_widget[i].setText(list_str_infinite[ list_infinite.index(val) ])
			else :
				self.list_widget[i].setText(unicode(val))
			
	#------------------------------------------------------------------------------
	def get_one_param(self, param ) :
		if param in self.list_param :
			i = self.list_param.index(param)
			if self.list_type[i] == type(True) :
				return  self.list_widget[i].isChecked()
			elif str(self.list_widget[i].text()).strip()=='None':
				return None
			elif type(self.default_param[i]) == type(0.) :
				# patch for win32 for float(numpy.inf) -> '1.#IND'
				t = self.list_widget[i].text()
				list_infinite = [nan, inf, -inf]
				list_str_infinite = ['nan' , 'inf' , '-inf' ]
				if t in list_str_infinite :
					return list_infinite[list_str_infinite.index(t)]
				else :
					return self.list_type[i]( self.list_widget[i].text() )
				#if isfinite(self.default_param[i]) :
					#print 'ici' , self.list_widget[i].text()
					#return self.list_type[i]( self.list_widget[i].text() )
				#else :
					#list_infinite = [nan, inf, -inf]
					##list_str_infinite_bug  = [ str(i) for i in list_infinite ]
					#list_str_infinite = ['nan' , 'inf' , '-inf' ]
					#t = self.list_widget[i].text()
					#print 'la' , t
					#if t in list_str_infinite :
						#return list_infinite[list_str_infinite.index(t)]
					#else :
						#return self.default_param[i]
			elif type(self.default_param[i]) == datetime.date :
				r = re.findall('(\d+)\-(\d+)\-(\d+)',str(self.list_widget[i].text()))
				if len(r) <1 :
					return self.default_param[i]
				else :
					YY , MM , DD =r[0]
					return datetime.date( int(YY) , int(MM) , int(DD) )
			elif type(self.default_param[i]) == datetime.datetime :
				r = re.findall('(\d+)\-(\d+)\-(\d+) (\d+):(\d+):(\d+)(.\d*)?',str(self.list_widget[i].text()))
				if len(r) <1 :
					return self.default_param[i]
				else :
					YY , MM , DD , hh, mm , ss , ms =r[0]
					if ms =='' :
						return datetime.datetime(int(YY) , int(MM) , int(DD) , int(hh), int(mm) , int(ss) )
					else :
						return datetime.datetime(int(YY) , int(MM) , int(DD) , int(hh), int(mm) , int(ss) , int(ms[1:]))
			elif self.list_type[i] is type(None):
				return None
			else :
				return self.list_type[i]( self.list_widget[i].text() )
		
	#------------------------------------------------------------------------------
	def get_dict(self) :
		d ={}
		for i,key in enumerate(self.list_param) :
			d[key] = self.get_param()[i]
		return d
	
	#------------------------------------------------------------------------------
	def comboBoxChangeParam(self , pos) :
		if pos == 0 :
			new_param = self.family_param['default']
		else :
			new_param = self.family_param['saved'][pos-1]
		self.set_param(new_param)
		
	#------------------------------------------------------------------------------
	def saveNewParam( self ) :
		name = ParamDialog(  ['name'] , [ 'name' ] )[0]
		usersetting.save_new(self.family, name , self.get_dict())
		self.family_param = usersetting.load(self.family)
		self.comboBox_param.clear()
		self.comboBox_param.addItems(['Default' , ]+self.family_param['name']  )
		self.comboBox_param.setCurrentIndex(len(self.family_param['name']))
		
	#------------------------------------------------------------------------------
	def delSavedParam( self) :
		pos = self.comboBox_param.currentIndex()
		if pos == 0: return
		usersetting.del_saved(self.family,pos-1)
		self.family_param = usersetting.load(self.family)
		self.comboBox_param.clear()
		self.comboBox_param.addItems(['Default' , ]+self.family_param['name']  )



	
	
#------------------------------------------------------------------------------
class ParamDialogTemplate(QDialog):
	def __init__(self, list_param , default_param , list_label = None  , parent = None ,  family = None, title=''):
		QDialog.__init__(self, parent)
		self.setWindowTitle (title)
		mainLayout = QVBoxLayout()
		self.param_widget = ParamWidget(list_param = list_param, default_param = default_param , list_label = list_label , family = family)
		mainLayout.addWidget(self.param_widget)
		h = QHBoxLayout()
		self.buttonOK = QPushButton('OK')
		h.addWidget(self.buttonOK)
		self.connect(self.buttonOK , SIGNAL('clicked()') , self , SLOT('accept()'))
		#self.buttonCancel = QPushButton('Cancel')
		#self.connect(self.buttonOK , SIGNAL('clicked()') , self , SLOT('reject()'))
		#h.addWidget(self.buttonCancel)
		mainLayout.addLayout(h)
		
		self.connect(self , SIGNAL('accepted()') , self.ok)
		self.connect(self , SIGNAL('rejected()') , self.cancel)
		
		self.setLayout(mainLayout)
		self.setModal(True)
	
	def ok(self) :
		pass
	def cancel(self) :
		pass


#------------------------------------------------------------------------------
def ParamDialog(  list_param , default_param , list_label = None , family = None, parent = None) :
	dialog_param = ParamDialogTemplate(list_param , default_param , list_label,
								family = family, parent = parent )
	list_value = []
#	for i in range(len(list_param)) :
#		list_value.append(dialog_param.list_type[i]( dialog_param.list_edit[i].text() ) )
	dialog_param.exec_()
	list_value = dialog_param.param_widget.get_param()
	#del(dialog_param)
	
	return list_value




#------------------------------------------------------------------------------
# Dialog for sql databases
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
class ConnectionDialogSQL(ParamDialogTemplate):
	def __init__(self, parent=None):
		list_param = ['server' , 'login' , 'password' ]
		default_param = ['' , '', '']
		list_label = ['server' , 'login' , 'password' ]
		family = 'sql_connection'
		ParamDialogTemplate.__init__(self,list_param , default_param , list_label = list_label  , parent = parent,  family = family )
		self.setWindowTitle (self.tr('Connection to MySQL'))
		self.param_widget.list_widget[2].setEchoMode(QLineEdit.Password)
		
#------------------------------------------------------------------------------
def ConnectionSql() :
	dialog_sql = ConnectionDialogSQL()
	dialog_sql.exec_()
	list_value = dialog_sql.param_widget.get_param()
	return tuple(list_value)
	
		
#------------------------------------------------------------------------------
class  ChooseBaseDialog(QDialog):
	def __init__(self, parent=None ):
		QDialog.__init__(self, parent)
		self.setWindowTitle (self.tr('Choose database'))
		mainLayout = QVBoxLayout()
		
		mainLayout.addWidget(QLabel(self.tr('Choose Database')))
		self.comboBox_base = QComboBox()
		mainLayout.addWidget(self.comboBox_base)
		self.comboBox_base.addItems(sql('SHOW DATABASES')[0])
		
		h = QHBoxLayout()
		self.buttonOK = QPushButton('OK')
		h.addWidget(self.buttonOK)
		self.connect(self.buttonOK , SIGNAL('clicked()') , self , SLOT('accept()'))
		self.buttonCancel = QPushButton('Cancel')
		self.connect(self.buttonCancel , SIGNAL('clicked()') , self , SLOT('reject()'))
		h.addWidget(self.buttonCancel)
		mainLayout.addLayout(h)

		self.setModal(True)
		
		self.connect(self , SIGNAL('accepted()') , self.ok)
		self.connect(self , SIGNAL('rejected()') , self.cancel)
		
		self.setLayout(mainLayout)
		
	def ok(self) :
		self.database_name =  str(self.comboBox_base.currentText())

	def cancel(self) :
		self.database_name = None




#------------------------------------------------------------------------------
# Dialog and widget for editing sql and treeview for sql
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
class QueryBox(QFrame) :
	def __init__(self, parent=None , table = '' , default_query = None) :
		QFrame.__init__(self, parent)
		self.table = table
		self.family = table + ' query'
		if default_query is None :
			default_query = 'SELECT id_%s FROM %s' % (table,table)
		dict_param = { 'query' : default_query }
		usersetting.add_family(self.family, dict_param)
		self.family_query = usersetting.load(self.family)
		
		mainLayout = QVBoxLayout()
		h = QHBoxLayout()
		mainLayout.addLayout(h)
		
		self.comboBox_query = QComboBox()
		h.addWidget(self.comboBox_query)
		self.comboBox_query.addItems(['Default' , ]+self.family_query['name']  )
		self.connect(self.comboBox_query,SIGNAL('currentIndexChanged( int  )') , self.comboBoxChangeQuery )
		buttonSave = QPushButton('+')
		buttonSave.setMaximumSize(20,20)
		#buttonSave.setIcon(Icon(dict_icon['filesave.png']))
		h.addWidget(buttonSave)
		self.connect(buttonSave,SIGNAL('clicked()') ,self.saveNewQuery )
		buttonDel = QPushButton('-')
		buttonDel.setMaximumSize(20,20)
		#buttonSave.setIcon(Icon(dict_icon['filesave.png']))
		h.addWidget(buttonDel)
		self.connect(buttonDel,SIGNAL('clicked()') ,self.delSavedQuery )
		
		h = QHBoxLayout()
		mainLayout.addLayout(h)
		self.editor = QTextEdit(self.family_query['default']['query'])
		h.addWidget(self.editor)
		self.bt_OkQuery = QPushButton('OK')
		h.addWidget(self.bt_OkQuery)
		self.setLayout(mainLayout)
	
	#------------------------------------------------------------------------------
	def get_query(self) :
		return unicode(self.editor.toPlainText ())
	#------------------------------------------------------------------------------
	def comboBoxChangeQuery(self , pos) :
		if pos == 0 :
			query = self.family_query['default']['query']
		else :
			query = self.family_query['saved'][pos-1]['query']
		self.editor.setPlainText(query)

	#------------------------------------------------------------------------------
	def saveNewQuery( self ) :
		name = ParamDialog(  ['name'] , [ 'name' ] )[0]
		usersetting.save_new(self.family, name , { 'query' : self.get_query() } )
		self.family_query = usersetting.load(self.family)
		self.comboBox_query.clear()
		self.comboBox_query.addItems(['Default' , ]+self.family_query['name']  )
		self.comboBox_query.setCurrentIndex(len(self.family_query['name']))
	#------------------------------------------------------------------------------
	def delSavedQuery( self) :
		pos = self.comboBox_query.currentIndex()
		if pos == 0: return
		usersetting.del_saved(self.family,pos-1)
		self.family_query = usersetting.load(self.family)
		self.comboBox_query.clear()
		self.comboBox_query.addItems(['Default' , ]+self.family_query['name']  )
	
#------------------------------------------------------------------------------
class QueryResultBox(QFrame) :
	#------------------------------------------------------------------------------
	def __init__(self, parent=None, table = '', field_list = [] , default_query = None ) :
		QFrame.__init__(self, parent)
		self.field_list = field_list
		self.table = table
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		sp = QSplitter(Qt.Vertical)
		sp.setOpaqueResize(False)
		mainLayout.addWidget(sp)
		self.queryBox = QueryBox(table = self.table , default_query =  default_query)
		sp.addWidget(self.queryBox)
		self.connect(self.queryBox.bt_OkQuery,SIGNAL('clicked()'),self.refreshResult)
		self.treeview_result = QTreeWidget()
		self.treeview_result.setColumnCount(1+len(field_list))
		self.treeview_result.setHeaderLabels( [ 'id_'+table ]+field_list)
		sp.addWidget(self.treeview_result)
		self.refreshResult()
		
	#------------------------------------------------------------------------------
	def refreshResult(self) :
		self.treeview_result.clear()
		query = self.queryBox.get_query()
		id_principals, = sql(query)
		for id_principal in id_principals :
			query = ' SELECT id_%s, ' % (self.table)
			for field in self.field_list :
				query += ' %s,' % field
			query = query[:-1]
			query += ' FROM %s WHERE id_%s = %s' % (self.table, self.table,id_principal)
			val = sql1(query)
			item = QTreeWidgetItem([ unicode(v) for v in val ])
			self.treeview_result.addTopLevelItem(item)
	
	#------------------------------------------------------------------------------
	def get_query(self) :
		return self.queryBox.get_query()
		

#------------------------------------------------------------------------------
# Dialog for editing sql field in treeview
#------------------------------------------------------------------------------

class DialogEditSqlField(QWidget):
	#------------------------------------------------------------------------------
	def __init__(self, parent=None , myclass = None ):
		QWidget.__init__(self, parent)
		self.setWindowFlags(Qt.Window)
		
		self.myclass = myclass
		
		mainLayout = QVBoxLayout()
		self.setLayout(mainLayout)
		
		list_param = [ ]
		default_param = [ ]
		list_type = [ ]
		for f,field in enumerate(myclass.list_field) :
			if myclass.field_type[f] != 'INDEX' and \
						myclass.field_type[f] !='NUMPY' and \
						myclass.field_type[f] !='PYOBJECT' :
				list_param.append(field)
				default_param += [ myclass[field] ]
				if myclass[field] is not None:
					list_type.append(type(myclass[field]))
				elif myclass.field_type[f]=='INT':
					list_type.append(type(0))
				elif myclass.field_type[f]=='FLOAT':
					list_type.append(type(0.))
				elif myclass.field_type[f]=='TEXT':
					list_type.append(type(''))
				else:
					list_type.append(type(None))

		self.paramWidget = ParamWidget(list_param , default_param , list_label = None  , parent = parent , family=None, list_type = list_type )
		mainLayout.addWidget(self.paramWidget)
		
		frame = QFrame()
		frame.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		h = QHBoxLayout()
		self.buttonSave = QPushButton('Save to database')
		self.buttonSave.setIcon(dict_icon['filesave'])
		h.addWidget(self.buttonSave)
		h.addStretch(10)
		self.buttonQuit = QPushButton('Quit')
		self.buttonQuit.setIcon(dict_icon['fileclose'])
		h.addWidget(self.buttonQuit)
		frame.setLayout(h)
		mainLayout.addWidget(frame)
		
		self.connect(self.buttonSave , SIGNAL('clicked()') , self.save_to_db)
		self.connect(self.buttonQuit , SIGNAL('clicked()') , self , SLOT('close()') )
		
		
	#------------------------------------------------------------------------------
	def save_to_db(self) :
		for p,param in enumerate(self.paramWidget.list_param) :
			self.myclass[param] = self.paramWidget.get_one_param(param)
		self.myclass.save_to_db()
		self.close()
	
