# -*- coding: utf-8 -*-


"""
module for user setting : parameters, connections, ...
manage family of parameters with : default, last, history, ...

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""



"""
self.usersettings_dict['family'] is a disct with
	default :		dict with default parameters
	saved : 	list of dict
	name:	user name
	prefered:			list with count of use
	last : 	number of last in the list
"""

import sys , os
import pickle

class UserSettings:
	#------------------------------------------------------------------------------
	def __init__(self,name = 'pyssdh'):
		self.name = name
		if sys.platform =='win32' :
			import win32api
			username = win32api.GetUserName()
			usersetting_dir = os.path.join(win32api.GetEnvironmentVariable('USERPROFILE'),'Application data/%s'%name)
		if sys.platform[:5] =='linux' :
			usersetting_dir = os.path.join(os.path.expanduser("~"),'.%s/' % name)
		if sys.platform == 'darwin' :
                        username = os.getlogin()
                        usersetting_dir = os.path.join(username,'/Users/'+username+'/.%s'%name)
		self.usersettings_file = os.path.join(usersetting_dir,'usersettings')
		if not(os.path.isdir(usersetting_dir)) :
			os.mkdir(usersetting_dir)

		if os.path.isfile(self.usersettings_file) :
			self.usersettings_dict = self.load_usersettings(self.usersettings_file)
		else :
			self.usersettings_dict = { }
		
	#------------------------------------------------------------------------------
	def add_family(self,family, param) :
		if family not in self.usersettings_dict.keys() :
			self.usersettings_dict[family] = { }
			self.usersettings_dict[family]['default'] = param
			self.usersettings_dict[family]['saved'] = []
			self.usersettings_dict[family]['name'] = []
			self.usersettings_dict[family]['prefered'] = []
			self.usersettings_dict[family]['last'] = None
		else :
			self.usersettings_dict[family]['default'] = param
		self.save_usersetting(self.usersettings_file)
	
	#------------------------------------------------------------------------------
	def save_new(self,family, name , param) :
		self.usersettings_dict[family]['saved'] += [param]
		self.usersettings_dict[family]['name'] += [name]
		self.usersettings_dict[family]['prefered'] += [0]
		self.save_usersetting(self.usersettings_file)
	
	#------------------------------------------------------------------------------
	def del_saved(self,family,pos) :
		del self.usersettings_dict[family]['saved'][pos]
		del self.usersettings_dict[family]['name'][pos]
		del self.usersettings_dict[family]['prefered'][pos]
		self.save_usersetting(self.usersettings_file)
	
	#------------------------------------------------------------------------------
	def load_last(self,family) :
		l = self.usersettings_dict[family]['last']
		if l is not None :
			for k in self.usersettings_dict[family]['default'].keys() :
				if k not in l.keys() :
					l[k] = self.usersettings_dict[family]['default'][k]
		else :
			l = self.usersettings_dict[family]['default']
		return l
	
	#------------------------------------------------------------------------------
	def save_last(self,family , param) :
		self.usersettings_dict[family]['last'] = param
		self.save_usersetting(self.usersettings_file)
	
	#------------------------------------------------------------------------------
	def load_default(self,family) :
		return self.usersettings_dict[family]['default']
	
	#------------------------------------------------------------------------------
	def load(self,family) :
		return self.usersettings_dict[family]
	
	#------------------------------------------------------------------------------
	def save_usersetting(self,filename) :
			fid = open(filename,'wb')
			pickle.dump(self.usersettings_dict,fid)
			fid.close()
			
	#------------------------------------------------------------------------------
	def load_usersettings(self,filename) :
		try :
			fid = open(filename,'rb')
			d = pickle.load(fid)
			fid.close()
			return d
		except :
			return { }
	


