# -*- coding: utf-8 -*-

"""
widget for managing several matplotlib figure


Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from my_mpl_widget import *

from dialogs import *
from icon import dict_icon


class FigureAndTool(QFrame) :
	def __init__(self,parent=None ,  figureZone = None) :
		QFrame.__init__(self,parent)
		#self.setFrameStyle(QFrame.Sunken | QFrame.StyledPanel)
		self.figureZone = figureZone
		h1 = QHBoxLayout()
		#v2 = QVBoxLayout()
		#h1.addLayout(v2)
		
		self.canvas = MyMplCanvas()
		self.fig = self.canvas.fig
		self.ax = self.canvas.ax
		#v2.addWidget(self.canvas)
		self.figToolBar = MyNavigationToolbar(self.canvas , self.canvas , direction = 'v')
		#v2.addWidget(self.figToolBar)
		v2 = QVBoxLayout()
		h1.addLayout(v2)
		v2.addWidget(self.figToolBar)
		self.tb_link = QPushButton()
		self.tb_link.setCheckable(True)
		self.tb_link.setMaximumSize(28,28)
		self.tb_link.setIcon(dict_icon['link'])
		self.connect(self.tb_link,SIGNAL('clicked()'),self.figureZone.refresh_link)
		v2.addWidget(self.tb_link)
		
		bt_close = QPushButton()
		bt_close.setMaximumSize(28,28)
		bt_close.setIcon(dict_icon['fileclose'])
		self.connect(bt_close,SIGNAL('clicked()'),self.close)
		#bt_close.setFlat(True)
		v2.addWidget(bt_close)
		h1.addWidget(self.canvas)
		self.setLayout(h1)
		
		self.setAcceptDrops(True)
		#self.setMinimumHeight(300)
		self.setMinimumSize(300,150)
		self.setMaximumWidth(100000)
		#h1.setSizeConstraint(QLayout.SetMinAndMaxSize)
		#self.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Preferred )
		self.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Expanding )
	
	def close(self) :
		self.figureZone.deleteFigure(self)
		

		

		
	def dropEvent(self, event):
		if event.mimeData().hasFormat("application/x-openelectrophy-figure"):
			itemData = event.mimeData().data("application/x-openelectrophy-figure")
			self.figureZone.drawFigure(itemData , multiple = False , ax = self.ax)
			self.canvas.draw()
			event.accept()
		else:
			event.ignore()
	
	def dragEnterEvent(self, event ) :
		#print 'DropZone dragEnterEvent'
		event.accept()
	def supportedDropActions(self) :
		#print 'supportedDropActions'
		return Qt.CopyAction
	def dragLeaveEvent(self, event ) :
		#print 'dragLeaveEvent'
		#print event
		event.accept()
	def dragMoveEvent(self, event ) :
		if event.source() == self:
			#event.ignore()
			#event.setDropAction(Qt.MoveAction)
			event.accept()
		else :
			#event.ignore()
			event.accept()
		#print 'dragMoveEvent',

class DropDrawButton(QPushButton) :
	def __init__(self,parent=None , figureZone = None) :
		QPushButton.__init__(self,'Draw')
		self.figureZone = figureZone
		self.setAcceptDrops(True)
	
	
	def dropEvent(self, event):
		if event.mimeData().hasFormat("application/x-openelectrophy-figure"):
			itemData = event.mimeData().data("application/x-openelectrophy-figure")
			self.figureZone.drawFigure(itemData , multiple = (self.figureZone.comboBoxMulti.currentIndex() == 0) )
			event.accept()
		else:
			event.ignore()
			
	def dragEnterEvent(self, event ) :
		event.accept()
		
	def supportedDropActions(self) :
		return Qt.CopyAction
	
	def dragLeaveEvent(self, event ) :
		event.accept()
		
	def dragMoveEvent(self, event ) :
		event.accept()




class FigureZone(QWidget  ) :
	def __init__(self, parent=None , explorer=None , dict_hierarchic_class = None):
		QWidget.__init__(self, parent)
		
		self.dict_hierarchic_class = dict_hierarchic_class
		
		self.explorer = explorer
		
		v1 = QVBoxLayout()
		v2 = QVBoxLayout()
		v1.addLayout(v2)
		
		
		# Option for plotting
		h3 = QHBoxLayout()
		v2.addLayout(h3)
		btDraw = DropDrawButton(figureZone = self)
		btDraw.setIcon(dict_icon['plot'])
		h3.addWidget(btDraw)
		self.connect(btDraw,SIGNAL('clicked()'),self.btDraw_Clicked)
		
		btCloseAll = QPushButton(self.tr('Close all figs'))
		btCloseAll.setIcon(dict_icon['fileclose'])
		self.connect(btCloseAll,SIGNAL('clicked()'),self.btCloseAll_Clicked)
		h3.addWidget(btCloseAll)
		
		
		self.comboBoxMulti = QComboBox()
		h3.addWidget(self.comboBoxMulti)
		self.comboBoxMulti.addItems([ 'multi figure' , 'mono figure' ])
		
		v4 = QVBoxLayout()
		h3.addLayout(v4)
		self.comboBoxOption = QComboBox()
		v4.addWidget(self.comboBoxOption)
		#st =  QStackedLayout()
		st =  QStackedWidget()
		st.setSizePolicy(QSizePolicy.Maximum , QSizePolicy.Maximum )
		self.connect(self.comboBoxOption, SIGNAL('activated(int)'),st, SLOT('setCurrentIndex(int)') )
		v4.addWidget(st)
		self.dict_checkBox = { }
		self.dict_button_param = { }
		self.dict_option_param = { }
		self.dict_option_value = { }
		self.dict_type_plot = { }
		for table in dict_hierarchic_class.keys() :
			myclass = dict_hierarchic_class[table]()
			if ('plot' in dir(myclass)) and ( 'list_type_plot' in dir(myclass) ) :
				self.dict_type_plot[table] = myclass.list_type_plot
				self.comboBoxOption.addItem(table)
				groupBox = QGroupBox(table)
				gridLayout = QGridLayout()
				groupBox.setLayout(gridLayout)
				st.addWidget(groupBox)
				list_checkBox = []
				list_button_param = [ ]
				for t,type_plot in enumerate(myclass.list_type_plot) :
					checkBox = QCheckBox(type_plot)
					if t == 0 : checkBox.setChecked(True)
					gridLayout.addWidget(checkBox , t , 0)
					list_checkBox.append(checkBox)
					bt = QPushButton('')
					bt.setIcon(dict_icon['param'])
					bt.setMaximumSize(25,25)
					gridLayout.addWidget(bt , t , 1)
					list_button_param.append(bt)
					self.connect(bt,SIGNAL('clicked()'),self.change_param_plot)
				self.dict_checkBox[table] = list_checkBox
				self.dict_button_param[table] = list_button_param
				self.dict_option_param[table] = myclass.list_option_param
				self.dict_option_value[table] = myclass.list_option_default

		

		# Figure area
		self.listFig = []
		
		qg = QGridLayout()
		
		ql =QLabel()
		#ql.setFrameStyle(QFrame.Sunken | QFrame.StyledPanel)
		ql.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Preferred )
		#ql.setSizePolicy(QSizePolicy.Expanding , QSizePolicy.Expanding )
		self.scrollArea = QScrollArea()
		self.vBoxFig = QVBoxLayout()
		self.vBoxFig.setSizeConstraint(QLayout.SetMinAndMaxSize)
		
		ql.setLayout(self.vBoxFig)
		self.scrollArea.setWidget(ql)
		self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
		#self.scrollArea.setFrameStyle(QFrame.Raised | QFrame.StyledPanel)
		
		self.scrollArea.setWidgetResizable(True)
		
		qg.addWidget(self.scrollArea,0,0)
		self.sliderH = QSlider(Qt.Horizontal)
		self.sliderH.setMinimum(200)
		self.sliderH.setMaximum(10000)
		self.sliderH.setValue(400)
		qg.addWidget(self.sliderH,1,0)
		self.connect(self.sliderH,SIGNAL('sliderReleased()'),self.slider_Changed)
		self.sliderV = QSlider(Qt.Vertical)
		self.sliderV.setMinimum(100)
		self.sliderV.setMaximum(1000)
		self.sliderV.setValue(250)
		qg.addWidget(self.sliderV,0,1)
		self.connect(self.sliderV,SIGNAL('sliderReleased()'),self.slider_Changed)
		v1.addLayout(qg)
		#v1.addWidget(self.scrollArea)
		
		#self.palette().setColor(QPalette.Background, Qt.white)
		#self.setAutoFillBackground(True)
		
		self.setLayout(v1)
		
	
	def addNewFigure(self) :
		figureAndTool =FigureAndTool(figureZone = self)
		self.vBoxFig.addWidget(figureAndTool)
		self.listFig.append(figureAndTool)
		return figureAndTool.ax
	
	def deleteFigure(self, fig) :
		if fig in self.listFig :
			fig.setVisible(False)
			self.vBoxFig.removeWidget(fig)
			self.listFig.remove(fig)
		
	def drawFigure(self,data , multiple = True , ax = None ) :
		
		if ax is None and not(multiple)  :
			ax = self.addNewFigure()
		
		for d in data.split(';') :
			if d =='' : continue
			table, id_principal = d.split(':')
			table = str(table)
			id_principal = int(id_principal)
			
			
			my_class = self.dict_hierarchic_class[table]()
			my_class.load_from_db(id_principal = id_principal)
			
			
			for t , type_plot in enumerate(my_class.list_type_plot) :
				if self.dict_checkBox[table][t].isChecked() :
					if multiple :
						ax = self.addNewFigure()
					my_class.plot(ax=ax , type_plot=type_plot , option =  self.dict_option_value[table][t]  )
			
					#self.dict_checkBox[table] = list_checkBox
					#self.dict_option_param[table] = myclass.list_option_param
					#self.dict_option_value[table] = myclass.list_option_default
	
	def btDraw_Clicked(self) :
		data = self.explorer.getPlotSelection()
		self.drawFigure(data , multiple = (self.comboBoxMulti.currentIndex() == 0) )
		
	
	def btCloseAll_Clicked(self) :
		for f in range(len(self.listFig)) :
			self.deleteFigure(self.listFig[0])
	
	def change_param_plot(self) :
		for key, list_button in self.dict_button_param.iteritems() :
			for i ,button in enumerate(list_button) :
				if button == self.sender() :
					table , t = key , i
					break
					
		self.dict_option_value[table][t] = ParamDialog(  self.dict_option_param[table][t] ,
													self.dict_option_value[table][t]  ,
													family = 'plot_'+table+self.dict_type_plot[table][t] )
		
	def slider_Changed(self) :
		for f in self.listFig :
			f.setMinimumSize(self.sliderH.value() , self.sliderV.value())
			
	def refresh_link(self) :
		list_link_axes = []
		list_link_fig = []
		for f in self.listFig :
			if f.tb_link.isChecked() :
				list_link_axes.append(f.ax)
				list_link_fig.append(f.fig)
			f.ax._sharex=None
			f.ax._masterx = False
			f.ax.pchanged()
		if len(list_link_axes) >1 :
			#for ax in list_link_axes[1:] :
			list_link_axes[0]._masterx = True
			list_link_axes[0].pchanged()
			for i  in range(1,len(list_link_axes)) :
				list_link_axes[i]._sharex = list_link_axes[0]
				list_link_axes[i]._masterx = False
				#list_link_axes[i].set_figure(list_link_fig[i])
				list_link_axes[i].pchanged()
		
		
		for f in self.listFig :
			print f.ax._sharex , f.ax._masterx
			f.ax.draw_artist()




