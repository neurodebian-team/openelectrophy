# -*- coding: utf-8 -*-

"""
widget for integrating matplotlib figure in QT4
derived from matplotlib example


Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


import sys
#import PyQt4
from PyQt4.QtCore import *
from PyQt4.QtGui import *
#from PyQt4 import QtCore, QtGui

if __name__ == "__main__":
	app = QApplication(sys.argv)

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.backends.backend_qt4 import FigureCanvasQT as FigureCanvas
from matplotlib.figure import Figure
#from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar

from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar2

#from matplotlib.backend_bases import NavigationToolbar2


import new

class MyMplCanvas(FigureCanvas):
	"""Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
	def __init__(self, parent=None, width=5, height=4, dpi=100 , nb_ax = 1 , ax_shape = (1,1), sharex= None , sharey= None):
		self.fig = Figure(figsize=(width, height), dpi=dpi)
		self.list_ax = [ ]
		for i in range(nb_ax) :
			ax = self.fig.add_subplot(ax_shape[0],ax_shape[1],i+1 , sharex=sharex , sharey=sharey)
			self.list_ax.append(ax)
		if nb_ax :
			self.ax = self.list_ax[0]
			self.ax.hold(True)
		else : 
			self.ax = None
		
		FigureCanvas.__init__(self, self.fig)
#        self.reparent(parent, QPoint(0, 0))

		FigureCanvas.setSizePolicy(self,
					QSizePolicy.Expanding,
					QSizePolicy.Expanding)
		FigureCanvas.updateGeometry(self)
		
		#self.setAcceptDrops(True)

	def sizeHint(self):
		w, h = self.get_width_height()
		return QSize(w, h)

	def minimumSizeHint(self):
		return QSize(10, 10)

class MyNavigationToolbar(NavigationToolbar2) :
	def __init__(self , parent , canvas , direction = 'h' ) :
		#NavigationToolbar.__init__(self,parent,canevas)
		#self.layout = QVBoxLayout( self )
		
		#self.canvas = canvas
		#QWidget.__init__( self, parent )
		
		#if direction=='h' :
		#	self.layout = QHBoxLayout( self )
		#else :
		#	self.layout = QVBoxLayout( self )
		
		#self.layout.setMargin( 2 )
		#self.layout.setSpacing( 0 )
		
		#NavigationToolbar2.__init__( self, canvas,parent )
		
		NavigationToolbar2.__init__( self, canvas,parent )
		if direction=='h' :
			pass
		else :
			try :
				self.setOrientation(Qt.Vertical)
			except :
				# bad patch for matplot lib version <=0.0.91
				self.layout.setDirection(QBoxLayout.TopToBottom)
		
		
	def set_message( self, s ):
		pass


#####################################################################
## DragingCursor
def on_move_canvas(self,event) :
	if event.inaxes is None :return
	for a in self.figure.get_axes() :
		if a.get_navigate_mode() is not None :
			return
	for dragingcursor in self.list_dragingcursor :
		if dragingcursor.moving :
			if event.inaxes != self.ax: return
			#dragingcursor.linev.set_visible(dragingcursor.vertOn)
			#dragingcursor.lineh.set_visible(dragingcursor.horizOn)
			if dragingcursor.direction =='ver' :
				if (dragingcursor.limit1 is not None) and (event.xdata <= dragingcursor.limit1)  :
					x = dragingcursor.limit1
				elif (dragingcursor.limit2 is not None) and (event.xdata >= dragingcursor.limit2)  :
					x = dragingcursor.limit2
				else :
					x = event.xdata
				dragingcursor.text.set_position((x , self.ax.get_ylim()[1]))
				dragingcursor.linev.set_xdata((x, x))
			if dragingcursor.direction =='hor' :
				if (dragingcursor.limit1 is not None) and (event.ydata <= dragingcursor.limit1)  :
					y = dragingcursor.limit1
				elif (dragingcursor.limit2 is not None) and (event.ydata >= dragingcursor.limit2)  :
					y = dragingcursor.limit2
				else :
					y = event.ydata
				dragingcursor.text.set_position((self.ax.get_xlim()[1],y ))
				dragingcursor.lineh.set_ydata((y,y))
			dragingcursor._update()

def on_button_press_event_canvas(self,event) :
	if event.inaxes is None :return
	
	for a in self.figure.get_axes() :
		if a.get_navigate_mode() is not None :
			return
		
	ax = event.inaxes
	for dragingcursor in self.list_dragingcursor :
		if dragingcursor.moving :
			return
	#loocking for the nearest
	list_distance = []
	list_delta = []
	list_coor = []
	for dragingcursor in self.list_dragingcursor :
		if dragingcursor.direction =='hor' :
			distance = abs(event.ydata - dragingcursor.coor())
			delta = (ax.get_ylim()[1]-ax.get_ylim()[0])/35
			coor = event.ydata
		elif dragingcursor.direction =='ver' :
			distance = abs(event.xdata - dragingcursor.coor())
			delta = (ax.get_xlim()[1]-ax.get_xlim()[0])/35
			coor = event.xdata
		list_distance.append(distance)
		list_coor.append(coor)
		list_delta.append(delta)
	nr = list_distance.index(min(list_distance))
			
	if (self.list_dragingcursor[nr].coor() > list_coor[nr]-list_delta[nr]) & \
	(self.list_dragingcursor[nr].coor() < list_coor[nr]+list_delta[nr]) :
		self.list_dragingcursor[nr].moving = True


def on_button_release_event_canvas(self,event) :
	if event.inaxes is None :return

	for a in self.figure.get_axes() :
		if a.get_navigate_mode() is not None :
			return
		
	for dragingcursor in self.list_dragingcursor :
		if dragingcursor.moving :
			dragingcursor.moving = False
			if dragingcursor.func_after_move is not None :
				dragingcursor.func_after_move()
				
def on_draw_event_canvas(self,event) :
	for dragingcursor in self.list_dragingcursor :
		dragingcursor.resfresh_text_position()


			

class DragingCursor  :
	def __init__(self , ax, direction = 'hor' , coor = 0. , useblit = False, label =u'' ,
						func_after_move = None, 
						limit1 = None, limit2 = None , **lineprops ) :
		self.ax = ax
		self.canvas = ax.figure.canvas
		self.direction = direction
		self.moving = False
		self.label = label
		self.limit1 = limit1
		self.limit2 = limit2
		self.func_after_move = func_after_move
		
		self.visible = True
		self.horizOn = False
		self.vertOn = False
		if direction =='ver' :
			self.vertOn = True
			self.linev = ax.axvline(x=coor, visible=self.vertOn, **lineprops)
			self.lineh = None
		if direction =='hor' :
			self.horizOn = True
			self.lineh = ax.axhline(y=coor, visible=self.horizOn, **lineprops)
			self.linev = None
		self.useblit = useblit
		#self.lineh = ax.axhline(y=coor, visible=self.horizOn, **lineprops)
		#self.linev = ax.axvline(x=coor, visible=self.vertOn, **lineprops)
		
		self.background = None
		#self.needclear = False
		if direction =='ver' :
			va = 'top'
			ha = 'left'
			x = coor
			y = self.ax.get_ylim()[1]
		if direction =='hor' :
			va = 'top'
			ha = 'right'
			x = self.ax.get_xlim()[1]
			y = coor
		
		self.text = self.ax.text(x, y, self.label,
			verticalalignment=va, horizontalalignment=ha)



		if 'list_dragingcursor' not in dir(self.canvas) :
			self.canvas.list_dragingcursor = []
			
			self.canvas.my_on_move = new.instancemethod(on_move_canvas ,
						self.canvas,
						self.canvas.__class__)
			self.canvas.my_on_button_press_event = new.instancemethod(on_button_press_event_canvas ,
						self.canvas,
						self.canvas.__class__)
			self.canvas.my_on_button_release_event = new.instancemethod(on_button_release_event_canvas ,
						self.canvas,
						self.canvas.__class__)
			self.canvas.my_on_draw_event = new.instancemethod(on_draw_event_canvas ,
						self.canvas,
						self.canvas.__class__)
			self.canvas.mpl_connect('motion_notify_event', self.canvas.my_on_move)
			self.canvas.mpl_connect('button_press_event', self.canvas.my_on_button_press_event)
			self.canvas.mpl_connect('button_release_event', self.canvas.my_on_button_release_event)
			self.canvas.mpl_connect('draw_event', self.canvas.my_on_draw_event)
			
		self.canvas.list_dragingcursor.append(self)

	def setCoor(self,coor ):
		if self.direction =='hor' :
			self.lineh.set_ydata([coor,coor])
		elif  self.direction =='ver' :
			self.linev.set_xdata([coor,coor])
		self._update()
		
	def coor(self) :
		if self.direction =='hor' :
			return self.lineh.get_ydata()[0]
		elif  self.direction =='ver' :
			return self.linev.get_xdata()[0]
		else :
			return None
	
	def _update(self):
		if self.useblit:
			#if self.background is not None:
			#	self.canvas.restore_region(self.background)
			
			if self.background is None:
				self.background =self.canvas.copy_from_bbox(self.ax.bbox)
			self.canvas.restore_region(self.background)
			if self.direction =='hor' :	
				self.ax.draw_artist(self.lineh)
			elif  self.direction =='ver' :
				self.ax.draw_artist(self.linev)
			self.ax.draw_artist(self.text)
			self.canvas.blit(self.ax.bbox)
		else:
			self.canvas.draw_idle()
		return False

	def delete(self):
		#~ self.canvas.list_dragingcursor.remove(self)
		
		if self.useblit:
			self.background = self.canvas.copy_from_bbox(self.ax.bbox)
		#~ if self.direction =='ver' :
			#~ self.linev.set_visible(False)
		#~ if self.direction =='hor' :
			#~ self.lineh.set_visible(False)
		if self.direction =='ver' :
			#~ del self.linev
			self.ax.lines.remove(self.linev)
		if self.direction =='hor' :
			#~ del self.lineh
			self.ax.lines.remove(self.lineh)
		
		#~ self.text.set_visible(False)
		self.ax.texts.remove(self.text)
		#~ self.text.set_visible(False)
		
		
		#~ print len(self.canvas.list_dragingcursor),
		self.canvas.list_dragingcursor.remove(self)
		#~ print len(self.canvas.list_dragingcursor)
		
		#~ self.text.set_visible(False)
		
		#~ self.text.set_visible(False)
		#~ print 'efface'
		
		#~ self.canvas.draw_idle()
		
		
		
	def resfresh_text_position(self) :
		x,y = self.text.get_position()
		if self.direction =='ver' :
			self.text.set_position((x , self.ax.get_ylim()[1]))
		if self.direction =='hor' :
			self.text.set_position(( self.ax.get_xlim()[1] , y))
		self.ax.draw_artist(self.text)
			



	
#------------------------------------------------------------------------------
# test


class TestMainLayout(QWidget):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
			
		
		v1 = QVBoxLayout()
		useblit = True
		c = MyMplCanvas()
		v1.addWidget(c)
		t = MyNavigationToolbar(c , c , direction = 'h' )
		v1.addWidget(t)
		c.ax.plot([-1,0,1])
		c.ax.set_xlim([-1,1])
		c.ax.set_ylim([-1,1])
		a = DragingCursor(c.ax , coor = -0.5,limit1 = -0.8, limit2 = None, useblit=useblit, label='1', color='red', linewidth=2 )
		b = DragingCursor(c.ax ,  coor = 0.2, limit1 = None, limit2 = 1, useblit=useblit, label='2', color='blue', linewidth=2)
		c = DragingCursor(c.ax , coor = 0.3 , limit1 = 0., limit2 = 0.5, direction = 'ver', label='3', useblit=False, color='red', linewidth=2, )
		e = DragingCursor(c.ax , coor = -0.5 , direction = 'ver', label='4', useblit=useblit, color='red', linewidth=2)
		e.delete()
		self.setLayout(v1)
		

def start_app() :
	#app = QApplication(sys.argv)
	app.setFont(QFont("Verdana", 12))
	ml = MainLayout()
	ml.show()
	sys.exit(app.exec_())



#------------------------------------------------------------------------------
if __name__ == "__main__":
	start_app()
