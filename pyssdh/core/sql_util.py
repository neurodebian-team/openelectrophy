# -*- coding: utf-8 -*-


"""
class et function in relation with sql

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""


import MySQLdb

#from pysqlite2 import dbapi2 as sqlite
#import sqlite
#from pysqlite2 import dbapi2 as sqlite
#from pysqlite3 import dbapi2 as sqlite

from numpy import *
#from scipy import *

import sys

import pickle

import threading

###############################################################################
# MySQL
#------------------------------------------------------------------------------
def init_connection(host=None, login=None, password=None,nom_base=None,
		dbfile=None):
	
	global db
	#global cur
	global connection_type
	global lock
	lock = threading.Lock()
	if dbfile is None :
		db = MySQLdb.connect(host=host, user=login, passwd=password , use_unicode = True)
		
		if nom_base is not None :
			db.select_db(nom_base)
		#cur = db.cursor()
		connection_type = 'mysql'
	else :
		#db = sqlite.connect(dbfile)
		# pour les datetime
		db = sqlite.connect(dbfile , detect_types=sqlite.PARSE_DECLTYPES|sqlite.PARSE_COLNAMES)
		#cur = db.cursor()
		connection_type = 'sqlite'

#------------------------------------------------------------------------------
def close_connection():
	db.close()

#------------------------------------------------------------------------------
def sql_pack(query,values = ()) :
	result , description = sql_execute(query,values)
	return result

#------------------------------------------------------------------------------
def sql_execute(query,values = ()) :
	lock.acquire()
	cur = db.cursor()
	cur.execute(query,values)
	result  = cur.fetchall()
	description = cur.description
	lock.release()
	return result , description

#------------------------------------------------------------------------------
def sql(query,values = (),Array = False,dtype='f') :
	result , description = sql_execute(query,values)
	
	if description==None : return ()
	n = len(description)

	if len(result) == 0 :
		if Array :
			tuple([  array([],dtype=dtype) for i in range(n) ])
		else :
			 return tuple([ () for i in range(n) ])
	
	t = [ ]
	for i in range(n) :
		t.append([])
	for row in result :
		for i in range(n) :
			t[i].append(row[i])
	if Array :
		for i in range(n) :
			t[i] = array(t[i],dtype=dtype)
	return tuple(t)

#------------------------------------------------------------------------------
def sql1(query,values = (),Array = False, dtype='f') :
	result , description = sql_execute(query,values)	

	if  description==None : return ()
	n = len(description)
	
	if len(result) == 0 :
		if Array :
			tuple([  array([],dtype=dtype) for i in range(n) ])
		else :
			 return tuple([ () for i in range(n) ])

	t = [ ]
	if len(result)==1 :
		for row in result :
			for i in range(n) :
				t.append(row[i])
		if Array :
			for i in range(n) :
				t[i] = array(t[i],dtype = dtype)
	else :
		for i in range(n) :
			t.append([])
		for row in result :
			for i in range(n) :
				t[i].append(row[i])
		if Array :
			for i in range(n) :
				t[i] = array(t[i],dtype=dtype)
	return tuple(t)



#------------------------------------------------------------------------------
def is_table(table_name) :

	if connection_type ==  'sqlite':
		tables, =  sql('SELECT name FROM sqlite_master WHERE type = "table"')
		
	else :
		tables, =  sql("SHOW TABLES")


	if sum(array(tables) == table_name ) == 1 :
		return True
	else :
		return False


#------------------------------------------------------------------------------
def is_field(table, field_name) :
	field  = sql_pack('SHOW FIELDS FROM '+table)
	field =[ field[i][0] for i in range(len(field)) ]
	return field_name in field

#------------------------------------------------------------------------------
def add_field(table,field_name, typ = ' TEXT ') :
	if not(is_field(table, field_name)) :
		sql('ALTER TABLE '+ table+ ' ADD '+field_name+' '+typ)
	

#------------------------------------------------------------------------------
class database_storage :
	"""
	virtual class for mananing storage
	"""
	table_name =''
	list_field = []
	field_type = []
	list_table_child = [ ]
	#------------------------------------------------------------------------------
	def __init__(self, **karg) :
		id_name = 'id_'+self.table_name
		self.id_principal = None
		self[id_name] = None
		for field in self.list_field :
			self[field] = None
		for k,v in karg.iteritems() :
			if k in self.list_field :
				self[k] = v
		if  id_name in karg.keys() :
			self.id_principal = karg[id_name]
			self[id_name] = karg[id_name]
			self.load_from_db(id_principal = self.id_principal)
		if  'id_principal' in karg.keys() :
			self.id_principal = karg['id_principal']
			self[id_name] = karg['id_principal']
			self.load_from_db(id_principal = self.id_principal)
			
			
	#------------------------------------------------------------------------------	
	def __setitem__(self , key , val) :
		self.__dict__[key] = val
		
	#------------------------------------------------------------------------------	
	def __getitem__(self, key):
		return self.__dict__[key]

	#------------------------------------------------------------------------------
	def create_table(self) :
		if self.is_table() : return
		query = 'CREATE TABLE  `'+self.table_name+'`'
		query += ' (id_'+self.table_name+' INT PRIMARY KEY AUTO_INCREMENT '
#		query += ' (id_'+self.table_name+' INT PRIMARY KEY '
		for i,field in enumerate(self.list_field) :
			
			if self.field_type[i] == 'INDEX' :
				query +=  ', `' + field + '` '+' INT , INDEX('+field+') '
			elif self.field_type[i] == 'NUMPY' : 
				query +=  ', `' + field +'` LONGBLOB , ' +\
							'`'+field+'_dtype` TEXT ,'  +\
							'`'+field+'_shape` TEXT '
			elif self.field_type[i] == 'PYOBJECT' : 
				query += ', `' + field + '` LONGBLOB '
			else :
				query += ', `' + field + '` '+self.field_type[i]
		query += ')'
		#~ print query
		sql(query)
	#------------------------------------------------------------------------------
	def is_table(self) :
		return is_table(self.table_name)
	
	#------------------------------------------------------------------------------
	def is_field(self,field_name) :
		return is_field(self.table_name, field_name)
	
	#------------------------------------------------------------------------------
	def test_all_field(self) :
		for i , field in enumerate(self.list_field) :
			if not( self.is_field(field) ) :
				if self.field_type[i] == 'INDEX' :
					query = 'ALTER TABLE %s  ADD (`%s` INT) ' % ( self.table_name , field  )
					sql(query)
					query = 'ALTER TABLE %s  ADD  INDEX (`%s` ) ' % ( self.table_name , field  )
					sql(query)
				elif self.field_type[i] == 'NUMPY' : 
					query = 'ALTER TABLE %s  ADD (`%s` LONGBLOB , `%s_dtype` TEXT , `%s_shape` TEXT) ' % ( self.table_name , field, field , field  )
					sql(query)
				elif self.field_type[i] == 'PYOBJECT' : 
					query = 'ALTER TABLE %s  ADD (`%s` %s) ' % ( self.table_name , field ,'LONGBLOB' )
					sql(query)
				else :
					query = "ALTER TABLE %s  ADD (`%s` %s) " % ( self.table_name , field ,self.field_type[i] )
					sql(query)

	#------------------------------------------------------------------------------
	def save_to_db(self) :
		if not(self.is_table()) : self.create_table()
		self.test_all_field()
		
		if self.id_principal == None :
			values = ()
			query = 'INSERT INTO '+self.table_name+'  ('
			for i, field in enumerate(self.list_field) :
				if self.field_type[i] == 'NUMPY' :
					query += field+' ,'+field+'_dtype ,'+field+'_shape,'
				else :
					query += field+' ,'
			query = query[:-1] + ' ) VALUES( '
			for i,field in enumerate(self.list_field) :
				if self.field_type[i] == 'NUMPY' :
					query += ' %s , %s , %s ,'
					if self.__dict__[field] is None :
						values += None , None , None
					else :
						values += self.__dict__[field].tostring(),self.__dict__[field].dtype.str , str(self.__dict__[field].shape),
				elif self.__dict__[field] is None:
					query += field + ' = NULL ,'
				elif self.field_type[i] == 'FLOAT' :
					query += ' %s ,'
					if  (self.__dict__[field] is not None) and isfinite(self.__dict__[field]):
						values += float(self.__dict__[field]),
					else :
						values += None,
				elif self.field_type[i] == 'PYOBJECT' :
					query += ' %s ,'
					values += pickle.dumps(self.__dict__[field]) ,
				else :
					query += ' %s ,'
					values += self.__dict__[field],
			query = query[:-1] + ' ) '
			sql(query, values)
			self.id_principal, = sql1('SELECT LAST_INSERT_ID() ')
			#self.id_principal = cur.insert_id()
			self['id_'+self.table_name] = self.id_principal
		else :
			values = ()
			query = 'UPDATE '+self.table_name#+' FROM  '+self.table_name
			query += ' SET '
			for i,field in enumerate(self.list_field) :
				if self.field_type[i] == 'NUMPY' :
					query += field+'  = %s ,'
					query += field+'_dtype '+'  = %s ,'
					query += field+'_shape '+'  = %s ,'
					if self.__dict__[field] is None :
						values += None , None , None
					else :
						values += self.__dict__[field].tostring(),self.__dict__[field].dtype.str , str(self.__dict__[field].shape),
				elif self.__dict__[field] is None:
					query += field + ' = NULL ,'
				elif self.field_type[i] == 'FLOAT' :
					query += field+'  = %s ,'
					if  (self.__dict__[field] is not None) and isfinite(self.__dict__[field]):
						values += self.__dict__[field],
					else :
						values += None,
				elif self.field_type[i] == 'PYOBJECT' :
					query += field+'  = %s ,'
					values += pickle.dumps(self.__dict__[field]) ,
				else :
					query += field+'  = %s ,'
					values += self.__dict__[field],
			query = query[:-1] +' WHERE id_'+self.table_name+' =  '+str(self.id_principal)
			#~ print query,values
			sql(query,values)
		return self.id_principal
			
	#------------------------------------------------------------------------------
	def load_from_db(self, id_principal = None, load_numpy = True) :
		self.test_all_field()
		if id_principal is not None : 
			self.id_principal = id_principal
			self['id_'+self.table_name] = self.id_principal
		query = ' SELECT '
		for i,field in enumerate(self.list_field) :
			if not(load_numpy) and self.field_type[i] == 'NUMPY' : 
				query += ' NULL,'
			else:
				query += field+' ,'
		query = query[:-1]
		query += ' FROM '+self.table_name+' WHERE id_'+self.table_name +' = %s ' % self.id_principal
		values = sql1(query)
		#print values
		for i,field in enumerate(self.list_field) :
			#print i,field
			if self.field_type[i] == 'NUMPY' :
				if  values[i] is None :
					self.__dict__[field] = None
				else :
					query = ' SELECT '
					query +=  field+'_dtype , '+field+'_shape '
					query += ' FROM '+self.table_name+' WHERE id_'+self.table_name+ ' = %s ' % self.id_principal
					dt , sh = sql1(query)
					
					self.__dict__[field] =  fromstring(values[i],dtype = dtype(str(dt)))
					
					sh = sh.replace( ',)' , ')')
					sh = [ int(s) for s in sh[1:-1].split(',') ]
					self.__dict__[field] = self.__dict__[field].reshape(sh)
			elif self.field_type[i] == 'PYOBJECT' :
				self.__dict__[field] = pickle.loads(values[i])
			else :
				self.__dict__[field] = values[i]
				
				
	#------------------------------------------------------------------------------
	def delete_from_db(self) :
		print 'attention delete ' ,  self.table_name , self.id_principal
		query = 'DELETE  '+self.table_name+' FROM '+self.table_name+' WHERE id_'+self.table_name +' = %s ' % self.id_principal
		sql(query)
	
	#------------------------------------------------------------------------------
	def delete_from_db_and_child(self , dict_hierarchic_class ) :
		for table_child in self.list_table_child :
			if is_table(table_child) :
				query = 'SELECT id_'+table_child+' FROM '+table_child+' WHERE id_'+self.table_name +' = %s ' % self.id_principal
				for id_child in sql(query)[0] :
					my_class = dict_hierarchic_class[table_child]()
					my_class.id_principal = id_child
					my_class.delete_from_db_and_child(dict_hierarchic_class )
		self.delete_from_db()


	
	
	
	
	
