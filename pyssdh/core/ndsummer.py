# -*- coding: utf-8 -*-

"""
Class for summing nd elements :
	CSummer for summing continous nd element
	DSummer for summing distrect elements

Author:	Samuel Garcia
Laboratoire de Neurosciences Sensorielles, Comportement, Cognition.
CNRS - UMR5020 - Universite Claude Bernard LYON 1
Equipe logistique et technique
50, avenue Tony Garnier
69366 LYON Cedex 07
FRANCE
sgarcia@olfac.univ-lyon1.fr

License: CeCILL v2 (GPL v2 compatible)

"""

from scipy import *
import pylab

#------------------------------------------------------------------------------
class CSummer :
	"""
	Class for summing continous nd element
	
	axis is the demension for alignement with the t vecteur
	
	"""
	#------------------------------------------------------------------------------
	def __init__(self , axis = 0 , yl = (None,None)) :
		
		self.axis = axis
		self.t = None
		self.n = None
		self.sum_sig = None
		self.sum2_sig = None
		self.yl = yl
	
	#------------------------------------------------------------------------------
	def add(self , sig ,t) :
		if self.t == None :
			self.t = t
			self.n = ones_like(t).astype('i')
			self.sum_sig = sig
			self.sum2_sig = sig**2
			return
		
		#verify ndim
		if sig.ndim != self.sum_sig.ndim :
			print 'problem ndim'
			return
		for n in range(self.sum_sig.ndim) :
			if n==self.axis : continue
			if sig.shape[n] != self.sum_sig.shape[n] :
				print 'problem shape'
				return
		
		#print  self.n.shape, self.sum_sig.shape
		#adding zeros rigth and left if necessary
		dt = mean(diff(self.t))
		l_zeros = where(t<self.t[0])[0].size
		#print 'l_zeros' , l_zeros
		if  l_zeros >0 :
			self.n = r_[zeros(l_zeros , dtype=self.n.dtype) ,self.n]
			self.t = r_[ (arange(l_zeros)-l_zeros)*dt+self.t[0]   ,self.t]
			shap = list(self.sum_sig.shape)
			shap[self.axis] = l_zeros
			self.sum_sig = concatenate((zeros(shap,dtype=self.sum_sig.dtype) , self.sum_sig ) , axis = self.axis)
			self.sum2_sig = concatenate((zeros(shap,dtype=self.sum_sig.dtype) , self.sum2_sig ) , axis = self.axis)
			l_shift = None
		else :
			l, = where(self.t<=t[0])
			if l.size ==0 :
				l_shift = None
			else :
				l_shift = l[-1]
		#r_zeros = where(t>=(self.t[-1]+dt))[0].size
		if l_shift is not None :
			r_zeros = l_shift + t.size - self.t.size
		else :
			r_zeros = t.size - self.t.size
		#print 'r_zeros' , r_zeros
		if  r_zeros >0 :
			self.n = r_[self.n , zeros(r_zeros , dtype=self.n.dtype) ]
			self.t = r_[self.t ,  (arange(1,r_zeros+1))*dt+self.t[-1]   ]
			shap = list(self.sum_sig.shape)
			shap[self.axis] = r_zeros
			self.sum_sig = concatenate((self.sum_sig , zeros(shap,dtype=self.sum_sig.dtype) ) , axis = self.axis)
			self.sum2_sig = concatenate((self.sum2_sig , zeros(shap,dtype=self.sum_sig.dtype)  ) , axis = self.axis)
			r_shift = None
		else :
			if l_shift is not None :
				r_shift = l_shift + t.size
			else : 
				r_shift = t.size
	
			
		#print 'l_shift , r_shift' , l_shift , r_shift
		
		sl = [ slice(None) for n in range(sig.ndim) ]
		sl[self.axis] = slice(l_shift,r_shift)
		#print self.sum_sig[sl].shape, sig.shape , t.shape
		self.sum_sig[sl] = self.sum_sig[sl] + sig
		self.sum2_sig[sl] = self.sum2_sig[sl] + sig**2
		self.n[l_shift:r_shift] +=1
		#print self.sum_sig.shape
		
	#------------------------------------------------------------------------------
	def plot(self, fig = None,
				ax_moy = None,
				ax_std = None,
				an_n = None,
				color = 'b',
				titl = None ,
				plot_std = False,
				plot_all=False,
				transpose2d = False ) :
		
		n_plot =1
		if plot_all : 
			n_plot +=1
		if plot_std and (self.sum_sig.ndim ==2) :
			n_plot +=1
		if fig is None and ax_moy is None :
			fig = pylab.figure()
			ax_moy = fig.add_subplot(n_plot,1,1)
		if fig is not None and ax_moy is None :
			ax_moy = fig.add_subplot(n_plot,1,1)
			
		if self.sum_sig.ndim ==1 :
			if plot_all :
				n = self.n
				m = self.sum_sig/self.n
				t = self.t
				if ax_n is None :
					ax_n = fig.add_subplot(n_plot,1,2)
				ax_n.plot(self.t,n)
			else :
				ind, = where(self.n.max() == self.n)
				n = self.n.max()
				m = self.sum_sig[ind]/n
				t = self.t[ind]
			ax_moy.plot(t,m)
			if plot_std :
				if plot_all :
					st = sqrt(self.sum2_sig/n-m**2)
				else :
					st = sqrt(self.sum2_sig[ind]/n-m**2)
				ax_moy.plot(t,m+st,'--')
				ax_moy.plot(t,m-st,'--')
		
		if self.sum_sig.ndim ==2 :
			if plot_all :
				xl = self.t[0] , self.t[-1]
				yl = self.yl
				if self.axis==0 :
					n = repmat(self.n,self.sum_sig.shape[1],1).transpose()
				else :
					n = repmat(self.n,self.sum_sig.shape[0],1)
				m = self.sum_sig/n
				if ax_n is None :
					ax_n = fig.add_subplot(n_plot,1,3)
				ax_n.imshow(n.transpose(), interpolation='nearest',extent = xl+yl , origin ='lower' , aspect = 'normal')
				ax_n.set_xlim(xl)
				ax_n.set_ylim(yl)
			else :
				ind, = where(self.n.max() == self.n)
				#~ print ind
				#~ print ind.shape
				xl = self.t[ind.min()] , self.t[ind.max()]
				yl = self.yl
				n = self.n.max()
				#print ind, n, self.n.shape, self.sum_sig.shape
				if self.axis == 0 :
					m = self.sum_sig[ind,:]/n
				else :
					m = self.sum_sig[:,ind]/n
			if self.axis == 0 :
				mp = m.transpose()
			else :
				mp = m
#			print xl,yl
			if transpose2d:
				xl, yl = yl, xl
				mp = mp.transpose()
			ax_moy.imshow(mp, interpolation='nearest',extent = xl+yl , origin ='lower' , aspect = 'normal' )
			ax_moy.set_xlim(xl)
			ax_moy.set_ylim(yl)
			if plot_std :
				if ax_std is None :
					ax_std =  fig.add_subplot(n_plot,1,2)
				if plot_all :
					st = sqrt(self.sum2_sig/n-m**2)
				else :
					if self.axis == 0 :
						st = sqrt(self.sum2_sig[ind,:]/n-m**2)
					else :
						st = sqrt(self.sum2_sig[:,ind]/n-m**2)
				if (self.axis == 0) ^transpose2d:
					st = st.transpose()
				ax_std.imshow(st, interpolation='nearest',extent = xl+yl , origin ='lower' , aspect = 'normal' )
				ax_std.set_xlim(xl)
				ax_std.set_ylim(yl)
				
		if titl is not None : ax.set_title(titl)
		
		return fig
		
		
#------------------------------------------------------------------------------
class DSummer :
	"""
	Class for summing discrete elements
	
	"""
	#------------------------------------------------------------------------------
	def __init__(self , size_win = None, 
			center_win = None,
			n_win = None) :
		if center_win is not None :
			self.center_win = center_win
			self.n_win = center_win.size
		elif n_win is not None :
			self.n_win = n_win
		self.sum_event = zeros(self.n_win, dtype='f')
		self.n=zeros(self.n_win)
		self.size_win = float(size_win)
	
	#------------------------------------------------------------------------------
	def add(self,event,center_win=None) :
		if center_win is None :
			center_win = self.center_win
		
		self.n += 1
		for i, center in enumerate(center_win) :
			self.sum_event[i] += where( ((center-self.size_win/2)<=event) & ((center+self.size_win/2)>=event) )[0].size/self.size_win
			

	